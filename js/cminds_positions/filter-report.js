function filterOffices(obj){
    var optionId = obj.options[obj.selectedIndex].value;
    if(optionId) {
        $$('#loading-mask')[0].show();
        $$('#cminds_salesrep_report_filter_container #office #report_office option').each(function (el) {
            if (el.value) {
                el.setAttribute('hidden', true);
                if (el.getAttribute('data-parent-id') == optionId) {
                    el.removeAttribute('hidden');
                }
            }
        });
        $$('#loading-mask')[0].hide();
        $$('#cminds_salesrep_report_filter_container #office')[0].show();
    } else {
        $$('#cminds_salesrep_report_filter_container .customer')[0].hide();
        $$('#cminds_salesrep_report_filter_container .customer #report_customer option')[0].selected = true;
        $$('#cminds_salesrep_report_filter_container .office')[0].hide();
        $$('#cminds_salesrep_report_filter_container .office #report_office option')[0].selected = true;
    }
}

function filterCustomers(obj, url){
    var optionId = obj.options[obj.selectedIndex].value;

    if(optionId) {
        new Ajax.Request(url, {
            method: 'get',
            parameters: {
                office_id: optionId
            },
            onSuccess: function (response) {
                var res = JSON.parse(response.responseText);
                $$('#cminds_salesrep_report_filter_container .customer #report_customer')[0].update(res);
                $$('#cminds_salesrep_report_filter_container .customer')[0].show();
            }
        });
    } else {
        $$('#cminds_salesrep_report_filter_container .customer #report_customer option')[0].selected = true;
        $$('#cminds_salesrep_report_filter_container .customer')[0].hide();
    }
}
