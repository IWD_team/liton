<?php

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);

$mageFilename = 'app/Mage.php';

if (!file_exists($mageFilename)) {
    echo $mageFilename . ' was not found';
    exit;
}

require_once $mageFilename;

Mage::app();

$resource = Mage::getSingleton('core/resource');
$connection = $resource->getConnection('core_write');

$q = 'SELECT `entity_id` FROM `catalog_product_entity_varchar` WHERE `value` LIKE \'Not found product%\';';
$productIds = $connection->fetchCol($q);
$productIdsStr = implode(',', $productIds);

echo 'Found ' . count($productIds) . ' products to remove' . "\n";

$q = 'SHOW tables like \'catalog_product_entity%\';';
$tables = $connection->fetchCol($q);

foreach ($tables as $table) {
    if ($table === 'catalog_product_entity_media_gallery_value') {
        continue;
    }

    $q = 'DELETE from ' . $table . ' WHERE entity_id in (' . $productIdsStr . ');';
    echo $q . "\n";
    $connection->query($q);
}

exit('All done...' . "\n");
