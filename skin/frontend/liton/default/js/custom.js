//Combo Category: Make all grid blocks the same height.
jQuery(window).on('load', function(){

	jQuery('.combo-categ-wrapper').each(function(){
		var highestBox = 0;
		var blocksSelector;

		if(jQuery(this).children('.category-product-item').length != 0){
			blocksSelector =  '.grid-item';
		}
		else if(jQuery(this).children('.category-level-5.last').length != 0){
			blocksSelector =  '.category-level-5.last';
		}
		else{
			blocksSelector =  '.category-level-6.last';
		}

		jQuery(this).children(blocksSelector).each(function(){
			if(jQuery(this).height() > highestBox){
				highestBox = jQuery(this).height();
			}
		})
		jQuery(this).children(blocksSelector).height(highestBox);

	});

});

jQuery(document).ready(function() {

	jQuery(".search-right .search-header").click(function(){
		if (jQuery('.search-right .form-search').is(":hidden")) {
			jQuery('.search-right .form-search').show( "slide", {direction: "right" }, 500 );
        } else {
			jQuery('.search-right .form-search').hide( "slide", {direction: "right" }, 500 );
        }
	});
})

jQuery(document).ready(function() {

	jQuery('.homeslider').bxSlider({
		auto:true,
		pause:7000,
		pager:false,
		controls:false,
        adaptiveHeight:true
	});
});

jQuery(document).ready(function() {
	var owl = jQuery(".home-callouts-main");
    owl.owlCarousel({
    itemsCustom : [
		[0, 1],
        [480, 2],
        [767, false],
        [1000, false],
        [1200, false],
        [1400, false],
        [1600, false]
	],
	navigation : true,
	pagination : false
    });
});

/*jQuery(document).ready(function() {
	jQuery('.new-product-slider').bxSlider({
		auto:false,
		pause:7000,
	});
});
*/
jQuery(document).ready(function() {
	var owl = jQuery(".new-product-slider");
    owl.owlCarousel({
    itemsCustom : [
		[0, 1],
        [480, 1],
		[600, 1],
        [767, 1],
        [1000, 2],
        [1200, 2],
        [1400, 2],
        [1600, 2]
	],
	navigation : true,
	pagination : true
    });
});

jQuery(document).ready(function() {
	jQuery(".new-product-sec .bx-wrapper .bx-controls .bx-controls-direction .bx-prev").insertBefore( jQuery(".new-product-sec .bx-wrapper .bx-controls .bx-pager") );
	jQuery(".new-product-sec .bx-wrapper .bx-controls .bx-controls-direction .bx-next").insertAfter( jQuery(".new-product-sec .bx-wrapper .bx-controls .bx-pager") );
});

jQuery(document).ready(function() {
	jQuery('.recent-post-slider').bxSlider({
		slideWidth: 250,
		auto:false,
		pause:7000,
		minSlides: 1,
    	maxSlides: 2,
		slideMargin: 50
	});
});

jQuery(document).ready(function() {
	jQuery(".recent-post-sec .bx-wrapper .bx-controls .bx-controls-direction .bx-prev").insertBefore( jQuery(".recent-post-sec .bx-wrapper .bx-controls .bx-pager") );
	jQuery(".recent-post-sec .bx-wrapper .bx-controls .bx-controls-direction .bx-next").insertAfter( jQuery(".recent-post-sec .bx-wrapper .bx-controls .bx-pager") );
});


jQuery(window).on("load resize",function(e){

	if (jQuery(window).width() > 1200) {
        jQuery('.home-slider-sec .bx-wrapper, .home-slider-sec .bx-wrapper .bx-viewport, .homeslider li').css({
            height: jQuery(window).height()-134+'px',
            width: jQuery(window).width()+'px'
        });
    }
    else if (jQuery(window).width() > 991 && jQuery(window).width() < 1199) {
        jQuery('.home-slider-sec .bx-wrapper, .home-slider-sec .bx-wrapper .bx-viewport, .homeslider li').css({
            height: jQuery(window).height()-134+'px',
            width: jQuery(window).width()+'px'
        });
    }
    else if (jQuery(window).width() > 768 && jQuery(window).width() < 990) {
        jQuery('.home-slider-sec .bx-wrapper, .home-slider-sec .bx-wrapper .bx-viewport, .homeslider li').css({
            height: jQuery(window).height()-124+'px',
            width: jQuery(window).width()+'px'
        });
    }
    else if (jQuery(window).width() < 767) {
        jQuery('.home-slider-sec .bx-wrapper, .home-slider-sec .bx-wrapper .bx-viewport, .homeslider li').css({
            height: jQuery(window).height()-134+'px',
            width: jQuery(window).width()+'px'
        });
		jQuery(".footer .footer-container .footer-newsletter").insertBefore( jQuery(".footer .footer-container .footer-links-sec") );
	}
	else {
		jQuery(".footer .footer-container .footer-newsletter").insertBefore( jQuery(".footer .footer-container .company-info") );
	}
});

jQuery(document).ready(function($) {
    $(".slide-scroll").click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
    });
	
	
});

	
jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.products-grid li.item .product-details-sec').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});

jQuery( document ).ready(function() {
	jQuery('.bredcumb-setion').after(jQuery('.category-section'));
	jQuery('.breadcrumb').after(jQuery('body.catalogsearch-result-index .page-title'));
	jQuery('.bredcumb-setion').after(jQuery('.product-view .top-section'));
	
});

jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.cateegory-listing .sub-details').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});

jQuery(document).ready(function() {
	var owl = jQuery(".product-essential .product-img-box .more-views ul");
    owl.owlCarousel({
    itemsCustom : [
		[0, 2],
        [480, 2],
		[600, 3],
        [767, 4],
        [1000, 4],
        [1200, 4],
        [1400, 4],
        [1600, 4]
	],
	navigation : true,
	pagination : false
    });
});

jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.product-view .description-other1').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});

jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.product-view .description-other2').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});

jQuery(document).ready(function() {
	var owl = jQuery("#block-related");
    owl.owlCarousel({
    itemsCustom : [
		[0, 1],
        [480, 2],
        [767, 3],
        [1000, 4],
        [1200, 4],
        [1400, 4],
        [1600, 4]
	],
	navigation : true,
	pagination : false
    });
});

jQuery(document).ready(function() {
	var owl = jQuery("#upsell-product-table");
    owl.owlCarousel({
    itemsCustom : [
		[0, 1],
        [480, 2],
        [767, 3],
        [1000, 4],
        [1200, 4],
        [1400, 4],
        [1600, 4]
	],
	navigation : true,
	pagination : false
    });
});

jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('#upsell-product-table .product-details-sec').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});
jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('#block-related .product-details-sec').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});

jQuery(window).on("load resize",function(e){
	if ( jQuery(window).width() <= 767 ) {
		jQuery(".product-view .order-config-sec h2").after(jQuery('.product-view .order-config-sec .notes-text'));	
	}
	else {
		jQuery(".product-view .product-options-bottom").append(jQuery('.product-view .order-config-sec .notes-text'));	
	}
});

jQuery(document).ready(function() {
	var owl = jQuery("#crosssell-products-list");
    owl.owlCarousel({
    itemsCustom : [
		[0, 1],
        [480, 2],
        [767, 3],
        [1000, 4],
        [1200, 4],
        [1400, 4],
        [1600, 4]
	],
	navigation : true,
	pagination : false
    });
});
jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('#crosssell-products-list .product-details-sec').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});
jQuery(document).ready(function() {
	jQuery(".bredcumb-setion .breadcrumb").after(jQuery('.account-login .page-title, .account-create .page-title, .customer-account-forgotpassword .page-title, .my-account .page-title'));
	jQuery(".bredcumb-setion").after(jQuery('.contact-title'));
	jQuery(".bredcumb-setion .breadcrumb").after(jQuery('.cms-page-view .page-title'));
	jQuery(".cms-content-img").after(jQuery('.cms-page-view .page-title'));
	jQuery(".category-section").after(jQuery('.top-filter-section'));
	jQuery(".catalogsearch-result-index .bredcumb-setion").after(jQuery('.top-filter-section'));
	jQuery(".breadcrumb").after(jQuery('#fullcontent h1').addClass('wishlist-title'));
});

jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.team-secton .team-lits').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});

jQuery(window).load(function(){
	var $height = jQuery('.cms-banner-img').height();
	var $catboxheight = $height;
	jQuery(".cms-content-img").css('height', $catboxheight);
	jQuery(".cms-banner-img").css('display', 'none');
});


jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.company-brand-section .company-brand-sectionbg ul li .brand-img').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});


jQuery(document).ready(function() {
	var owl = jQuery(".company-brand-sectionbg .company-brand-slider");
    owl.owlCarousel({
    itemsCustom : [
		[0, 1],
        [480, 1],
        [767, 2],
        [1000, 4],
        [1200, 4],
        [1400, 4],
        [1600, 4]
	],
	navigation : true,
	pagination : false
    });
});

jQuery(document).ready(function() {
	var owl = jQuery(".postWrapper .post-image-slider > div");
    owl.owlCarousel({
    itemsCustom : [
		[0, 1],
        [480, 2],
        [767, 2],
		[992, 3],
        [1000, 4],
        [1200, 4],
        [1400, 4],
        [1600, 4]
	],
	navigation : true,
	pagination : false
    });
});



jQuery(window).load(function(){
	// Fade in images so there isn't a color "pop" document load and then on window load
	jQuery(".blog-list-sec .postWrapper .post-img img, .postWrapper .post-image-slider img, .blog-post-view .postWrapper .post-img img, .company-bottom-sec ul li .img img, .company-brand-section .company-brand-sectionbg ul li .brand-img .brand-img-center img").animate({opacity:1});
	// clone image
	jQuery('.blog-list-sec .postWrapper .post-img img, .postWrapper .post-image-slider img, .blog-post-view .postWrapper .post-img img, .company-bottom-sec ul li .img img, .company-brand-section .company-brand-sectionbg ul li .brand-img .brand-img-center img').each(function(){
		var el = jQuery(this);
		el.css({"position":"absolute"}).wrap("<div class='img_wrapper' style='display: block;'>").clone().addClass('img_grayscale').css({"position":"absolute","z-index":"998","opacity":"1"}).insertBefore(el).queue(function(){
			var el = jQuery(this);
			el.parent().css({"width":this.width,"height":this.height});
			el.dequeue();
		});
		this.src = grayscale(this.src);
	});
	// Fade image 
	jQuery('.blog-list-sec .postWrapper .post-img img, .postWrapper .post-image-slider img, .blog-post-view .postWrapper .post-img img, .company-bottom-sec ul li .img img, .company-brand-section .company-brand-sectionbg ul li .brand-img .brand-img-center img').mouseover(function(){
		jQuery(this).parent().find('img:first').stop().animate({opacity:0});
	})
	jQuery('.img_grayscale').mouseout(function(){
		jQuery(this).stop().animate({opacity:1});
	});		
});
// Grayscale w canvas method
function grayscale(src){
	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext('2d');
	var imgObj = new Image();
	imgObj.src = src;
	canvas.width = imgObj.width;
	canvas.height = imgObj.height; 
	ctx.drawImage(imgObj, 0, 0); 
	var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
	for(var y = 0; y < imgPixels.height; y++){
		for(var x = 0; x < imgPixels.width; x++){
			var i = (y * 4) * imgPixels.width + x * 4;
			var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
			imgPixels.data[i] = avg; 
			imgPixels.data[i + 1] = avg; 
			imgPixels.data[i + 2] = avg;
		}
	}
	ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	return canvas.toDataURL();
}

jQuery(document).ready(function() {
	jQuery(".top-filter-section h3").click(function () {
		if (jQuery(this).hasClass('closed')) {
			jQuery(this).removeClass('closed').addClass('open');
			jQuery(this).next().toggle().removeClass('show-sub').addClass('hide-sub');
		} else {
			jQuery(this).removeClass('open').addClass('closed');
			jQuery(this).next().toggle().removeClass('hide-sub').addClass('show-sub');
		}
	});
});

jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.product-options .optionlist').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});


jQuery(document).ready(function() {
	jQuery(".product-view .product-options dt").click(function () {
		if (jQuery(this).hasClass('closed')) {
			jQuery(this).removeClass('closed').addClass('open');
			jQuery(this).next().slideToggle().removeClass('show-sub').addClass('hide-sub');
		} else {
			jQuery(this).removeClass('open').addClass('closed');
			jQuery(this).next().slideToggle().removeClass('hide-sub').addClass('show-sub');
		}
	});	
});

jQuery(window).load(function(){
	var $ProBannerImg = jQuery('.top-section .product-banner img').height();
	var $ProBannerImgFinal = $ProBannerImg;
	jQuery(".top-section .product-banner").css('height', $ProBannerImg);
	jQuery('.top-section .product-banner img').css('display', 'none');
});



jQuery(function(){
   jQuery('.product-essential .product-img-box .more-views ul li a').click(function(){
      var item=jQuery(this);
       jQuery('.more-views ul li a').removeClass('zoomThumbActive');
       item.addClass("zoomThumbActive")
    });
});

jQuery(document).ready(function($) {
	jQuery('.select-store-page .state-list ul li a').click(function(){
		var CodeNew = jQuery(this).attr('href').replace('#', '');;
		jQuery('.select-store-page .all-state-list').css('display', 'none');
		jQuery(".select-store-page").find('[id="'+[CodeNew]+'"]').css('display', 'block');
	});
});

jQuery(function() {
	jQuery('#mobile-show-store').change(function() {
		var OnChangeVal = jQuery('#mobile-show-store').val();
		jQuery('.select-store-page .all-state-list').hide();
		jQuery('#' + jQuery(this).val()).show();
	}).change(); 
});


jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.product-view .product-shop .top-features-sec ul li').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});

jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.cateegory-listing .catgory-title-img div.cat-img').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});

jQuery(document).ready(function($) {
	$('.product-view .order-configure-details #product-options-wrapper ul li input').click(function(){
		$(this).parent().parent().find('li').removeClass("active");
		$(this).parent().addClass("active");
	});
});

jQuery(window).on("load resize",function(e){
	var max = 0;
	jQuery('.product-view .order-configure-details #product-options-wrapper .option').each(function() {
		jQuery(this).height('auto');
		var h = jQuery(this).height();
		max = Math.max(max, h);
		}).height(max);
});



jQuery( document ).ready(function() {
	jQuery("ul.navbar-nav li.parent").append("<span class='arrow'></span>")
	jQuery("ul.navbar-nav li.parent span.arrow").click(function() {
		if (jQuery(this).prev().is(":hidden")) {
         	jQuery(this).addClass('active');
			jQuery(this).prev().slideDown();
        } else {
			jQuery(this).removeClass('active');
         	jQuery(this).prev().slideUp();
        }
	});
});


jQuery(document).ready(function() {
jQuery(".toolbar-bottom .pager .sort-by .select-field").find("option:contains('Position')").remove();
});


jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop() > 1){  
        jQuery('header').addClass("sticky");
		jQuery("header.sticky .header-top-sec").hide(300);
		
    }
    else{
        jQuery('header').removeClass("sticky");
		jQuery("header .header-top-sec").show();
    }
});

jQuery(document).ready(function () {
    jQuery('#itoris-wishlist-popup-box .itoris-wishlist-button-select').click(function () {
        if (jQuery("#product-options-wrapper .validation-advice").text()) {
            jQuery("#itoris-wishlist-popup-box").hide();
        }
    });
});

jQuery(document).on('click', '.product-new-btn li a[href^="#"]', function(e) {
    var id = jQuery(this).attr('href');
    var $id = jQuery(id);
    if ($id.length === 0) {
        return;
    }
    e.preventDefault();
    var pos = jQuery(id).offset().top;
    jQuery('body, html').animate({scrollTop: pos});
});