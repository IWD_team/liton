<?php

class Cminds_Positions_Adminhtml_GoalsController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction(){
        $this->_title($this->__('Goals Management'));
        $this->loadLayout();
        $this->_setActiveMenu('system');
        $this->renderLayout();
    }

    public function editAction()
    {
        $goal_id = $this->getRequest()->getParam('id', null);
        $model = Mage::getModel('cminds_positions/customergoal')->load($goal_id);

        if ($model) {
            Mage::register('goal_data', $model);
        }

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('cminds_positions/adminhtml_goals_container_edit'));
        $this->renderLayout();
    }

    public function deleteAction() {
        if ($goalId = $this->getRequest()->getParam('id', false)) {
            $field = Mage::getModel('cminds_positions/customergoal');
            $field->load($goalId);

            if (!$field->getId()) {
                $this->_getSession()->addError(
                    $this->__('This goal no longer exists.')
                );
            }

            try {
                $field->delete();

                $this->_getSession()->addSuccess(
                    $this->__('Goal has bees deleted successfuly.')
                );
                return $this->_redirect(
                    '*/*/index'
                );
            } catch(Exception $e) {
                $this->_getSession()->addError(
                    $this->__('Can not delete this goal.')
                );
                return $this->_redirect(
                    '*/*/index'
                );
            }
        }

        return $this->_redirect(
            '*/*/index'
        );
    }

    public function saveAction() {
        $params = $this->getRequest()->getParams();

        try {
            $goalModel = Mage::getModel('cminds_positions/customergoal');

            switch($params['type_id']){
                case 0:
                    $this->_getSession()->addError($this->__('Please select goal type'));

                    return $this->_redirect(
                        '*/*/index'
                    );
                    break;
                case 1:
                    unset($params['region_id']);
                    unset($params['office_id']);

                    if(isset($params['customer_id'])){
                        $customer = Mage::getModel('customer/customer')->load($params['customer_id']);

                        if($customer->getId()){
                            $salesrepId = $customer->getSalesrepRepId();
                            $salesrep = Mage::getModel('admin/user')->load($salesrepId);

                            if($salesrep->getId()){
                                $salesrepPositionId = $salesrep->getSalesrepPositionId();

                                $position = Mage::getModel('cminds_positions/position')->load($salesrepPositionId);

                                if($position->getId()){
                                    $params['office_id'] = $position->getId();

                                    if($position->getParentId() != 0 || $position->getParentId() != null){
                                        $params['region_id'] = $position->getParentId();
                                    }
                                }
                            }
                        }

                    } else {
                        $this->_getSession()->addError($this->__('Please select customer'));

                        return $this->_redirect(
                            '*/*/index'
                        );
                    }

                    break;
                case 2:
                    unset($params['customer_id']);

                    $selectedRegionId = $params['region_id'];

                    if(isset($params['office_id'][$selectedRegionId]) && $params['office_id'][$selectedRegionId] != 0) {
                        $regionOffice = $params['office_id'][$selectedRegionId];

                        $params['office_id'] = $regionOffice;
                    } else {
                        $this->_getSession()->addError($this->__('Please select office'));

                        return $this->_redirect(
                            '*/*/index'
                        );
                    }
                    break;
                case 3:
                    unset($params['customer_id']);
                    unset($params['office_id']);
                    break;
            }

            if(isset($params['id'])){
                $goalModel->load($params['id']);
            } else {
                $goalModel
                    ->setId(null);
            }

            $goalModel
                ->addData($params)
                ->save();

            $this->_getSession()->addSuccess($this->__('The goal has been saved.'));

            return $this->_redirect(
                '*/*/index'
            );
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            return $this->_redirect(
                '*/*/index'
            );
        }
    }

    protected function _isAllowed()
    {
        return true;
    }

}