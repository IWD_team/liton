<?php
class Cminds_Positions_Model_Source_OfficesList {

    public static function toOptionArray() {

        $availablePositions = Mage::helper('cminds_positions')->getAllSubordinates();

        $filteredPositionTypes = Mage::helper('cminds_positions')
            ->getPositionTypes()
            ->addFieldToFilter('id', array('in' => $availablePositions));

        $result   = array();

        $offices = array();
        foreach ($filteredPositionTypes AS $positionType){

            foreach (Mage::helper('cminds_positions')->getPosition($positionType->getId()) as $position){
                $offices[] = array('value' => $position->getId(), 'label' => $position->getName());
            }
            $result[] = array('label'=>$positionType->getName(), 'value' => $offices);
            $offices = array();
        }

        return $result;
    }
}
