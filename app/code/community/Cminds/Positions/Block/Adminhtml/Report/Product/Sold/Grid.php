<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Report Sold Products Grid Block
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Cminds_Positions_Block_Adminhtml_Report_Product_Sold_Grid extends Mage_Adminhtml_Block_Report_Grid
{
    /**
     * Sub report size
     *
     * @var int
     */
    protected $_subReportSize = 0;

    /**
     * Initialize Grid settings
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);
        $this->setTemplate('cminds_positions/report/product/grid/form.phtml');
        $this->setUseAjax(false);
        $this->setCountTotals(true);
        $this->setId('gridProductsSold');
    }


    /**
     * Prepare collection object for grid
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        $collection = $this->getCollection()
            ->initReport('cminds_positions/report_product_sold_collection');

        Mage::register('report_region',$this->getFilter('report_region'));
        Mage::register('region_office',$this->getFilter('report_office'));
        Mage::register('region_customer',$this->getFilter('report_customer'));

        return $collection;
    }

    /**
     * Prepare Grid columns
     *
     * @return Mage_Adminhtml_Block_Report_Product_Sold_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header'    =>Mage::helper('reports')->__('Product Name'),
            'index'     =>'order_items_name'
        ));

        $this->addColumn('product_sku', array(
            'header'    =>Mage::helper('reports')->__('Root Product SKU'),
            'index'     =>'product_sku'
        ));

        $this->addColumn('main_category', array(
            'header'    =>Mage::helper('reports')->__('Product Main Category'),
            'index'     =>'order_items_name',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Product_Sold_Renderer_Category'
        ));

        $this->addColumn('ordered_sum', array(
            'header'    =>Mage::helper('reports')->__('Sales'),
            'index'     =>'ordered_sum'
        ));

        $this->addColumn('invoiced_sum', array(
            'header'    =>Mage::helper('reports')->__('Invoices'),
            'index'     =>'invoiced_sum'
        ));

        $this->addColumn('ordered_qty', array(
            'header'    =>Mage::helper('reports')->__('Quantity Ordered'),
            'width'     =>'120px',
            'align'     =>'right',
            'index'     =>'ordered_qty',
            'total'     =>'sum',
            'type'      =>'number'
        ));

        $this->addExportType('*/*/exportSoldCsv', Mage::helper('reports')->__('CSV'));
        $this->addExportType('*/*/exportSoldExcel', Mage::helper('reports')->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getPositionsHelper(){
        return Mage::helper('cminds_positions');
    }

    protected function getSubordinateOffices(){
        $helper = $this->getPositionsHelper();
        $offices = $helper->getCurrentAdminOffices();

        $positions = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('id', array('in' => $offices));
		
		$positions->getSelect()->order('name DESC');
        $officesArray = array();
        foreach($positions as $position){
            $officesArray[] = array('value' => $position->getId(), 'label' => $position->getName(), 'parent_id' => $position->getParentId());
        }
        return $officesArray;
    }

    public function getSubordinateRegions(){
        $helper = $this->getPositionsHelper();
        $regions = $helper->getCurrentAdminRegions();

        $positions = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('id', array('in' => $regions));
		
		$positions->getSelect()->order('name DESC');
        $regionsArray = array();
        foreach($positions as $position){
            $regionsArray[] = array('value' => $position->getId(), 'label' => $position->getName());
        }
        return $regionsArray;
    }

    public function canDisplayRegions(){
        $canDisplay = false;

        if($this->getPositionsHelper()->isTopLevel()){
            $canDisplay = true;
        }

        return $canDisplay;
    }

    public function canDisplayOffice(){
        $canDisplay = false;

        if($this->getPositionsHelper()->isRegional()){
            $canDisplay = true;
        }

        return $canDisplay;
    }

    public function getFilterCustomersUrl(){
        return $this->getUrl('*/*/filteredCustomers');
    }

    public function getSalesrepCustomers(){
        $helper = $this->getPositionsHelper();
        $officeId = $helper->getCurrentAdmin()->getSalesrepPositionId();
        $adminIds = $helper->getAdminIdsByOfficeId($officeId);
        $customersCollection = $helper->getCustomersIdsByAssignedRepIds($adminIds);
$customersCollection->addAttributeToSort('firstname', 'DESC');
        $result = '';
        $result .= '<option value="">Please select</option>';
        foreach($customersCollection as $customer){
            $customerData = Mage::getModel('customer/customer')->load($customer->getId());
            $result .= '<option value="' . $customer->getId() . '">' . $customerData->getName() . '</option>';
        }

        return $result;
    }
}
