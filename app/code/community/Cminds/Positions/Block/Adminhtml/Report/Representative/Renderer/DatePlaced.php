<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_DatePlaced extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {

        $created = $row->getData($this->getColumn()->getIndex());

        if ($created != '') {
            $short = date("l jS F", strtotime($created));
        } else {
            $short = '-';
        }

        return '<span>' . $short . '</span>';


    }
}
