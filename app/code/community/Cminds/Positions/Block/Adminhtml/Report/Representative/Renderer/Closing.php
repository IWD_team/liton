<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_Closing
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        if ($row->getData('is_successful')) {
            return '<div style="width: 100%; background-color: #C1FFC1;">' . $row->getData('closing_units') . '</div>';
        } else {
            return '<div style="width: 100%; background-color: #FFCBCB;">' . $row->getData('closing_units') . '</div>';
        }
    }
}
