<?php
class Cminds_Positions_Block_Adminhtml_Report_Representative_Downline extends Cminds_Positions_Block_Adminhtml_Report_Representative_Abstract {

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('sales_downline');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('salesrep/salesrep')->getCollection();
        $collection->getSelect()
            ->joinLeft(
                array(
                    'order'=> 'sales_flat_order'),
                'order.entity_id = main_table.order_id',
                array(
                    'order.created_at AS date_placed',
                    "CONCAT(order.customer_firstname, ' ', order.customer_lastname) AS customer_name",
                    "order.status",
                    "order.increment_id",
                    "order.base_currency_code AS currency",
                    "order.base_grand_total AS sale_amount",
                )
            )
            ->where('main_table.rep_id = ?', $this->getUserId())
            ->where('main_table.is_downline = ?', 1)
            ->where('main_table.rep_commission_status != ?', 'ineligible');

        $filterFrom = $this->getFilter('from');
        $filterTo = $this->getFilter('to');

        if($filterFrom && $filterTo) {
            $date = new Zend_Date($filterFrom, 'd/m/Y');
            $collection->getSelect()->where('order.created_at >= ?', $date->toString('Y-m-d 00:00:00'));
            $date = new Zend_Date($filterTo, 'd/m/Y');
            $collection->getSelect()->where('order.created_at <= ?', $date->toString('Y-m-d'));
        }
        $date = new DateTime('yesterday');

        $subordinatesArray = Mage::helper('cminds_positions')
            ->getAllSubordinates($this->getUserId());

        $canDisplayDownline = Mage::helper('cminds_positions')
            ->canDisplayDownline($subordinatesArray, $date, $this->getUserId());

        if(!$canDisplayDownline) {
            $collection->addFieldToFilter('order.entity_id', 0);
        }
        $this->setCollection($collection);
    }
    protected function _prepareColumns() {
        $baseCurrencyCode = Mage::app()->getStore((int)$this->getParam('store'))->getBaseCurrencyCode();
        $this->addColumn('order_id', array(
            'header'    => Mage::helper('salesrep')->__('Order #'),
            'width'     => '50px',
            'index'     => 'increment_id',
            'type'  => 'number',
			'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_OrderLink',
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('salesrep')->__('Date Placed'),
            'index'     => 'date_placed',
			'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_DatePlaced',
        ));
        $this->addColumn('customer_name', array(
            'header'    => Mage::helper('salesrep')->__('Customer Name'),
            'index'     => 'customer_name',
			'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_Customer',
        ));
        $this->addColumn('Status', array(
            'header'    => Mage::helper('salesrep')->__('Status'),
            'index'     => 'status',
			'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_StatusName',
        ));
        $this->addColumn('Currency', array(
            'header'    => Mage::helper('salesrep')->__('Currency'),
            'index'     => 'currency',
            'type'      => 'currency',
            'currency'  => 'base_currency_code',
        ));
        $this->addColumn('sale_amount', array(
            'header'    => Mage::helper('salesrep')->__('Sale Amount'),
            'index'     => 'sale_amount',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
        ));
        $this->addColumn('rate', array(
            'header'    => Mage::helper('salesrep')->__('Rate'),
            'index'     => 'rep_percentage',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_ManagerCommission'
        ));
        $this->addColumn('gross_status', array(
            'header'    => Mage::helper('salesrep')->__('Downline Commission %'),
            'index'     => 'rep_percentage',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_ManagerCommission'
        ));
        $this->addColumn('gross_commission', array(
            'header'    => Mage::helper('salesrep')->__('Gross Commission'),
            'index'     => 'rep_commission_earned',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
        ));
        return parent::_prepareColumns();
    }
}
