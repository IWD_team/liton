<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_OrderLink extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {

        $value = $row->getData($this->getColumn()->getIndex());

        $order = Mage::getModel('sales/order')->load($value, 'increment_id');
        $id = $order->getId();

        $url = $this->getUrl('adminhtml/sales_order/view',
            array('order_id' => $id));

        return '<a href="' . $url . '" target="_blank" title="View Order">' . $value . '</a>';

    }
}
