<?php
class Cminds_Positions_Block_Adminhtml_Report_Offices_Totals extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Initialize container block settings
     *
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_report_offices_totals';
        $this->_blockGroup = 'cminds_positions';
        $this->_headerText = Mage::helper('reports')->__('Products Ordered');
        parent::__construct();
        $this->_removeButton('add');
    }
}
