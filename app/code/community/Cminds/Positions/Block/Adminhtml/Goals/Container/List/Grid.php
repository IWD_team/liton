<?php
class Cminds_Positions_Block_Adminhtml_Goals_Container_List_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('office_id');
        $this->setId('salesrep_goals_list_grid');
        $this->setDefaultDir('asc');
//        $this->setSaveParametersInSession(true);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('cminds_positions/customergoal')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('office_id', array(
            'header'    => Mage::helper('cminds_positions')->__('Office Name'),
            'width'     => '50px',
            'index'     => 'office_id',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Goals_Container_List_Renderer_Office'
        ));
        $this->addColumn('region_id', array(
            'header'    => Mage::helper('cminds_positions')->__('Region'),
            'index'     => 'region_id',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Goals_Container_List_Renderer_Region'
        ));
        $this->addColumn('customer_id', array(
            'header'    => Mage::helper('cminds_positions')->__('Customer Name'),
            'index'     => 'customer_id',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Goals_Container_List_Renderer_Customer'
        ));
        $this->addColumn('sales_amount', array(
            'header'    => Mage::helper('cminds_positions')->__('Sales'),
            'index'     => 'sales_amount',
        ));
        $this->addColumn('goal_period', array(
            'header'    => Mage::helper('cminds_positions')->__('Goal Period'),
            'index'     => 'goal_period',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Goals_Container_List_Renderer_Period'
        ));
        $this->addColumn('period_target', array(
            'header'    => Mage::helper('cminds_positions')->__('Goal Target'),
            'index'     => 'period_target',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Goals_Container_List_Renderer_Target'
        ));

        if($this->isStoreAdmin() || $this->isTopLevel()) {
            $this->addColumn('action',
                array(
                    'header' => Mage::helper('cminds_positions')->__('Action'),
                    'width' => '100',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('cminds_positions')->__('Edit'),
                            'url' => array('base' => '*/*/edit'),
                            'field' => 'id'
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
                ));
        }
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    public function isStoreAdmin(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }

    public function isTopLevel(){
        return Mage::helper('cminds_positions')->isTopLevel();
    }
}
