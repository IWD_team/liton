<?php

class Cminds_Positions_Block_Adminhtml_Goals_Container_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'cminds_positions';
        $this->_controller = 'adminhtml_goals_container';
        $this->_mode = 'edit';

        $this->removeButton('back');
        $this->removeButton('reset');

    }

    public function getHeaderText() {
        return '';
    }

}