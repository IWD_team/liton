<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('customer_orders_grid');
        $this->setDefaultSort('created_at', 'desc');
        $this->setUseAjax(true);
        if($this->canSeeElement()){
            $this->setFilterVisibility(false);
        }
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('sales/order_grid_collection')
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect('increment_id')
            ->addFieldToSelect('customer_id')
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('grand_total')
            ->addFieldToSelect('order_currency_code')
            ->addFieldToSelect('store_id')
            ->addFieldToSelect('billing_name')
            ->addFieldToSelect('shipping_name')
            ->addFieldToFilter('customer_id', Mage::registry('current_customer')->getId())
            ->setIsCustomerMode(true);

        $collection->getSelect()->joinLeft(array('sales_order_table' => $collection->getTable('sales/order')), 'sales_order_table.entity_id=main_table.entity_id', array('po_number', 'import_id'));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        if($this->canSeeElement()){

            $this->addColumnAfter('import_id', array(
                'header' => Mage::helper('sales')->__('Invoice Number'),
                'index' => 'import_id',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_ImportId',
                'filter_condition_callback' => array($this, 'filterConditionInvoiceIdCallback')
            ),
                'increment_id'
            );

            $this->addColumnAfter('po_number', array(
                'header' => Mage::helper('sales')->__('Business Partner PO'),
                'index' => 'po_number',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_InvoiceNumber',
                'filter_condition_callback' => array($this, 'filterConditionInvoiceNoCallback')
            ),
                'import_id'
            );


            $this->addColumn('created_at', array(
                'header'    => Mage::helper('customer')->__('Purchase On'),
                'index'     => 'created_at',
                'type'      => 'datetime',
            ));

            $this->addColumn('billing_name', array(
                'header'    => Mage::helper('customer')->__('Bill to Name'),
                'index'     => 'billing_name',
            ));

            $this->addColumn('shipping_name', array(
                'header'    => Mage::helper('customer')->__('Shipped to Name'),
                'index'     => 'shipping_name',
            ));

            $this->addColumn('grand_total', array(
                'header'    => Mage::helper('customer')->__('Order Total'),
                'index'     => 'grand_total',
                'type'      => 'currency',
                'currency'  => 'order_currency_code',
            ));

            if (!Mage::app()->isSingleStoreMode()) {
                $this->addColumn('store_id', array(
                    'header'    => Mage::helper('customer')->__('Bought From'),
                    'index'     => 'store_id',
                    'type'      => 'store',
                    'store_view' => true
                ));
            }

            if (Mage::helper('sales/reorder')->isAllow()) {
                $this->addColumn('action', array(
                    'header'    => ' ',
                    'filter'    => false,
                    'sortable'  => false,
                    'width'     => '100px',
                    'renderer'  => 'adminhtml/sales_reorder_renderer_action'
                ));
            }
            parent::_prepareColumns();

            $this->addColumn('increment_id', array(
                'header'=> Mage::helper('sales')->__('Order #'),
                'width' => '80px',
                'type'  => 'text',
                'index' => 'increment_id',
                'column_css_class'=>'no-display',
                'header_css_class'=>'no-display'
            ));
        } else {

            $this->addColumn('increment_id', array(
                'header'=> Mage::helper('sales')->__('Order #'),
                'width' => '80px',
                'type'  => 'text',
                'index' => 'increment_id',
                'column_css_class'=>'no-display',
                'header_css_class'=>'no-display'
            ));
            $this->addColumnAfter('import_id', array(
                'header' => Mage::helper('sales')->__('Invoice Number'),
                'index' => 'import_id',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_ImportId',
                'filter_condition_callback' => array($this, 'filterConditionInvoiceIdCallback')
            ),
                'increment_id'
            );

            $this->addColumnAfter('po_number', array(
                'header' => Mage::helper('sales')->__('Business Partner PO'),
                'index' => 'po_number',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_InvoiceNumber',
                'filter_condition_callback' => array($this, 'filterConditionInvoiceNoCallback')
            ),
                'import_id'
            );

            $this->addColumn('created_at', array(
                'header' => Mage::helper('sales')->__('Purchase Date'),
                'index' => 'created_at',
                'type' => 'datetime',
                'width' => '100px',
                'filter' => false
            ));

            $this->addColumn('shipping_data', array(
                'header' => Mage::helper('sales')->__('Shipping'),
                'index' => 'shipping_data',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Shipment',
                'filter' => false
            ));

            $this->addColumn('base_grand_total', array(
                'header' => Mage::helper('sales')->__('Total Ordered'),
                'index' => 'base_grand_total',
                'type' => 'currency',
                'currency' => 'base_currency_code',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Ordered',
                'filter' => false
            ));

            $this->addColumn('total_invoiced', array(
                'header' => Mage::helper('sales')->__('Total Invoiced'),
                'index' => 'total_invoiced',
                'type' => 'currency',
//                'currency' => 'order_currency_code',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Invoiced',
                'filter' => false
            ));

            $this->addColumn('status', array(
                'header' => Mage::helper('sales')->__('Status'),
                'index' => 'status',
                'type' => 'options',
                'width' => '120px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Status',
                'filter' => false
            ));

            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
                $this->addColumn('action',
                    array(
                        'header' => Mage::helper('sales')->__('Action'),
                        'width' => '50px',
                        'type' => 'action',
                        'getter' => 'getId',
                        'actions' => array(
                            array(
                                'caption' => Mage::helper('sales')->__('View'),
                                'url' => array('base' => '*/sales_order/view'),
                                'field' => 'order_id',
                                'data-column' => 'action',
                            )
                        ),
                        'filter' => false,
                        'sortable' => false,
                        'index' => 'stores',
                        'is_system' => true,
                    ));
            }

            if(!$this->isRegional()) {
                $this->addExportType('*/*/exportCsv',
                    Mage::helper('sales')->__('CSV'));
                $this->addExportType('*/*/exportExcel',
                    Mage::helper('sales')->__('Excel XML'));
            }
        }
    }

    protected function filterConditionInvoiceNoCallback($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            // if($column->getId() != 'invoice_number') {
            return $this;
            // }
        }
        if (empty($value) && $column->getId() != 'po_number') {
            return $this;
        } else {
            if($column->getId() == 'po_number') {
                $filteredCollection = Mage::getModel('sales/order')
                    ->getCollection()
                    ->addFieldToFilter(
                        'po_number', array('like' => '%' . $value . '%')
                    );

                $invoiceNumberArray = array();
                foreach($filteredCollection as $order){
                    $invoiceNumberArray[] = $order->getId();
                }
                $collection
                    ->addFieldToFilter(
                        'entity_id', array('in' => $invoiceNumberArray)
                    );
            }
        }
        return $this;
    }

    protected function filterConditionInvoiceIdCallback($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            // if($column->getId() != 'invoice_number') {
            return $this;
            // }
        }
        if (empty($value) && $column->getId() != 'import_id') {
            return $this;
        } else {
            if($column->getId() == 'import_id') {
                $filteredCollection = Mage::getModel('sales/order')
                    ->getCollection()
                    ->addFieldToFilter(
                        'import_id', array('like' => '%' . $value . '%')
                    );

                $invoiceNumberArray = array();
                foreach($filteredCollection as $order){
                    $invoiceNumberArray[] = $order->getId();
                }
                $collection
                    ->addFieldToFilter(
                        'entity_id', array('in' => $invoiceNumberArray)
                    );
            }
        }
        return $this;
    }

    public function getMainButtonsHtml()
    {
        $html = '';
        if($this->canSeeElement()) {
            if ($this->getFilterVisibility()) {
                $html .= $this->getResetFilterButtonHtml();
                $html .= $this->getSearchButtonHtml();
            }
        }
        return $html;
    }
    /**
     * Check if admin user is store admin.
     */
    public function canSeeElement(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }

    public function isRegional(){
        $currentUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currentUser->getSalesrepPositionId());


        if($positionModel->getId()){
            $childrenCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id', array('eq' => $currentUser->getSalesrepPositionId()));
            if($positionModel->getParentId() != 0 && $positionModel->getParentId() != null && $childrenCollection->getSize()){
                return true;
            }
        }
        return false;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/sales_order/view', array('order_id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/orders', array('_current' => true));
    }
}
