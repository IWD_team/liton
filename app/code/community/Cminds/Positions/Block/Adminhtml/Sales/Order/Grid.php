<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Retrieve collection class
     *
     * @return string
     */
    protected function _getCollectionClass()
    {
        return 'sales/order_grid_collection';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        if($this->canSeeElement()){
            parent::_prepareColumns();
            $this->addColumn('real_order_id', array(
                'header'=> Mage::helper('sales')->__('Order #'),
                'width' => '80px',
                'type'  => 'text',
                'index' => 'increment_id',
                'column_css_class'=>'no-display',
                'header_css_class'=>'no-display'
            ));

            $this->addColumnAfter('import_id', array(
                'header' => Mage::helper('sales')->__('Invoice Number'),
                'index' => 'import_id',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_ImportId',
                'filter_condition_callback' => array($this, 'filterConditionInvoiceIdCallback')
            ),
                'real_order_id'
            );

            $this->addColumnAfter('po_number', array(
                'header' => Mage::helper('sales')->__('Business Partner PO'),
                'index' => 'po_number',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_InvoiceNumber',
                'filter_condition_callback' => array($this, 'filterConditionInvoiceNoCallback')
            ),
                'import_id'
            );

            $this->addColumnAfter('job_name', array(
                'header' => Mage::helper('sales')->__('Job Name'),
                'index' => 'entity_id',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_JobName'
            ),
                'invoice_number'
            );
        } else {

            $this->addColumn('real_order_id', array(
                'header'=> Mage::helper('sales')->__('Order #'),
                'width' => '80px',
                'type'  => 'text',
                'index' => 'entity_id',
                'column_css_class'=>'no-display',
                'header_css_class'=>'no-display'
            ));

            $this->addColumn('import_id', array(
                'header' => Mage::helper('sales')->__('Invoice Number'),
                'index' => 'import_id',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_ImportId',
                'filter_condition_callback' => array($this, 'filterConditionInvoiceIdCallback')
            ));

            $this->addColumn('invoice_number', array(
                'header' => Mage::helper('sales')->__('Business Partner PO'),
                'index' => 'invoice_number',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_InvoiceNumber',
                'filter_condition_callback' => array($this, 'filterConditionInvoiceNoCallback')
            ));

            $this->addColumnAfter('job_name', array(
                'header' => Mage::helper('sales')->__('Job Name'),
                'index' => 'entity_id',
                'width' => '100px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_JobName'
            ),
                'invoice_number'
            );

            $this->addColumn('created_at', array(
                'header' => Mage::helper('sales')->__('Purchase Date'),
                'index' => 'created_at',
                'type' => 'datetime',
                'width' => '100px',
                'format'    => 'F',
            ));

            $this->addColumn('billing_name', array(
                'header' => Mage::helper('sales')->__('Customer Name'),
                'index' => 'billing_name',
            ));

            $this->addColumn('shipping_data', array(
                'header' => Mage::helper('sales')->__('Shipment Tracking'),
                'index' => 'shipping_data',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_Shipment'
            ));

            $this->addColumn('base_grand_total', array(
                'header' => Mage::helper('sales')->__('Total Ordered'),
                'index' => 'base_grand_total',
                'type' => 'currency',
                'currency' => 'base_currency_code',
            ));

            $this->addColumn('total_invoiced', array(
                'header' => Mage::helper('sales')->__('Total Invoiced'),
                'index' => 'total_invoiced',
                'type' => 'currency',
//                'currency' => 'order_currency_code',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_Invoiced'
            ));

            $this->addColumn('status', array(
                'header' => Mage::helper('sales')->__('Status'),
                'index' => 'status',
                'type' => 'options',
                'width' => '120px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_Status',
                'filter_condition_callback' => array($this, 'filterConditionStatusCallback'),
                'options'   => array(
                    0 => Mage::helper('sales')->__('Shipped'),
                    1 => Mage::helper('sales')->__('Not Shipped'),
                    2 => Mage::helper('sales')->__('Partically Shipped')
                )
            ));

            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
                $this->addColumn('action',
                    array(
                        'header' => Mage::helper('sales')->__('Action'),
                        'width' => '50px',
                        'type' => 'action',
                        'getter' => 'getId',
                        'actions' => array(
                            array(
                                'caption' => Mage::helper('sales')->__('View'),
                                'url' => array('base' => '*/sales_order/view'),
                                'field' => 'order_id',
                                'data-column' => 'action',
                            )
                        ),
                        'filter' => false,
                        'sortable' => false,
                        'index' => 'stores',
                        'is_system' => true,
                    ));
            }

            if(!$this->isRegional()) {
                $this->addExportType('*/*/exportCsv',
                    Mage::helper('sales')->__('CSV'));
                $this->addExportType('*/*/exportExcel',
                    Mage::helper('sales')->__('Excel XML'));
            }
        }
    }
	
    protected function filterConditionStatusCallback($collection, $column){
        $d = 't';
        $value = $column->getFilter()->getValue();

        if (empty($value) && $column->getId() != 'status') {
            return $this;
        } else {
            if($column->getId() == 'status') {
                $filteredOrders = $this->getOrdersByShippingStatus($value);

                $collection
                    ->addFieldToFilter(
                        'entity_id', array('in' => $filteredOrders)
                    );
            }
        }
        return $this;
    }


    protected function filterConditionInvoiceNoCallback($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            // if($column->getId() != 'invoice_number') {
            return $this;
            // }
        }
        if (empty($value) && $column->getId() != 'po_number') {
            return $this;
        } else {
            if($column->getId() == 'po_number') {
                $filteredCollection = Mage::getModel('sales/order')
                    ->getCollection()
                    ->addFieldToFilter(
                        'po_number', array('like' => '%' . $value . '%')
                    );

                $invoiceNumberArray = array();
                foreach($filteredCollection as $order){
                    $invoiceNumberArray[] = $order->getId();
                }
                $collection
                    ->addFieldToFilter(
                        'entity_id', array('in' => $invoiceNumberArray)
                    );
            }
        }
        return $this;
    }

    protected function filterConditionInvoiceIdCallback($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            // if($column->getId() != 'invoice_number') {
            return $this;
            // }
        }
        if (empty($value) && $column->getId() != 'import_id') {
            return $this;
        } else {
            if($column->getId() == 'import_id') {
                $filteredCollection = Mage::getModel('sales/order')
                    ->getCollection()
                    ->addFieldToFilter(
                        'import_id', array('like' => '%' . $value . '%')
                    );

                $invoiceNumberArray = array();
                foreach($filteredCollection as $order){
                    $invoiceNumberArray[] = $order->getId();
                }
                $collection
                    ->addFieldToFilter(
                        'entity_id', array('in' => $invoiceNumberArray)
                    );
            }
        }
        return $this;
    }


    protected function _prepareMassaction()
    {
        if($this->canSeeElement()){
            return parent::_prepareMassaction();
        } else {
            return $this;
        }
    }

    /**
     * Check if admin user is store admin.
     */
    public function canSeeElement(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }

    public function isRegional(){
        $currentUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currentUser->getSalesrepPositionId());


        if($positionModel->getId()){
            $childrenCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id', array('eq' => $currentUser->getSalesrepPositionId()));
            if($positionModel->getParentId() != 0 && $positionModel->getParentId() != null && $childrenCollection->getSize()){
                return true;
            }
        }
        return false;
    }

    public function getOrdersByShippingStatus($type){
        $salesModel = Mage::getModel('sales/order')->getCollection();

        $ordersArray = array();
        if($type == 0){
            foreach($salesModel as $order) {
                $allItems = $order->getAllVisibleItems();

                $isFullyShipped = true;
                foreach ($allItems as $item) {
                    if ($item->getQtyOrdered() != $item->getQtyShipped()) {
                        $isFullyShipped = false;
                    }

                }

                if($isFullyShipped){
                    $ordersArray[] = $order->getId();
                }
            }
        } elseif($type == 2){
            foreach($salesModel as $order) {
                $allItems = $order->getAllVisibleItems();

                $isFullyShipped = true;
                foreach ($allItems as $item) {
                    if ($item->getQtyOrdered() != $item->getQtyShipped()) {
                        $isFullyShipped = false;
                    }

                }

                if(!$isFullyShipped && $order->hasShipments()){
                    $ordersArray[] = $order->getId();
                }
            }
        } elseif($type == 1){
            foreach($salesModel as $order) {
                if(!$order->hasShipments()){
                    $ordersArray[] = $order->getId();
                }
            }
        }

        return $ordersArray;
    }
}
