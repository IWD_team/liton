<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_JobName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $jobName = '';
        $giftcardPayment = Mage::getModel('giftcard/payment')->load($row->getId(), 'order_id');
        if($giftcardPayment->getId()){
            $jobName = $giftcardPayment->getJobName();
        }
        return $jobName;
    }
}