<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_Grid_Renderer_ImportId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $importId = '';
        $orderModel = Mage::getModel('sales/order')->load($row->getId());
        if($orderModel->getId()){
            if($orderModel->getImportId()){
                $importId = $orderModel->getImportId();
            }
        }
        return $importId;
    }
}