<?php

class Cminds_Positions_Block_Adminhtml_Sales_Order_View_Tab_Renderer_Trackingnumber
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $trackingModel = Mage::getModel('sales/order_shipment')->load($row->getId());

        $trackingNumbers = '';
        foreach($trackingModel->getAllTracks() as $track){
            if($track->getId()) {
                $trackingNumbers .= $track->getTitle() .
                    ' ' . $track->getTrackNumber() . "<br/>";
            }
        }

        return $trackingNumbers;
    }
}
