<?php

class Cminds_Positions_Block_Adminhtml_Notifications_Container_List_Renderer_Customer
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $customerId = $row->getId();

        $customerModel = Mage::getModel('customer/customer')->load($customerId);

        return $customerModel->getName();
    }
}