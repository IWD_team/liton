<?php

class Cminds_Positions_Block_Adminhtml_Notifications_Container_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'cminds_positions';
        $this->_controller = 'adminhtml_notifications_container';
        $this->_mode = 'edit';

        $this->removeButton('back');
        $this->removeButton('reset');
        $this->removeButton('save');

        $this->addButton('save', array(
            'label'     => Mage::helper('adminhtml')->__('Filter'),
            'onclick'   => 'editForm.submit();',
            'class'     => 'save',
        ), 1);

    }

    public function getHeaderText() {
        return '';
    }

}