<?php

class Cminds_Positions_Block_Adminhtml_Position_List_Renderer_Parenttype
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $parentTypeId = $row->getData('type_id');

        if($parentTypeId == 0) {
            return Mage::helper('cminds_positions')->__("No Parent Type");
        } else {
            $parent = Mage::getModel('cminds_positions/positiontype')->load($parentTypeId);

            if($parent->getId()) {
                return $parent->getName();
            }
        }
    }
}