<?php
class Cminds_Positions_Helper_Dashboard extends Mage_Checkout_Helper_Data {

    protected $_module_name = 'Cminds_Positions';

    public function getUserId() {
        $p = $this->getFilter('order_admins');

        if(!$p) {
            $p = Mage::registry('admin_user')->getId();
        }

        return $p;
    }

    protected function getFilter($filterName) {
        $filter = Mage::app()->getRequest()->getParam('filter', null);
        if (is_string($filter)) {
            $data = array();
            $filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);

            if(isset($data[$filterName])) {
                return $data[$filterName];
            }

            return false;
        } else if(Mage::registry('cron_filters')) {
            $filters = Mage::registry('cron_filters');

            if(isset($filters[$filterName])) {
                return $filters[$filterName];
            }

            return false;
        } else {
            return false;
        }
    }
}
