<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('customer/entity'),
    'salesrep_notify_when_order_placed',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => true,
        'default'   => 0,
        'comment'   => 'Notify when order placed'
    )
);

$installer->getConnection()->addColumn($installer->getTable('customer/entity'),
    'salesrep_notify_when_order_shipped',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => true,
        'default'   => 0,
        'comment'   => 'Notify when order shipped'
    )
);


$installer->endSetup();