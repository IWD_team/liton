<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'),'import_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Import order id'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'),'email_follower', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Email follower'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'),'po_number', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Po Number'
    ));

$installer->getConnection()
    ->addColumn(
        $installer->getTable('sales/order'),
        'shipping_time',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable'  => false,
            'length'    => 255,
            'after'     => null, // column name to insert new column after
            'comment'   => 'Shipping time'
        )
    );

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('sales_flat_order_shipping_rate')};
CREATE TABLE {$this->getTable('sales_flat_order_shipping_rate')} (
  `rate_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `carrier` varchar(255) DEFAULT NULL,
  `carrier_title` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `method_description` text,
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `parent_id` varchar(255) DEFAULT NULL,
  `error_message` varchar(255) DEFAULT NULL,
  `method_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rate_id`),
  KEY `FK_SALES_QUOTE_SHIPPING_RATE_ADDRESS` (`address_id`),
  KEY `FK_SALES_ORDER_SHIPPING_RATE_ORDER` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('products_skus')};
CREATE TABLE {$this->getTable('products_skus')} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `sku` varchar(255)  NOT NULL ,
  `options_skus` varchar(255)  NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$entity = $this->getEntityTypeId('customer');

$this->addAttribute($entity, 'import_custom_id', array(
    'type' =>  Varien_Db_Ddl_Table::TYPE_TEXT,
    'label' => __('Import customer ID'),
    'input' => 'text',
    'visible' => TRUE,
    'required' => FALSE,
    'default_value' => 1,
    'adminhtml_only' => '1'
));

Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'import_custom_id')
    ->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit','checkout_register'))
    ->save();

$installer->getConnection()
    ->addColumn($installer->getTable('admin/user'),'import_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Import id'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'line_number_type', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Line number type'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'order_entry_date', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Order entry date'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'order_processed_by', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Order processed by'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'order_edited_by', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Order edited by'
    ));


$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'pdd', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATE,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Pdd'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'pdd_buffer', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Pdd buffer'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/invoice'), 'custom_import_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length'    => 255,
        'after'     => null, // column name to insert new column after
        'comment'   => 'Import ID'
    ));

$installer->endSetup();
