<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ENABLED_KEY = 'toe/orderedit/active';
	const MAXIMUM_AVAILABLE_NUMBER = 99999999;
	/**
         * Checks the quote to see if the items allowed are greater than the checkout qty
         * @param Mage_Sales_Model_Order $order Order Object
         * @param type $amount
         * @return Cminds_OrderEdit_Helper_Data
         */
	public function checkQuoteAmount(Mage_Sales_Model_Order $order, $amount)
	{
		if (!$order->getHasError() && ($amount>=self::MAXIMUM_AVAILABLE_NUMBER)) {
			$order->setHasError(true);
			$order->addMessage(
		    $this->__('Some items have quantities exceeding allowed quantities. Please select a lower quantity to checkout.')
		);
	}
		return $this;
	}

    public function getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    public function enabledTaxPercent()
    {
        return Mage::getStoreConfig('toe/orderedit/custom_tax_percent');
    }
        
}

