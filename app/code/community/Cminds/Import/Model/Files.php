<?php
class Cminds_Import_Model_Files extends Mage_Core_Model_Abstract
{
    const FTP_HOST = 'litoneem.nextmp.net';

    const FTP_USER = 'ftpbaan@liton.com';

    const FTP_PASSWORD = 'SickerBussDigestSince22';

    const FTP_FILE_DIR = 'Reports/';

    const FTP_FILE_MOVE_DIR = 'Reports/processed/';

    const FTP_FILE_ORDERS = 'SalesOrder.csv';

    const FTP_FILE_CUSTOMERS = 'customer.csv';

    const FTP_FILE_SALESREPS = 'Customer_SalesRep.csv';

    const LOCAL_FILE_DIR = 'csv/';

    const LOCAL_FILE_ORDERS = 'orders.csv';

    const LOCAL_FILE_CUSTOMERS = 'customers.csv';

    const LOCAL_FILE_SALESREPS = 'salesreps.csv';

    const DEBUG_MODE = true;

    const LOG_FILE = 'cminds_files.log';

    /**
     * FTP connection handler.
     *
     * @var $ftpConnection
     */
    protected $ftpConnection;

    /**
     * Init ftp connection.
     */
    public function _construct()
    {
        try {
            $this->ftpConnection = ftp_connect(self::FTP_HOST);
            ftp_login($this->ftpConnection, self::FTP_USER, self::FTP_PASSWORD);
        } catch (Exception $e) {
            Mage::log('['.$e->getFile().':'.$e->getLine().'] '.$e->getMessage(), null, 'cminds_files_exceptions.log');
        }
    }

    /**
     * Get orders file from ftp server and save on local server.
     */
    public function copyOrdersFile()
    {
        if (!empty($this->ftpConnection)) {
            $localFile = self::LOCAL_FILE_DIR.self::LOCAL_FILE_ORDERS;
            $remoteFile = self::FTP_FILE_DIR.self::FTP_FILE_ORDERS;
            if (ftp_size($this->ftpConnection, $remoteFile) !== -1) {
                if (ftp_get(
                    $this->ftpConnection,
                    $localFile,
                    $remoteFile,
                    FTP_BINARY
                )) {
                    $message = 'Orders file was created.';
                    $this->moveFile($remoteFile);
                } else {
                    $message = 'Orders file was not created.';
                }
            } else {
                $message = 'Orders file doesnt exists on remote server.';
            }
            $this->saveLog($message);
        }
    }

    /**
     * Get customers file from ftp server and save on local server.
     */
    public function copyCustomersFile()
    {
        if (!empty($this->ftpConnection)) {
            $localFile = self::LOCAL_FILE_DIR.self::LOCAL_FILE_CUSTOMERS;
            $remoteFile = self::FTP_FILE_DIR.self::FTP_FILE_CUSTOMERS;
            if (ftp_size($this->ftpConnection, $remoteFile) !== -1) {
                if (ftp_get(
                    $this->ftpConnection,
                    $localFile,
                    $remoteFile,
                    FTP_BINARY
                )) {
                    $message = 'Customers file was created.';
                    $this->moveFile($remoteFile);
                } else {
                    $message = 'Customers file was not created.';
                }
            } else {
                $message = 'Customers file doesnt exists on remote server.';
            }
            $this->saveLog($message);
        }
    }
    /**
     * Get salesreps file from ftp server and save on local server.
     */
    public function copySalesRepsFile()
    {
        if (!empty($this->ftpConnection)) {
            $localFile = self::LOCAL_FILE_DIR.self::LOCAL_FILE_SALESREPS;
            $remoteFile = self::FTP_FILE_DIR.self::FTP_FILE_SALESREPS;
            if (ftp_size($this->ftpConnection, $remoteFile) !== -1) {
                if (ftp_get(
                    $this->ftpConnection,
                    $localFile,
                    $remoteFile,
                    FTP_BINARY
                )) {
                    $message = 'Salesreps file was created.';
                    $this->moveFile($remoteFile);
                } else {
                    $message = 'Salesreps file was not created.';
                }
            } else {
                $message = 'Salesreps file doesnt exists on remote server.';
            }
            $this->saveLog($message);
        }
    }


    /**
     * Save log if debug mode is enabled.
     *
     * @param string $message
     */
    protected function saveLog($message)
    {
        if (self::DEBUG_MODE) {
            Mage::log(date("Y-m-d H:i:s").': '.$message, null, self::LOG_FILE);
        }
    }

    /**
     * Move file to new directory.
     *
     * New file name is generate from old file name + current unix time.
     * If $deletedOldFile === true, source file is remove after move.
     *
     * @param string $sourceFile
     */
    protected function moveFile($sourceFile)
    {
        $this->createFtpMoveDir();
        $sourceFileArray = explode('/', $sourceFile);
        $newFileArray = explode('.', $sourceFileArray[1]);
        $newFileName = $newFileArray[0].'-'.time().'.'.$newFileArray[1];
        $pathToNewFile = self::FTP_FILE_MOVE_DIR.$newFileName;
        if (ftp_rename($this->ftpConnection, $sourceFile, $pathToNewFile)) {
            $message = 'File "'.$sourceFile.'" was moved to new directory.';
        } else {
            $message = 'File "'.$sourceFile.'" was not moved to new directory.';
        }
        $this->saveLog($message);
    }

    /**
     * Create move dir if dosent exists on ftp server.
     */
    protected function createFtpMoveDir()
    {
        if (ftp_nlist($this->ftpConnection, self::FTP_FILE_MOVE_DIR) === false) {
            ftp_mkdir($this->ftpConnection, self::FTP_FILE_MOVE_DIR);
        }
    }
}