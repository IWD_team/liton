<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Model_Order_Address_Total_Subtotal extends Cminds_Import_Model_Order_Address_Total_Abstract
{
    /**
     * Collect address subtotal
     *
     * @param   Mage_Sales_Model_Order_Address $address
     * @return  Mage_Sales_Model_Order_Address_Total_Subtotal
     */
    public function collect(Cminds_Import_Model_Order_Address $address)
    {
    	
        /**
         * Reset subtotal information
         */
        $address->setSubtotal(0);
        $address->setBaseSubtotal(0);
        $address->setTotalQty(0);
        $address->setBaseTotalPriceIncTax(0);
        $address->setGrandTotal(0);
        $address->setBaseGrandTotal(0);
        
        
        
		$order = $address->getOrder();
		

        /**
         * Process address items
         */
        $items = $order->getOrderItems();
		
        foreach ($items as $item) {
            if (!$this->_initItem($address, $item) || $item->getQtyOrdered()<=0) {
                //$this->_removeItem($address, $item);
            }
        }
		
		
        /**
         * Initialize grand totals
         */ 

        $address->setGrandTotal($address->getSubtotal());
        $address->setBaseGrandTotal($address->getBaseSubtotal());
        
        return $this;
    }

    /**
     * Address item initialization
     *
     * @param  $item
     * @return bool
     */
    protected function _initItem($address, $item)
    {
        if ($item instanceof Cminds_Import_Model_Order_Address_Item) {
            $orderItem = $item->getAddress()->getItemById($item->getOrderItemId());
        } else {
            $orderItem = $item;
        }

		if ($orderItem->getParentItem() && $orderItem->isChildrenCalculated()) {
            $finalPrice = $orderItem->getParentItem()->getProduct()->getPriceModel()->getChildFinalPrice(
               $orderItem->getParentItem()->getProduct(),
               $orderItem->getParentItem()->getQty(),
               $orderItem->getProduct(),
               $orderItem->getQty()
            );
            $item->setPrice($finalPrice);
            $item->calcRowTotal();
        }
        else if (!$orderItem->getParentItem()) {
	            $item->calcRowTotal();
	
	            $address->setSubtotal($address->getSubtotal() + $item->getRowTotal());
	
	            $address->setBaseSubtotal($address->getBaseSubtotal() + $item->getBaseRowTotal());
	            $address->setTotalQty($address->getTotalQty() + $item->getQtyOrdered());
        }

        return true;
    }

    /**
     * Remove item
     *
     * @param  $address
     * @param  $item
     * @return Mage_Sales_Model_Order_Address_Total_Subtotal
     */
    protected function _removeItem($address, $item)
    {
        if ($item instanceof Mage_Sales_Model_Order_Item) {
            $address->removeItem($item->getId());
            if ($address->getOrder()) {
                $address->getOrder()->removeItem($item->getId());
            }
        }
        elseif ($item instanceof Cminds_Import_Model_Order_Address_Item) {
            $address->removeItem($item->getId());
            if ($address->getOrder()) {
                $address->getOrder()->removeItem($item->getOrderItemId());
            }
        }

        return $this;
    }
    /**
     * Fetch the total
     * @param Cminds_Import_Model_Order_Address $address
     */
    public function fetch(Cminds_Import_Model_Order_Address $address)
    {
        $address->addTotal(array(
            'code'=>$this->getCode(),
            'title'=>Mage::helper('sales')->__('Subtotal'),
            'value'=>$address->getSubtotal()
        ));
        return $this;
    }
}