<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Model_Order_Address_Item extends Cminds_Import_Model_Order_Item_Abstract
{
    /**
     * Quote address model object
     *
     * @var Mage_Sales_Model_Quote_Address
     */
    protected $_address;
    protected $_order;

    protected function _construct()
    {
        $this->_init('sales/order_address_item');
    }
    /**
     * Before saving, set the order address id
     * @return Cminds_Import_Model_Order_Address_Item
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        if ($this->getAddress()) {
            $this->setOrderAddressId($this->getAddress()->getId());
        }
        return $this;
    }

    /**
     * Declare address model
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Item
     */
    public function setAddress(Cminds_Import_Model_Order_Address $address)
    {
        $this->_address = $address;
        $this->_order   = $address->getOrder();
        return $this;
    }

    /**
     * Retrieve address model
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getAddress()
    {
        return $this->_address;
    }

    /**
     * Retrieve quote model instance
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * Imports the order item into the item
     * @param Cminds_Import_Model_Order_Item $orderItem
     * @return Cminds_Import_Model_Order_Address_Item
     */
    public function importOrderItem(Cminds_Import_Model_Order_Item $orderItem)
    {
        $this->_order = $orderItem->getOrder();
        $this->setOrderItem($orderItem)
            ->setOrderItemId($orderItem->getId())
            ->setProductId($orderItem->getProductId())
            ->setProduct($orderItem->getProduct())
            ->setSku($orderItem->getSku())
            ->setName($orderItem->getName())
            ->setDescription($orderItem->getDescription())
            ->setWeight($orderItem->getWeight())
            ->setPrice($orderItem->getPrice())
            ->setCost($orderItem->getCost());

        if (!$this->hasQty()) {
            $this->setQty($orderItem->getQty());
        }
        $this->setOrderItemImported(true);
        return $this;
    }
    /**
     * Gets options by their code
     * @param string $code
     * @return string 
     */
    public function getOptionBycode($code)
    {
        if ($this->getOrderItem()) {
        	return $this->getOrderItem()->getOptionBycode($code);
        }
        return null;
    }
}