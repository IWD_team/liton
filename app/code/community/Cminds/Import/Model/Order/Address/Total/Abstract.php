<?php
/**
 * CreativeMindsSolutions
 */
abstract class Cminds_Import_Model_Order_Address_Total_Abstract
{
    /**
     * Code for shipping
     */
    protected $_code;

    /**
     * Set the code
     */
    public function setCode($code)
    {
        $this->_code = $code;
        return $this;
    }

    /**
     * Get the code
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * Collect totals
     */
    public function collect(Cminds_Import_Model_Order_Address $address)
    {
        return $this;
    }
    /**
     * Fetch totals
     */
    public function fetch(Cminds_Import_Model_Order_Address $address)
    {
        return array();
    }
}
