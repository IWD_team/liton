<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Model_Resource_Order_Address_Rate_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Constructs the SQL
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('import/order_address_rate');
    }
}