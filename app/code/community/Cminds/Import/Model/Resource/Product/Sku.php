<?php
class Cminds_Import_Model_Resource_Product_Sku extends Mage_Core_Model_Resource_Db_Abstract{
    protected function _construct()
    {
        $this->_init('import/product_sku', 'id');
    }
}