<?php

class Cminds_Import_Model_Orders extends Mage_Core_Model_Abstract
{
    const CSV_URL = '/csv/orders.csv';

    /**
     * @var array
     */
    protected $ordersRepository = array();

    /**
     * Magento website id.
     *
     * @var int
     */
    protected $websiteId;

    /**
     * Magento store id
     *
     * @var int
     */
    protected $store;

    /**
     * Quote
     *
     * @var
     */
    protected $quote;

    /**
     * Array with customer data from csv.
     *
     * @var array
     */
    protected $ordersArr = array();

    /**
     * Array with products skus dosent found.
     *
     * @var array
     */

    protected $skusNotFound = array();

    /**
     * Array with customer data from csv.
     *
     * @var array
     */
    protected $orders = array();

    /**
     * Array with order to cancell.
     *
     * @var array
     */
    protected $ordersToCancell = array();

    /**
     * Product instance.
     *
     * @var array
     */
    protected $product = null;

    /**
     * Product option instance.
     *
     * @var array
     */
    protected $productOption = array();

    /**
     * Array with invoices data from csv.
     *
     * @var array
     */
    protected $invoicesArr = array();

    /**
     * Array with shipments data from csv.
     *
     * @var array
     */
    protected $shipmentsArr = array();

    /**
     * Missing salesreps.
     *
     * @var array
     */
    protected $missingSealereps = array();

    protected $shippingCharges = array();

    protected $csv = false;

    /**
     * Order counter.
     *
     * @var int
     */
    protected $ordersCounter = 0;

    public function createOrders()
    {
        $executionStart = microtime(true);

        Mage::log(
            'createOrders method execution, processing order line items',
            null,
            'cminds_import_orders.log'
        );

        try {
            set_time_limit(0);
            ini_set('memory_limit', '-1');

            $this->websiteId = Mage::app()->getWebsite()->getId();
            $this->store = Mage::app()->getStore();

            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

            $i = 1;
            $this->ordersArr = $this->prepareCsvArr();
            $orderLineItemsCount = count($this->ordersArr);

            echo 'Starting orders import.' . "\n\n";
            echo '1. Processing order line items.' . "\n";
            foreach ($this->ordersArr as $orderArr) {
                if (!isset($orderArr['id'])) {
                    continue;
                }

                $this->processOrderData($orderArr);

                echo 'Order line item processed [' . $i . '/' . $orderLineItemsCount . ']' . "\n";
                $i++;
            }

            echo 'Order line items has been processed.' . "\n\n";

            echo '2. Processing order shipment tracking codes.' . "\n";
            $this->createShipments();
            echo 'Order shipment tracking codes has been processed.' . "\n\n";

            echo '3. Processing order shipping charges.' . "\n";
            $this->processShippingCharges();
            echo 'Order shipping charges has been processed.' . "\n\n";

            echo '4. Processing order invoices.' . "\n";
            $this->createInvoices();
            echo 'Order invoices has been processed.' . "\n\n";

            echo '5. Processing order statuses.' . "\n";
            $this->updateOrdersStatus();
            echo 'Order statuses has been processed.' . "\n\n";

            echo 'Order import has been finished.' . "\n";

            $this->sendMail();
        } catch (Exception $e) {
            Mage::log(
                '[' . $e->getFile() . ':' . $e->getLine() . '] ' . $e->getMessage(),
                null,
                'cminds_import_orders_errors.log'
            );
        }

        $executionEnd = microtime(true);
        $executionTime = $executionEnd - $executionStart;
        $averageTime = $executionTime / $orderLineItemsCount;

        echo 'Average time for single order line item: '
            . $this->getFormattedTimeString($averageTime) . "\n";
        echo 'Finished in: '
            . $this->getFormattedTimeString($executionTime) . "\n\n";
    }

    protected function getFormattedTimeString($seconds)
    {
        return sprintf(
            '%02d%s%02d%s%02d',
            floor($seconds / 3600),
            ':',
            ($seconds / 60) % 60,
            ':',
            $seconds % 60
        );
    }

    protected function processOrderData(array $orderArr)
    {
        try {
            if (!empty($orderArr['shipping_charge'])
                && (int)$orderArr['shipping_charge'] === 1
            ) {
                /**
                 * If shipping charge column is equal to 1 this line item
                 * is probably freight or any other item that is classified
                 * as shipping charge.
                 */
                $this->shippingCharges[] = $orderArr;

                Mage::log(
                    $orderArr['id'] . ' - ' . $orderArr['sales_order_line']
                    . ' skipping, it is a shipping charge.',
                    null,
                    'cminds_import_orders.log'
                );

                return $this;
            }

            $this->searchProductBySku(
                $orderArr['product_sku'],
                $orderArr
            );

            if (empty($orderArr['product_qty'])) {
                $orderArr['product_qty'] = 1;
            }

            $order = $this->getOrder($orderArr);

            if ($order->getId()) {
                if ($orderArr['order_status'] === 'Cancelled') {
                    $this->ordersToCancell[] = $order->getId();
                }

                $this->orders[] = $order->getId();

                if (!empty($orderArr['product_sku'])) {
                    $this->assignNextProduct($orderArr, $order);
                }
            } else {
                $admin = Mage::getModel('admin/user')
                    ->getCollection()
                    ->addFieldToFilter(
                        'import_id',
                        $orderArr['order_salesrep']
                    )
                    ->load();
                if (count($admin) === 0) {
                    $this->missingSealereps[] = $orderArr['order_salesrep'];
                } else {
                    $adminData = $admin->getData();
                    Mage::app()->getFrontController()->getRequest()
                        ->setPost(
                            'salesrep_rep_id',
                            $adminData[0]['user_id']
                        );
                }

                $this->createQuote($orderArr);

                $service = Mage::getModel(
                    'sales/service_quote',
                    $this->quote
                );
                $service->submitAll();

                $order = $service->getOrder();
                $order
                    ->setData('import_id', $orderArr['id'])
                    ->setData('po_number', $orderArr['po_number'])
                    ->setData('email_follower', $orderArr['email_follower'])
                    ->setData('shipping_time', $orderArr['shipping_time'])
                    ->setData('line_number_type', $orderArr['line_number_type']);

                foreach ($order->getAllItems() as $item) {
                    $item->setImportId($orderArr['sales_order_line']);
                    $item->save();
                }

                $format = 'd/m/Y';
                $dateobj = DateTime::createFromFormat(
                    $format,
                    $orderArr['order_entry_date']
                );
                $iso_datetime = $dateobj->format(Datetime::ATOM);
                if (!empty($orderArr['order_entry_date'])) {
                    $order->setData(
                        'created_at',
                        date('Y-m-d', strtotime($iso_datetime))
                    );
                }

                $order->setData(
                    'order_processed_by',
                    $orderArr['order_processed_by']
                );
                $order->setData(
                    'order_edited_by',
                    $orderArr['order_edited_by']
                );

                $dateobj = DateTime::createFromFormat(
                    $format,
                    $orderArr['pdd']
                );
                $iso_datetime = $dateobj->format(Datetime::ATOM);
                $order->setData(
                    'pdd',
                    date('Y-m-d', strtotime($iso_datetime))
                );
                $order->setData(
                    'pdd_buffer',
                    $orderArr['pdd_buffer']
                );
                $order->save();

                $this->orders[] = $order->getId();
                $this->ordersCounter++;
            }

            if (!empty($orderArr['order_tracking_number'])) {
                $this->shipmentsArr[$orderArr['order_tracking_number']][] = $orderArr;
            }
            if (!empty($orderArr['invoice_id'])) {
                $this->invoicesArr[$orderArr['invoice_id']][] = $orderArr;
            }

            if ($orderArr['order_status'] === 'Cancelled') {
                $this->ordersToCancell[] = $order->getId();
            }

            $this->addNoteToOrder($order, $orderArr);


            Mage::log(
                $orderArr['id'] . ' - ' . $orderArr['sales_order_line']
                . ' has been processed.',
                null,
                'cminds_import_orders.log'
            );
        } catch (Exception $e) {
            Mage::log(
                '[' . $e->getFile() . ':' . $e->getLine() . '] '
                . '(Order id: ' . $orderArr['id'] . ') ' . $e->getMessage(),
                null,
                'cminds_import_orders_errors.log'
            );
        }

        return $this;
    }

    protected function processShippingCharges()
    {
        foreach ($this->shippingCharges as $shippingChargeData) {
            $this->processShippingCharge($shippingChargeData);
        }

        return $this;
    }

    protected function processShippingCharge(array $shippingChargeData)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $this->getOrder($shippingChargeData);
        if ($order->getId() === null) {
            return $this;
        }

        $shippingAmount = 0;
        if (!empty($shippingChargeData['product_price'])) {
            $shippingAmount = (float)$shippingChargeData['product_price'];
        }

        $oldShippingAmount = $order->getBaseShippingAmount();
        $newShippingAmount = $shippingAmount + $oldShippingAmount;

        $grandTotal = $order->getBaseGrandTotal();
        $grandTotal += $shippingAmount;

        $order
            ->setShippingAmount($newShippingAmount)
            ->setBaseShippingAmount($newShippingAmount)
            ->setGrandTotal($grandTotal)
            ->setBaseGrandTotal($grandTotal);

        $order->save();

        return $this;
    }

    protected function searchProductBySku($sku, $orderArr)
    {
        $this->product = Mage::getModel('catalog/product')
            ->loadByAttribute('sku', $sku);

        if (!$this->product) {
            $product_skus = Mage::getModel('import/product_sku')->getCollection();
            $product_skus->getSelect()->where('sku = ?', array($sku));
            $product_skus_data = $product_skus->getData();

            if (isset($product_skus_data[0]['product_id'])) {
                $this->productOption = explode(
                    ',',
                    $product_skus_data[0]['options_skus']
                );
                $this->product = Mage::getModel('catalog/product')
                    ->load($product_skus_data[0]['product_id']);
            } else {
                $this->product = $this->createSimpleProduct($orderArr);

                if (!empty($sku)) {
                    $this->skusNotFound[] = $sku;
                }
            }
        }

        return $sku;
    }

    protected function createSimpleProduct($orderArr)
    {
        $product = Mage::getModel('catalog/product');
        $defaultAttributeSetId = Mage::getModel('catalog/product')
            ->getDefaultAttributeSetId();

        $product
            ->setTypeId('simple')
            ->setAttributeSetId($defaultAttributeSetId)
            ->setName('Not found product "' . $orderArr['product_sku'] . '"')
            ->setSku($orderArr['product_sku'])
            ->setPrice($orderArr['product_price'])
            ->setWebsiteIDs(array(1));

        $product->save();

        return $product;
    }

    public function sendMail()
    {
        $cSkusNotFound = count($this->skusNotFound);
        $cMissingSealereps = count($this->missingSealereps);

        if ($cSkusNotFound > 0) {
            $name = Mage::getStoreConfig('trans_email/ident_general/name');
            $email = Mage::getStoreConfig('trans_email/ident_general/email');
            $body = '';

            if ($cSkusNotFound > 0) {
                $body .= 'SKUS not found: <br/><br/>'
                    . implode('<br/>', $this->skusNotFound) . '<br/><br/>';
            }
            if ($cMissingSealereps > 0) {
                $body .= 'Salesreps not found: <br/><br/>'
                    . implode('<br/>', $this->missingSealereps);
            }

            $mail = Mage::getModel('core/email');
            $mail->setToName($name);
            $mail->setToEmail($email);
            $mail->setBody($body);
            $mail->setSubject('Liton order report');
            $mail->setFromEmail($email);
            $mail->setFromName($name);
            $mail->setType('html');

            try {
                $mail->send();
            } catch (Exception $e) {
                Mage::log(
                    'Cant send mail',
                    null,
                    'cminds_import_orders_errors.log'
                );
            }

            if ($cSkusNotFound > 0) {
                Mage::log(
                    'SKUS not found: ' . "\n"
                    . implode("\n", $this->skusNotFound),
                    null,
                    'cminds_import_orders_errors.log'
                );
            }
            if ($cMissingSealereps > 0) {
                Mage::log(
                    'Salesreps not found: ' . "\n"
                    . implode("\n", $this->missingSealereps),
                    null,
                    'cminds_import_orders_errors.log'
                );
            }
        }
    }

    protected function searchProductBySkuStatic($sku)
    {
        $product = Mage::getModel('catalog/product')
            ->loadByAttribute('sku', $sku);

        if (!$product) {
            $product_skus = Mage::getModel('import/product_sku')->getCollection();
            $product_skus->getSelect()->where('sku = ?', array($sku));
            $product_skus_data = $product_skus->getData();

            if (isset($product_skus_data[0]['product_id'])) {
                $product = Mage::getModel('catalog/product')
                    ->load($product_skus_data[0]['product_id']);
            } else {
                $sku = Mage::getStoreConfig('order_import/general/default_product_sku');
                $product = Mage::getModel('catalog/product')
                    ->loadByAttribute('sku', $sku);
            }
        }

        return $product;
    }

    protected function updateOrdersStatus()
    {
        foreach ($this->orders as $orderId) {
            $state = null;
            $order = Mage::getModel('sales/order')
                ->load($orderId);

            if (in_array($orderId, $this->ordersToCancell)) {
                $state = Mage_Sales_Model_Order::STATE_CANCELED;
            } else {
                $fullyShipped = true;

                foreach ($order->getAllItems() as $item) {
                    if (($item->getQtyToShip() > 0
                            && !$item->getIsVirtual()
                            && !$item->getLockedDoShip())
                        || $item->getQtyToInvoiced() > 0
                    ) {
                        $fullyShipped = false;
                    }
                }

                if ($fullyShipped) {
                    $state = Mage_Sales_Model_Order::STATE_COMPLETE;
                }
            }

            if ($state) {
                $status = $order->getConfig()->getStateDefaultStatus($state);
                $order->setStatus($status);
                $order->save();
            }
        }
    }

    protected function addNoteToOrder($order, $orderArr)
    {
        $note = '';

        if (!empty($orderArr['order_note_reporter'])) {
            $note .= 'Reporter: ' . $orderArr['order_note_reporter'] . ' ';
        }
        if (!empty($orderArr['order_note_created_at'])) {
            $note .= 'Created At: ' . $orderArr['order_note_created_at'] . ' ';
        }
        if (!empty($orderArr['order_note'])) {
            $note .= $orderArr['order_note'] . ' ';
        }
        if (!empty($note)) {
            $order->addStatusHistoryComment($note);
        }
        $order->save();
    }

    protected function createShipments()
    {
        foreach ($this->shipmentsArr as $trackingNumber => $items) {
            try {
                $order = Mage::getModel('import/order')->load(
                    $items[0]['id'],
                    'import_id'
                );

                if (!$order->getId()) {
                    continue;
                }

                $shipment = Mage::getModel('sales/order_shipment_track')->getCollection();
                $shipment->getSelect()->where('track_number', $trackingNumber);
                $shipmentData = $shipment->getData();

                if (isset($shipmentData[0]['id']) && !empty($shipmentData[0]['id'])) {
                    continue;
                }

                $qty_arr = array();
                foreach ($items as $item) {
                    $product = $this->searchProductBySkuStatic($item['product_sku']);

                    if (empty($product)) {
                        throw new Exception(
                            sprintf(
                                'Product with "%s" sku does not exists.',
                                $item['product_sku']
                            )
                        );
                    }

                    $itemCollection = $this->getItemByImportIdOrderId(
                        $item['sales_order_line'],
                        $order
                    );

                    $itemData = $itemCollection->getData();

                    $qty_arr[$itemData[0]['item_id']] =
                        (int)$item['product_qty'];
                }
                if (count($qty_arr) > 0) {
                    $shipment = $order->prepareShipment($qty_arr);
                    $shipment->register();

                    foreach ($shipment->getAllItems() as $item) {
                        $orderItem = Mage::getModel('sales/order_item')
                            ->load($item->getOrderItemId());
                        $orderItem->setQtyShipped($item->getQty());
                        $orderItem->save();
                    }

                    $shipment->save();
                    $sh = Mage::getModel('sales/order_shipment_track')
                        ->setShipment($shipment)
                        ->setData('title', 'Title')
                        ->setData('number', $trackingNumber)
                        ->setData('order_id', $shipment->getData('order_id'));

                    $sh->save();
                }

                $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                $status = $order->getConfig()->getStateDefaultStatus($state);
                $order->setStatus($status);
                $order->save();
            } catch (Exception $e) {
                Mage::log(
                    '[' . $e->getFile() . ':' . $e->getLine() . '] '
                    . '(Order id: ' . $items[0]['id'] . ') ' . $e->getMessage(),
                    null,
                    'cminds_import_orders_errors.log'
                );
            }
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function createInvoices()
    {
        foreach ($this->invoicesArr as $invoiceId => $items) {
            try {
                $order = Mage::getModel('import/order')->load(
                    $items[0]['id'],
                    'import_id'
                );

                if (!$order->getId()) {
                    continue;
                }

                $invoice = Mage::getModel('sales/order_invoice')->getCollection();
                $invoice->getSelect()->where(
                    'custom_import_id = ?',
                    $invoiceId
                );
                $invoiceData = $invoice->getData();

                if (isset($invoiceData[0]['entity_id'])
                    && !empty($invoiceData[0]['entity_id'])
                ) {
                    continue;
                }

                $qty_arr = array();

                foreach ($items as $item) {
                    $product = $this->searchProductBySkuStatic($item['product_sku']);

                    if (empty($product)) {
                        throw new Exception(
                            'Product with SKU dosent exists.'
                        );
                    }
                    $itemCollection = $this->getItemByImportIdOrderId(
                        $item['sales_order_line'],
                        $order
                    );
                    $itemData = $itemCollection->getData();
                    $qty_arr[$itemData[0]['item_id']] =
                        (int)$item['product_qty'];
                }

                if (count($qty_arr) > 0) {
                    $invoice = Mage::getModel('import/invoice', $order)
                        ->prepareInvoice($qty_arr);

                    $invoice->register()
                        ->save()
                        ->sendEmail(false)
                        ->setEmailSent(false);

                    $invoice->save();

                    $newInvoice = Mage::getModel('sales/order_invoice')
                        ->load($invoice->getId());
                    $newInvoice->setCustomImportId($invoiceId);
                    $newInvoice->save();
                }

                $state = Mage_Sales_Model_Order::STATE_PROCESSING;
                $status = $order->getConfig()->getStateDefaultStatus($state);
                $order->setStatus($status);
                $order->save();
            } catch (Exception $e) {
                Mage::log(
                    '[' . $e->getFile() . ':' . $e->getLine() . '] '
                    . '(Order id: ' . $items[0]['id'] . ') ' . $e->getMessage(),
                    null,
                    'cminds_import_orders_errors.log'
                );
            }
        }

        return true;
    }

    protected function getOrder($orderArr)
    {
        $id = $orderArr['id'];

        //if (!isset($this->ordersRepository[$id])) {
            $this->ordersRepository[$id] = Mage::getModel('import/order')
                ->load($id, 'import_id');
        //}

        return $this->ordersRepository[$id];
    }

    protected function createQuote($orderArr)
    {
        $this->quote = Mage::getModel('sales/quote')
            ->setStoreId($this->store->getId());

        $customer = array();

        if (!empty($orderArr['customer_email'])
            || !empty($orderArr['customer_id'])
        ) {
            $customer = $this->assignCustomer($orderArr);
        }
        $this->quote->save();

        $this->assignProduct($orderArr);
        $this->createBillingAddress($orderArr, $customer);
        $this->createShippingAddress($orderArr, $customer);

        $this->quote->getPayment()
            ->importData(array(
                'method' => 'purchaseorder',
                'po_number' => $orderArr['po_number'],
            ));

        $this->quote
            ->collectTotals()
            ->save();
    }

    protected function getItemByImportIdOrderId($importId, $order)
    {
        $itemCollection = Mage::getModel('sales/order_item')->getCollection();
        $itemCollection->getSelect()->where('import_id = ?', $importId)
            ->where('order_id = ?', $order->getId());

        return $itemCollection;
    }

    protected function assignNextProduct($orderArr, $order)
    {
        $product = $this->product;

        if (!$product) {
            throw new Exception(
                'Product with  "' . $orderArr['product_sku'] . '" sku does not exists.'
            );
        }

        $productData = $product->getData();

        $product = Mage::getModel('catalog/product')
            ->load($productData['entity_id']);

        $itemCollection = $this->getItemByImportIdOrderId(
            $orderArr['sales_order_line'],
            $order
        );

        $itemData = $itemCollection->getData();

        if (isset($itemData[0]['item_id'])
            && !empty($itemData[0]['item_id'])
            //&& count($this->productOption) === 0
        ) {
            $orderItem = Mage::getModel('sales/order_item')
                ->load($itemData[0]['item_id']);

            $qty = (int)$orderArr['product_qty'];
            $price = (float)$orderItem->getPrice();

            $rowTotal = $qty * $price;

            $orderItem
                ->setQtyOrdered($orderArr['product_qty'])
                ->setSubTotal($rowTotal)
                ->setRowTotal($rowTotal)
                ->setRowTotalInclTax($rowTotal)
                ->setBaseRowTotalInclTax($rowTotal);
            $orderItem->save();
        } else {
            $qty = $orderArr['product_qty'];
            $quote = Mage::getModel('sales/quote')
                ->getCollection()
                ->addFieldToFilter('entity_id', $order->getQuoteId())
                ->getFirstItem();

            if (!$quote->getId()) {
                $quote = Mage::getModel('sales/convert_order')
                    ->toQuote($order)
                    ->setIsActive(false)
                    ->setReservedOrderId($order->getIncrementId())
                    ->save();
            }

            $requestTax = Mage::getSingleton('tax/calculation')
                ->getRateRequest()
                ->setProductClassId($product->getTaxClassId());

            $taxRate = Mage::getSingleton('tax/calculation')
                ->getRate($requestTax);

            $product->setSku($orderArr['product_sku']);
            $totalAmountInclTax = $product->getPrice() + ($product->getPrice() / 100) * $taxRate;
            $quoteItem = Mage::getModel('sales/quote_item')
                ->setProduct($product)
                ->setPrice($product->getPrice())
                ->setOriginalPrice($product->getPrice())
                ->setSubTotal($product->getPrice() * $qty)
                ->setRowTotal($product->getPrice() * $qty)
                ->setRowTotalInclTax($totalAmountInclTax * $qty)
                ->setBaseRowTotalInclTax($totalAmountInclTax * $qty)
                ->setQuote($quote)
                ->setImportId($orderArr['sales_order_line'])
                ->setQty($qty);

            if (!empty($orderArr['discounts_amount'])) {
                $quoteItem
                    ->setDiscountAmount($orderArr['discounts_amount']);
            }

            if (!empty($orderArr['product_price'])
                && $product->getPrice() != $orderArr['product_price']
            ) {
                $quoteItem
                    ->setCustomPrice($orderArr['product_price'])
                    ->setOriginalCustomPrice($orderArr['product_price']);
            }

            $quoteItem
                ->save();

            $orderItem = Mage::getModel('sales/convert_quote')
                ->itemToOrderItem($quoteItem)
                ->setOrderID($order->getId());

            if (count($this->productOption) > 0) {
                $option_arr = array(
                    'info_buyRequest' =>
                        array(
                            'options' => array(),
                        ),
                    array(
                        'options' => array(),
                    ),
                );
                foreach ($this->productOption as $productOption) {
                    foreach ($product->getOptions() as $option) {
                        foreach ($option->getValues() as $value) {
                            if ($value->getSku() != $productOption) {
                                continue;
                            }

                            $price =
                                Mage::helper('core')->currency(
                                    $qty * $value->getPrice(),
                                    true,
                                    false
                                );
                            $optionData = $option->getData();
                            $option_arr['info_buyRequest']['options'][$value->getOptionId()]
                                = $value->getOptionTypeId();
                            $option_arr['options'][] =
                                array(
                                    'label' => $optionData['default_title'],
                                    'value' => $qty . ' x ' . $value->getTitle() . ' - ' . $price,
                                    'option_id' => $optionData['option_id'],
                                    'option_type' => $optionData['type'],
                                    'option_value' => $value->getOptionTypeId(),
                                    'custom_view' => false,
                                );

                            $orderItem->setRowTotalInclTax($orderArr['product_price'] * $qty)
                                ->setBaseRowTotalInclTax($orderArr['product_price'] * $qty)
                                ->setOriginalPrice($value->getPrice());

                        }
                    }
                }

                $optionsArrMerged = array_merge(
                    $orderItem->getProductOptions(),
                    $option_arr
                );
                $orderItem->setProductOptions($optionsArrMerged);
            }
            $orderItem->setImportId($orderArr['sales_order_line']);
            $orderItem
                ->save($order->getId());
        }

        if ($order->getId()) {
            $order->save();
        }

        return $this;
    }

    protected function assignCustomer($orderArr)
    {
        if (!empty($orderArr['customer_id'])) {
            $customer = Mage::getModel('customer/customer')
                ->getCollection()
                ->addFieldToFilter('import_custom_id', $orderArr['customer_id'])
                ->load();

            $customerData = $customer->getData();

            if (isset($customerData[0]['entity_id'])
                && !empty($customerData[0]['entity_id'])
            ) {
                $customer = Mage::getModel('customer/customer')
                    ->load($customerData[0]['entity_id']);
            } else {
                $customerArr = $this->prepareCustomerArray($orderArr);
                $email = Cminds_Import_Model_Customers::generateEmailForCustomer(
                    $customerArr,
                    $this->websiteId,
                    $this->store,
                    null
                );

                $customer = Mage::getModel('customer/customer');
                $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
                $customer->loadByEmail($email);

                if (!$customer->getId()) {
                    $customer = $this->createCustomer($orderArr);
                }
            }
        }

        if (!empty($customer)) {
            $this->quote->setCustomer($customer);
        }

        return $customer;
    }

    /**
     * Method create new customer.
     *
     * @param $customerArr
     *
     * @return Mage_Customer_Model_Customer
     *
     * @throws Exception
     */
    protected function createCustomer($customerArr)
    {
        $customerModel = Cminds_Import_Model_Customers::createCustomer($customerArr);

        Cminds_Import_Model_Customers::createCustomerBillingAddress(
            $customerModel,
            $customerArr
        );
        Cminds_Import_Model_Customers::createCustomerShippingAddress(
            $customerModel,
            $customerArr
        );

        return $customerModel;
    }

    /**
     * Prepare array which will use in create customer methods.
     *
     * @param $orderArr
     *
     * @return array
     */
    protected function prepareCustomerArray($orderArr)
    {
        $customerArr = array();
        $customerArr['id'] = $orderArr['customer_id'];
        $customerArr['firstname'] = $orderArr['customer_type'];

        $customerArr['billing_country'] = $orderArr['billing_address_country'];
        $customerArr['billing_city'] = $orderArr['billing_address_city'];
        $customerArr['billing_postalcode'] = $orderArr['billing_address_postalcode'];
        $customerArr['billing_telephone'] = $orderArr['billing_address_phone'];
        $customerArr['billing_street1'] = $orderArr['billing_address_street'];
        $customerArr['billing_street2'] = '';
        $customerArr['billing_region'] = $orderArr['billing_address_state'];

        $customerArr['shipping_country'] = $orderArr['shipping_address_country'];
        $customerArr['shipping_city'] = $orderArr['shipping_address_city'];
        $customerArr['shipping_postalcode'] = $orderArr['shipping_address_postalcode'];
        $customerArr['shipping_telephone'] = $orderArr['shipping_address_phone'];
        $customerArr['shipping_street1'] = $orderArr['shipping_address_street'];
        $customerArr['shipping_street2'] = '';
        $customerArr['shipping_region'] = $orderArr['shipping_address_state'];

        return $customerArr;
    }

    /**
     * Generate email for new customer.
     *
     * @param $orderArr
     *
     * @return string
     */
    protected function generateEmail($orderArr)
    {
        return 'customer_' . $orderArr['customer_id'] . '@mail.com';
    }

    protected function assignProduct($orderArr)
    {
        $product = $this->product;

        if (!$product) {
            throw new Exception(
                'Product with SKU ' . $orderArr['product_sku'] . ' does not exists.'
            );
        }

        $productData = $product->getData();

        $productById = Mage::getModel('catalog/product')
            ->load($productData['entity_id']);
        if (count($this->productOption) > 0) {
            $option_arr = array(
                'options' => array(),
            );
            foreach ($this->productOption as $productOption) {
                foreach ($productById->getOptions() as $option) {
                    foreach ($option->getValues() as $value) {
                        if ($value->getSku() == $productOption) {
                            $option_arr['options'][$value->getOptionId()] = $value->getOptionTypeId();
                        }
                    }
                }
            }

            $request = new Varien_Object();
            $request->setData($option_arr);

            $quoteItem = $this->quote->addProduct($productById, $request);
        } else {
            $quoteItem = $this->quote->addProduct(
                $productById,
                $orderArr['product_qty']
            );
        }

        if (is_string($quoteItem)) {
            throw new Exception(
                'Product with SKU ' . $orderArr['product_sku'] . ': ' . $quoteItem
            );
        }

        $quoteItem->setQty($orderArr['product_qty']);
        $quoteItem->setImportId($orderArr['sales_order_line']);

        if (!empty($orderArr['discounts_amount'])) {
            $quoteItem
                ->setDiscountAmount($orderArr['discounts_amount']);
        }

        if (!empty($orderArr['product_price'])
            && $product->getPrice() != $orderArr['product_price']
        ) {
            $quoteItem
                ->setCustomPrice($orderArr['product_price'])
                ->setOriginalCustomPrice($orderArr['product_price']);
        }

        $quoteItem->save();

        return $this;
    }

    protected function createBillingAddress(&$orderArr, $customer)
    {
        $billingAddress = $this->quote->getBillingAddress();
        $customer = Mage::getModel('customer/customer')->load($customer->getId());
        $customerBillingAddress = array();

        if (count($customer) > 0 && !empty($customer) && $customer->getPrimaryBillingAddress()) {
            $customerBillingAddress = $customer->getPrimaryBillingAddress()->getData();
        }

        if (count($customerBillingAddress) > 0) {
            if (empty($orderArr['billing_address_first_name'])
                && isset($customerBillingAddress['firstname'])
            ) {
                $orderArr['billing_address_first_name'] =
                    $customerBillingAddress['firstname'];
            }
            if (empty($orderArr['billing_address_last_name'])
                && isset($customerBillingAddress['lastname'])
            ) {
                $orderArr['billing_address_last_name'] =
                    $customerBillingAddress['lastname'];
            }
            if (empty($orderArr['billing_address_country'])
                && isset($customerBillingAddress['country_id'])
            ) {
                $orderArr['billing_address_country'] =
                    $customerBillingAddress['country_id'];
            }
            if (empty($orderArr['billing_address_city'])
                && isset($customerBillingAddress['city'])
            ) {
                $orderArr['billing_address_city'] =
                    $customerBillingAddress['city'];
            }
            if (empty($orderArr['billing_address_postalcode'])
                && isset($customerBillingAddress['postcode'])
            ) {
                $orderArr['billing_address_postalcode'] =
                    $customerBillingAddress['postcode'];
            }
            if (empty($orderArr['billing_address_phone'])
                && isset($customerBillingAddress['telephone'])
            ) {
                $orderArr['billing_address_phone'] =
                    $customerBillingAddress['telephone'];
            }
            if (empty($orderArr['billing_address_street'])
                && isset($customerBillingAddress['street'])
            ) {
                $orderArr['billing_address_street'] =
                    $customerBillingAddress['street'];
            }
            if (empty($orderArr['billing_address_state'])
                && isset($customerBillingAddress['region'])
            ) {
                $orderArr['billing_address_state'] =
                    $customerBillingAddress['region'];
            }
        }

        if (empty($orderArr['billing_address_first_name'])) {
            $orderArr['billing_address_first_name'] = 'Dummy First Name';
        }
        if (empty($orderArr['billing_address_last_name'])) {
            $orderArr['billing_address_last_name'] = 'Dummy Last Name';
        }
        if (empty($orderArr['billing_address_country'])) {
            $orderArr['billing_address_country'] = 'USA';
        }
        if (empty($orderArr['billing_address_city'])) {
            $orderArr['billing_address_city'] = 'Dummy City';
        }
        if (empty($orderArr['billing_address_phone'])) {
            $orderArr['billing_address_phone'] = '111 222 333';
        }
        if (empty($orderArr['billing_address_street'])) {
            $orderArr['billing_address_street'] = 'Dummy Street';
        }

        $billingAddress
            ->setFirstname($orderArr['billing_address_first_name'])
            ->setLastname($orderArr['billing_address_last_name'])
            ->setCountryId($orderArr['billing_address_country'])
            ->setCity($orderArr['billing_address_city'])
            ->setPostcode($orderArr['billing_address_postalcode'])
            ->setTelephone($orderArr['billing_address_phone'])
            ->setStreet($orderArr['billing_address_street']);

        if (isset($orderArr['billing_address_state'])
            && !empty($orderArr['billing_address_state'])
        ) {
            if ($orderArr['billing_address_country'] === 'US') {
                $regionModel = Mage::getModel('directory/region')
                    ->load($orderArr['billing_address_state'], 'default_name');
                $billingAddress->setRegionId($regionModel->getRegionId());
            } else {
                $billingAddress->setRegion($orderArr['billing_address_state']);
            }
        }
    }

    protected function createShippingAddress($orderArr, $customer)
    {
        $shippingAddress = $this->quote->getShippingAddress();

        $customerShippingAddress = array();

        if (count($customer) > 0 && !empty($customer) && $customer->getPrimaryShippingAddress()) {
            $customerShippingAddress = $customer->getPrimaryShippingAddress()->getData();
        }

        if (count($customerShippingAddress) > 0) {
            if (empty($orderArr['shipping_address_first_name'])
                && isset($customerShippingAddress['firstname'])
            ) {
                $orderArr['shipping_address_first_name'] =
                    $customerShippingAddress['firstname'];
            }
            if (empty($orderArr['shipping_address_last_name'])
                && isset($customerShippingAddress['lastname'])
            ) {
                $orderArr['shipping_address_last_name'] =
                    $customerShippingAddress['lastname'];
            }
            if (empty($orderArr['shipping_address_country'])
                && isset($customerShippingAddress['country_id'])
            ) {
                $orderArr['shipping_address_country'] =
                    $customerShippingAddress['country_id'];
            }
            if (empty($orderArr['shipping_address_city'])
                && isset($customerShippingAddress['city'])
            ) {
                $orderArr['shipping_address_city'] =
                    $customerShippingAddress['city'];
            }
            if (empty($orderArr['shipping_address_postalcode'])
                && isset($customerShippingAddress['postcode'])
            ) {
                $orderArr['shipping_address_postalcode'] =
                    $customerShippingAddress['postcode'];
            }
            if (empty($orderArr['shipping_address_phone'])
                && isset($customerShippingAddress['telephone'])
            ) {
                $orderArr['shipping_address_phone'] =
                    $customerShippingAddress['telephone'];
            }
            if (empty($orderArr['shipping_address_street'])
                && isset($customerShippingAddress['street'])
            ) {
                $orderArr['shipping_address_street'] =
                    $customerShippingAddress['street'];
            }
            if (empty($orderArr['shipping_address_state'])
                && isset($customerShippingAddress['region'])
            ) {
                $orderArr['shipping_address_state'] =
                    $customerShippingAddress['region'];
            }
        }

        if (empty($orderArr['shipping_address_first_name'])) {
            $orderArr['shipping_address_first_name'] =
                $orderArr['billing_address_first_name'];
        }

        if (empty($orderArr['shipping_address_last_name'])) {
            $orderArr['shipping_address_last_name'] =
                $orderArr['billing_address_last_name'];
        }
        if (empty($orderArr['shipping_address_country'])) {
            $orderArr['shipping_address_country'] =
                $orderArr['billing_address_country'];
        }
        if (empty($orderArr['shipping_address_city'])) {
            $orderArr['shipping_address_city'] =
                $orderArr['billing_address_last_name'];
        }
        if (empty($orderArr['shipping_address_postalcode'])) {
            $orderArr['shipping_address_postalcode'] =
                $orderArr['billing_address_city'];
        }
        if (empty($orderArr['shipping_address_phone'])) {
            $orderArr['shipping_address_phone'] =
                $orderArr['billing_address_phone'];
        }
        if (empty($orderArr['shipping_address_street'])) {
            $orderArr['shipping_address_street'] =
                $orderArr['billing_address_street'];
        }
        if (empty($orderArr['shipping_address_state'])) {
            $orderArr['shipping_address_state'] =
                $orderArr['billing_address_state'];
        }

        $shippingAddress
            ->setFirstname($orderArr['shipping_address_first_name'])
            ->setLastname($orderArr['shipping_address_last_name'])
            ->setCountryId($orderArr['shipping_address_country'])
            ->setCity($orderArr['shipping_address_city'])
            ->setPostcode($orderArr['shipping_address_postalcode'])
            ->setTelephone($orderArr['shipping_address_phone'])
            ->setStreet($orderArr['shipping_address_street']);

        if (isset($orderArr['shipping_address_state'])
            && !empty($orderArr['shipping_address_state'])
        ) {
            if ($orderArr['shipping_address_country'] === 'US') {
                $regionModel = Mage::getModel('directory/region')
                    ->load($orderArr['shipping_address_state'], 'default_name');
                $shippingAddress->setRegionId($regionModel->getRegionId());
            } else {
                $shippingAddress->setRegion($orderArr['shipping_address_state']);
            }

            if (empty($orderArr['shipping_method'])) {
                $shippingMethod = 'flatrate_flatrate';
            } else {
                $shippingMethod = $orderArr['shipping_method'];
            }

            $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates()
                ->setShippingMethod($shippingMethod);
        }
    }

    protected function prepareCsvArr()
    {
        if (!$this->csv) {
            $filePath = Mage::getBaseDir() . self::CSV_URL;

            if (file_exists($filePath) === false) {
                Mage::log(
                    sprintf('File %s does not exists.', $filePath),
                    null,
                    'cminds_import_orders.log',
                    true
                );

                return array();
            }

            $this->csv = file_get_contents($filePath);
        }

        $lines = explode("\n", $this->csv);
        if ($lines[count($lines) - 1] === '') {
            array_pop($lines);
        }

        $headers = str_getcsv($lines[0], '|');
        unset($lines[0]);
        $lines = array_values($lines);

        foreach ($headers as $key => $header) {
            $headers[$key] = str_replace(' ', '_', strtolower($header));
        }
        unset(
            $key,
            $header
        );

        if (count($lines) === 0) {
            Mage::log(
                'No order line records found in file',
                null,
                'cminds_import_orders.log'
            );

            return array();
        }

        $cHeaders = count($headers);
        $csvArr = array();

        foreach ($lines as $line) {
            $row = str_getcsv($line, '|');
            if (is_array($row) === false) {
                continue;
            }

            array_splice($row, count($headers));

            foreach ($row as $key => $value) {
                $row[$key] = trim($value);
            }

            $cRow = count($row);
            if ($cHeaders > 0 && $cRow > 0
                && $cHeaders === $cRow
            ) {
                $csvArr[] = array_combine($headers, $row);
            }
        }

        return $csvArr;
    }
}
