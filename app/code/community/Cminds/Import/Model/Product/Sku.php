<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_Import_Model_Product_Sku extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('import/product_sku');
    }
}