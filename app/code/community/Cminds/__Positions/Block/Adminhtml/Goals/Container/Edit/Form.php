<?php

class Cminds_Positions_Block_Adminhtml_Goals_Container_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        if (Mage::registry('goal_data')){
            $data = Mage::registry('goal_data')->getData();
        } else {
            $data = array();
        }

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data',
        ));

        $form->setUseContainer(true);

        $this->setForm($form);

        if (Mage::registry('goal_data')){
            $fieldset = $form->addFieldset('goals_form', array(
                'legend' =>Mage::helper('cminds_positions')->__('Edit goal')
            ));
        } else {
            $fieldset = $form->addFieldset('goals_form', array(
                'legend' =>Mage::helper('cminds_positions')->__('Add new goal')
            ));
        }


        $fieldset->addField('type_id', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Type'),
            'name'      => 'type_id',
            'values'    => array(
                array('value'=> '' , 'label' => 'None'),
                array('value'=> 1 , 'label' => 'Customers'),
                array('value'=> 2 , 'label' => 'Offices'),
                array('value'=> 3 , 'label' => 'Regional')
            ),
            'onchange'   => 'chooseGoalType(this)',
            'required'   =>true
        ));

        $fieldset->addField('customer_id', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Customer Name'),
            'name'      => 'customer_id',
            'values'    => $this->getCustomersOptionArray(),
            'class'     => 'dynamic-select-goal-fields',
            'required'   =>true
        ));

        $fieldset->addField('region_id', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Region Name'),
            'name'      => 'region_id',
            'values'    => $this->getRegionOptionArray(),
            'class'     => 'dynamic-select-goal-fields',
            'onchange'   => 'chooseGoalRegion(this)',
            'required'   =>true
        ));

        $regionArray = Mage::helper('cminds_positions')->getCurrentAdminRegions();
        foreach($regionArray as $region) {
            $fieldset->addField('office_id['.$region.']', 'select', array(
                'label' => Mage::helper('cminds_positions')->__('Office Name'),
                'name' => 'office_id['.$region.']',
                'values' => $this->getOfficesOptionArray($region),
                'class' => 'offices office-'.$region.' dynamic-select-goal-fields',
                'required'   =>true
            ));
        }

        $fieldset->addField('period_type', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Period Type'),
            'name'      => 'period_type',
            'values'    => array(
                array('value' => '', 'label' => 'None'),
                array('value' => 1, 'label' => 'Month'),
                array('value' => 2, 'label' => 'Year')
            ),
            'required'   =>true,
            'onchange'   => 'chooseGoalPeriodType(this)',
        ));

        $fieldset->addField('period_months', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Month'),
            'name'      => 'period_months',
            'values'    => Cminds_Positions_Model_Source_MonthsList::toOptionArray(),
            'class' => 'period period-month',
        ));

        $fieldset->addField('period_year', 'select', array(
            'label'     => Mage::helper('cminds_positions')->__('Year'),
            'name'      => 'period_year',
            'values'    => Cminds_Positions_Model_Source_YearList::toOptionArray(),
            'class' => 'period period-year',
        ));

        $fieldset->addField('period_target', 'text', array(
            'label'     => Mage::helper('cminds_positions')->__('Target'),
            'name'      => 'period_target',
            'required'   =>true
        ));


        $form->setValues($data);

        return parent::_prepareForm();
    }

    public function getRegionOptionArray(){
        $regionArray = Mage::helper('cminds_positions')->getCurrentAdminRegions();

        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('id', array('in' => $regionArray));

        $options = array();

        $options[] = array('value' => 0, 'label' => 'No region');
        foreach($positionsCollection as $position){
            $options[] = array('value' => $position->getId(), 'label' => $position->getName());
        }

        return $options;
    }

    public function getOfficesOptionArray($region){
//        $officesArray = Mage::helper('cminds_positions')->getCurrentAdminOffices();

        $positionsCollection = Mage::getModel('cminds_positions/position')
            ->getCollection()
            ->addFieldToFilter('parent_id', array('eq' => $region));

        $options = array();

        $options[] = array('value' => 0, 'label' => 'No office');
        foreach($positionsCollection as $position){
            $options[] = array('value' => $position->getId(), 'label' => $position->getName());
        }

        return $options;
    }

    public function getCustomersArray(){
        $customersCollection = Mage::getModel('customer/customer')
            ->getCollection();

        $options = array();
        foreach($customersCollection as $customer){
            $options[] = $customer->getId();
        }

        return $options;
    }

    public function getCustomersOptionArray(){
        $customersCollection = Mage::getModel('customer/customer')
            ->getCollection();

        $options = array();

        $options[] = array('value' => 0, 'label' => 'No customer');
        foreach($this->getCustomersArray() as $customer){

            $customerModel = Mage::getModel('customer/customer')->load($customer);
            $options[] = array('value' => $customerModel->getId(), 'label' => $customerModel->getName());
        }

        return $options;
    }


}