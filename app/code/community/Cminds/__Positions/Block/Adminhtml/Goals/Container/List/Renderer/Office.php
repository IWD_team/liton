<?php

class Cminds_Positions_Block_Adminhtml_Goals_Container_List_Renderer_Office
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $officeId = $row->getData('office_id');

        if($officeId == 0 || $officeId == null) {
            return '';
        } else {
            $office = Mage::getModel('cminds_positions/position')->load($officeId);

            if($office->getId()) {
                return $office->getName();
            }
        }
    }
}