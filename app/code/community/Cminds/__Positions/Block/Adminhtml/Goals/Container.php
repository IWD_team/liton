<?php
class Cminds_Positions_Block_Adminhtml_Goals_Container extends Mage_Core_Block_Template
{

    public function __construct()
    {
        $this->setTemplate('cminds_positions/goals/container.phtml');
        parent::__construct();
    }

    public function isStoreAdmin(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }

    public function isTopLevel(){
        return Mage::helper('cminds_positions')->isTopLevel();
    }
}