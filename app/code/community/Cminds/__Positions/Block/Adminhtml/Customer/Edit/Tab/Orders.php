<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders extends Mage_Adminhtml_Block_Customer_Edit_Tab_Orders
{
    public function __construct()
    {
        parent::__construct();
        if($this->canSeeElement()){
            $this->setFilterVisibility(false);
        }
    }

    protected function _prepareColumns()
    {
        if($this->canSeeElement()){
            return parent::_prepareColumns();
        } else {

            $this->addColumn('real_order_id', array(
                'header'=> Mage::helper('sales')->__('Order #'),
                'width' => '80px',
                'type'  => 'text',
                'index' => 'entity_id',
                'filter' => false
            ));

            $this->addColumn('increment_id', array(
                'header' => Mage::helper('sales')->__('Purchase #'),
                'index' => 'increment_id',
                'width' => '100px',
                'filter' => false
            ));
            $this->addColumn('created_at', array(
                'header' => Mage::helper('sales')->__('Purchase Date'),
                'index' => 'created_at',
                'type' => 'datetime',
                'width' => '100px',
                'filter' => false
            ));

            $this->addColumn('shipping_data', array(
                'header' => Mage::helper('sales')->__('Shipping'),
                'index' => 'shipping_data',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Shipment',
                'filter' => false
            ));

            $this->addColumn('base_grand_total', array(
                'header' => Mage::helper('sales')->__('Total Ordered'),
                'index' => 'base_grand_total',
                'type' => 'currency',
                'currency' => 'base_currency_code',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Ordered',
                'filter' => false
            ));

            $this->addColumn('total_invoiced', array(
                'header' => Mage::helper('sales')->__('Total Invoiced'),
                'index' => 'total_invoiced',
                'type' => 'currency',
//                'currency' => 'order_currency_code',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Invoiced',
                'filter' => false
            ));

            $this->addColumn('status', array(
                'header' => Mage::helper('sales')->__('Status'),
                'index' => 'status',
                'type' => 'options',
                'width' => '120px',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Orders_Renderer_Status',
                'filter' => false
            ));

            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
                $this->addColumn('action',
                    array(
                        'header' => Mage::helper('sales')->__('Action'),
                        'width' => '50px',
                        'type' => 'action',
                        'getter' => 'getId',
                        'actions' => array(
                            array(
                                'caption' => Mage::helper('sales')->__('View'),
                                'url' => array('base' => '*/sales_order/view'),
                                'field' => 'order_id',
                                'data-column' => 'action',
                            )
                        ),
                        'filter' => false,
                        'sortable' => false,
                        'index' => 'stores',
                        'is_system' => true,
                    ));
            }

            if(!$this->isRegional()) {
                $this->addExportType('*/*/exportCsv',
                    Mage::helper('sales')->__('CSV'));
                $this->addExportType('*/*/exportExcel',
                    Mage::helper('sales')->__('Excel XML'));
            }
        }
    }

    public function getMainButtonsHtml()
    {
        $html = '';
        if($this->canSeeElement()) {
            if ($this->getFilterVisibility()) {
                $html .= $this->getResetFilterButtonHtml();
                $html .= $this->getSearchButtonHtml();
            }
        }
        return $html;
    }
    /**
     * Check if admin user is store admin.
     */
    public function canSeeElement(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }

    public function isRegional(){
        $currentUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currentUser->getSalesrepPositionId());


        if($positionModel->getId()){
            $childrenCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id', array('eq' => $currentUser->getSalesrepPositionId()));
            if($positionModel->getParentId() != 0 && $positionModel->getParentId() != null && $childrenCollection->getSize()){
                return true;
            }
        }
        return false;
    }
}
