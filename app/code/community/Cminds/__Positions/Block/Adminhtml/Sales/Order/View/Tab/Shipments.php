<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_View_Tab_Shipments
    extends Mage_Adminhtml_Block_Sales_Order_View_Tab_Shipments
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareColumns()
    {
        if(!Mage::helper('cminds_positions')->isStoreAdmin()) {
            $this->addColumn('increment_id', array(
                'header' => Mage::helper('sales')->__('Shipment #'),
                'index' => 'increment_id',
            ));

            $this->addColumn('created_at', array(
                'header' => Mage::helper('sales')->__('Shipping Date'),
                'index' => 'created_at',
                'type' => 'datetime',
            ));

            $this->addColumn('tracking_number', array(
                'header' => Mage::helper('sales')->__('Tracking Number'),
                'index' => 'tracking_number',
                'renderer' => 'Cminds_Positions_Block_Adminhtml_Sales_Order_View_Tab_Renderer_Trackingnumber'
            ));



            $this->addColumn('action',
                array(
                    'header' => Mage::helper('catalog')->__('Action'),
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('catalog')->__('View'),
                            'url' => array(
                                'base' => '*/sales_order_shipment/view'
                            ),
                            'field' => 'shipment_id'
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                )
            );

            $this->sortColumnsByOrder();
        } else {
            return parent::_prepareColumns();
        }

    }

    /**
     * Check if admin user is store admin.
     */
    public function canSeeElement(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }

    public function getTabLabel()
    {
        if($this->canSeeElement()) {
            return parent::getTabLabel();
        } else {
            return Mage::helper('sales')->__('Shipping');
        }
    }

    public function getTabTitle()
    {
        if($this->canSeeElement()) {
            return parent::getTabTitle();
        } else {
            return Mage::helper('sales')->__('Shipping');
        }
    }
}
