<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Abstract extends Mage_Adminhtml_Block_Widget_Grid
{

    private $_currentUser = false;

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);

        $this->setTemplate('cminds_positions/report/representative/grid.phtml');
    }

    public function getRowUrl($row)
    {
        return false;
    }

    protected function getUserId()
    {
        return Mage::helper('cminds_positions/dashboard')->getUserId();
    }

    protected function getTableName($model)
    {
        return Mage::getSingleton('core/resource')->getTableName($model);
    }

    protected function getFilter($filterName)
    {
        $filter = Mage::app()->getRequest()->getParam('filter', null);
        if (is_null($filter)) {
            $filter['order_admins'] = $this->getUser()->getSalesrepPositionId();
            $filter['from'] = date('Y-m-d');
            $filter['to'] = date('Y-m-d');
        }

        if (is_string($filter)) {
            $data = array();
            $filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);

            if ($filterName == 'from' || $filterName == 'to') {
                return $this->_parsePeriodFilter($filterName, $data);
            } else {
                if (isset($data[$filterName])) {
                    return $data[$filterName];
                }
            }
            return false;
        } else {
            if (Mage::registry('cron_filters')) {
                $filters = Mage::registry('cron_filters');

                if ($filterName == 'from' || $filterName == 'to') {
                    return $this->_parsePeriodFilter($filterName, $filters);
                } else {
                    if (isset($filters[$filterName])) {
                        return $filters[$filterName];
                    }
                }

                return false;
            } elseif (is_array($filter)) {
                if ($filterName == 'from' || $filterName == 'to') {
                    return $this->_parsePeriodFilter($filterName, $filter);
                } else {
                    if (isset($data[$filterName])) {
                        return $filter[$filterName];
                    }
                }

                return false;
            } else {
                return false;
            }
        }
    }

    private function _parsePeriodFilter($type, $filters)
    {
        $date = new DateTime($filters['from']);

        if ($type == 'from') {
            return $date->format('01/m/Y');
        } elseif ($type == 'to') {
            return $date->format('t/m/Y');
        }
        return false;
    }

    public function addColumn($columnId, $column)
    {
        if (is_array($column)) {
            $column['sortable'] = false;

            $this->_columns[$columnId] = $this->getLayout()->createBlock('adminhtml/widget_grid_column')
                ->setData($column)
                ->setGrid($this);
        } else {
            throw new Exception(Mage::helper('adminhtml')->__('Wrong column format.'));
        }

        $this->_columns[$columnId]->setId($columnId);
        $this->_lastColumnId = $columnId;
        return $this;
    }

    public function getUser()
    {
        if (!$this->_currentUser) {
            $this->_currentUser = Mage::getModel('admin/user')->load($this->getUserId());
        }

        return $this->_currentUser;
    }

    public function canShowDownlineCommission()
    {
        $userData = $this->getUser()->getSalesrepIsStoreManager();

        if ($userData) {
            return false;
        }
        return true;
    }
}
