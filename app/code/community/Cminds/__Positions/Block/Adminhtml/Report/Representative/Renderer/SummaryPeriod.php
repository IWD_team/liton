<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_SummaryPeriod extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $price = $row->getPeriod();
        return Mage::helper('core')->currency($price, true, false);
    }
}
