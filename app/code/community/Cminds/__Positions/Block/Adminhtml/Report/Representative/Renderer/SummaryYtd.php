<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_SummaryYtd extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $price = $row->getYtd();
        return Mage::helper('core')->currency($price, true, false);
    }
}
