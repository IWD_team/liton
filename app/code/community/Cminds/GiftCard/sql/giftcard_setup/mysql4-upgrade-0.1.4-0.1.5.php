<?php

$installer = $this;

$installer->startSetup();

$installer->run("


    
INSERT INTO {$this->getTable('core_email_template')} (`template_code`, `template_text`, `template_styles`, `template_type`, `template_subject`, `template_sender_name`, `template_sender_email`, `added_at`, `modified_at`, `orig_template_code`, `orig_template_variables`) VALUES
('Gift Card',' Gift card number : {{var number}} Gift Card message : {{var msg}} Gift Card amount: {{var amount}}','body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }','2','{{var store.getFrontendName()}}: New GiftCard # {{var giftcard.template_id}}','','',now(),'','giftcard_template','{\"Gift card number=\":\"number\",\"Gift Card message=\":\"msg\",\"Gift Card amount=\":\"amount\"}')

")->endSetup();
