<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Model_System_Config_Source_Card_Customcolumn
{

    public function toOptionArray($isMultiselect = true)
    {
        $columns = array();
        $columns[] = array('label' => 'Numeric', 'value' => 'numeric');
        $columns[] = array('label' => 'Alphanumeric', 'value' => 'alphanumeric');
        $columns[] = array('label' => 'Alphabetic', 'value' => 'alpha');
        return $columns;
    }
}
