<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Form extends Mage_Payment_Block_Form
{
    /**
     * Prepares the template layout
     */
    public function _prepareLayout()
    {
        $this->setTemplate('cminds_giftcard/form.phtml');
    }
}
