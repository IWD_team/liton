<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Giftcard_Orders_List extends Cminds_GiftCard_Block_Giftcard_Orders
{
    public function getGiftcard()
    {
        $giftcard = Mage::registry('current_gift_code');

        if ($giftcard) {
            return $giftcard;
        } else {
            return false;
        }
    }

    public function getGiftcardOrders()
    {
        $giftcard = $this->getGiftcard();

        $orders = Mage::getModel('giftcard/giftcard')
            ->getOrdersByGiftcardNumber($giftcard);


        return $orders;
    }

    public function getCurrentCustomerOrders()
    {
        $customerId = Mage::getSingleton('customer/session')->getId();

        $orderCollection = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('customer_id', array('eq' => array($customerId)));

        $orderIdsArray = array();

        foreach ($orderCollection as $order) {
            $orderIdsArray[] = $order->getId();
        }

        return $orderIdsArray;
    }

    public function getCustomerOrdersByGiftcard()
    {
        $giftcardOrders = $this->getGiftcardOrders();

        $customerOrders = $this->getCurrentCustomerOrders();
        $ordersFilteredByCustomer = $giftcardOrders
            ->addFieldToFilter(
                'order_id',
                array('in' => $customerOrders)
            );

        return $ordersFilteredByCustomer;
    }

    public function getGiftCardData()
    {
        $giftcard = $this->getGiftcard();

        $data = Mage::getModel('giftcard/giftcard')
            ->getGiftCardByNumber($giftcard);

        return $data;
    }
}