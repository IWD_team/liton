<?php

class Cminds_Litongiftcard_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isGiftcardOrder($order)
    {
        $isGiftcardOrder = false;
        $giftcardPayment = Mage::getModel('giftcard/payment');

        $giftcardOrder = $giftcardPayment->load($order->getQuoteId(), 'quote_id');

        if ($giftcardOrder->getId()) {
            $isGiftcardOrder = true;
        }

        return $isGiftcardOrder;
    }
}