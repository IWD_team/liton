<?php

class Cminds_Litongiftcard_Block_Adminhtml_Giftcardorders_Renderer_OrderExpected
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $expectedArray = Cminds_Litongiftcard_Model_Source_OrderExpected::toOptionArray();


        foreach ($expectedArray as $expected) {
            if($row->getOrderExpected() == '') continue;
            if ($expected['value'] == $row->getOrderExpected()) {
                return $expected['label'];
            }
        }
    }
}
