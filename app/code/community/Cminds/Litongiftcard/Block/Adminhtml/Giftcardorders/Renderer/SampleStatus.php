<?php

class Cminds_Litongiftcard_Block_Adminhtml_Giftcardorders_Renderer_SampleStatus
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $statuses = Mage::getResourceModel('sales/order_status_collection')
            ->addStatusFilter($row->getStatus())
            ->toOptionHash();
        $status = $row->getStatus();
        return $statuses[$status];
    }
}
