<?php

class Cminds_Litongiftcard_Model_Cron
{
    const XML_PATH_EMAIL_IDENTITY = 'sales_email/order/identity';

    public function sendReminder()
    {
        $remindersToSend = Mage::getResourceModel('cminds_litongiftcard/history_collection')->getOrdersToSendCron();

        if (!empty($remindersToSend)) {
            foreach ($remindersToSend as $orderId) {
                $order = Mage::getModel('sales/order')->load($orderId);
                $store = Mage::app()->getStore();
                $email = Mage::getModel('core/email_template');
                $email->loadDefault('sales_email_order_notification');

                $email->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name'));
                $email->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));

                $email->setTemplateSubject('Please update status of #' . $order->getIncrementId() . ' order.');

                // Retrieve specified view block from appropriate design package (depends on emulated store)
                $paymentBlock = Mage::helper('payment')
                    ->getInfoBlock($order->getPayment())
                    ->setIsSecureMode(true);
                $paymentBlock->getMethod()->setStore($store->getStoreId());
                $paymentBlockHtml = $paymentBlock->toHtml();

                $vars = array(
                    'order'        => $order,
                    'billing'      => $order->getBillingAddress(),
                    'payment_html' => $paymentBlockHtml
                );
                $giftcardEmailCopy = Mage::getStoreConfig('sales/giftcard/order_email_copy');
                try {
                    $email
                        ->send(
                            $giftcardEmailCopy,
                            $store->getName(),
                            $vars
                        );
                } catch (Exception $e) {
                    Mage::log($e->getMessage(), null, 'giftcard_reminder_email.log');
                }
            }
        }
    }
}