<?php

class Cminds_Positions_Adminhtml_DashboardController
    extends Mage_Adminhtml_Controller_Action
{
    public function _initAction() {
        $this->loadLayout()
            ->_addBreadcrumb(Mage::helper('reports')->__('Reports'), Mage::helper('reports')->__('Reports'))
            ->_addBreadcrumb(Mage::helper('reports')->__('Sales'), Mage::helper('reports')->__('Sales'));
        return $this;
    }

    public function _initReportAction($block) {
        $requestData = Mage::helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'));
        // $requestData = $this->_filterDates($requestData, array('from', 'to'));

        $requestData['store_ids'] = $this->getRequest()->getParam('store_ids');
        $params = new Varien_Object();

        foreach ($requestData as $key => $value) {
            if (!empty($value)) {
                $params->setData($key, $value);
            }
        }

        if ($block) {
            $block->setPeriodType($params->getData('period_type'));
            $block->setFilterData($params);
        }

        return $this;
    }

    public function representativeAction(){
        $selectedUser = $this->getRequest()->getParam('order_admins', false);
        if(!$selectedUser) {
            $user = Mage::getSingleton('admin/session')->getUser();
        } else {
            $user = Mage::getModel('admin/user')->load($selectedUser);
        }
        

        Mage::register('admin_user', $user);
        $this->_title($this->__('Salesrep Dashboard'))->_title("Salesrep Dashboard")->_title("Salesrep Dashboard");
        $this->_initAction()->_setActiveMenu('salesrep_dashboard');

        $this->_initReportAction($this->getLayout()->getBlock('grid.filter.form.representative'));

        $this->renderLayout();

    }
    protected function _isAllowed()
    {
        return true;
    }

    public function saveDefaultShippingStatusAction(){

        $params = $this->getRequest()->getParams();

        $sessionParams = Mage::getSingleton('admin/session')->getDefaultShippingStatus();
        if($sessionParams){
            $sessionData = $sessionParams;
        } else {
            $sessionData = array();
        }

        if(isset($params['optionId']) && isset($params['regionId'])){
            $sessionData[$params['regionId']] = $params['optionId'];
        }

        Mage::getSingleton('admin/session')->setDefaultShippingStatus($sessionData);

        echo json_encode(array('success' => true));

        return $this->_redirect(
            '/*/*/representative'
        );
    }

}