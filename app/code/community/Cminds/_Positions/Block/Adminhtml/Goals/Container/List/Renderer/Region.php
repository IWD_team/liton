<?php

class Cminds_Positions_Block_Adminhtml_Goals_Container_List_Renderer_Region
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $regionId = $row->getData('region_id');

        if($regionId == 0 || $regionId == null) {
            return '';
        } else {
            $region = Mage::getModel('cminds_positions/position')->load($regionId);

            if($region->getId()) {
                return $region->getName();
            }
        }
    }
}