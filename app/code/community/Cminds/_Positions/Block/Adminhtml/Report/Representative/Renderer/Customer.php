<?php

class Cminds_Positions_Block_Adminhtml_Report_Representative_Renderer_Customer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $order = Mage::getModel('sales/order')->load($row->getOrderId());
        $customer = $order->getCustomerId();
        $name = $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname();

        $url = $this->getUrl('adminhtml/customer/edit',
            array('id' => $customer));

        return '<a href="' . $url . '" target="_blank" title="View Customer">' . $name . '</a>';

    }
}