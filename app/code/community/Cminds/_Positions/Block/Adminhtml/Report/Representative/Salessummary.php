<?php
class Cminds_Positions_Block_Adminhtml_Report_Representative_Salessummary extends Cminds_Positions_Block_Adminhtml_Report_Representative_Abstract {

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('salesrep_salessummary_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    protected function _prepareCollection()
    {
        $collection = new Varien_Data_Collection();

        $adminRegions = $this->getRegions();

        foreach($adminRegions as $region) {
            $commissions = Mage::getModel('sales/order')->getCollection();
            $regionSalesreps = array();
            $regionSalesreps = $this->getRepIdsByRegion($region);

            $filterFrom = $this->getFilter('from');
            $filterTo = $this->getFilter('to');

            if ($filterFrom && $filterTo) {
                $dateFrom = new Zend_Date($filterFrom, 'd-m-Y');

                $dateTo = new Zend_Date($filterTo, 'd-m-Y');
                $commissions->addAttributeToFilter('created_at', array(
                    'from' => $dateFrom->toString('Y-m-d 00:00:00'),
                    'to' => $dateTo->toString('Y-m-d 23:59:59')
                ));
            }

            $commissions->getSelect()->joinLeft($this->getTableName('salesrep/salesrep'),
                $this->getTableName('salesrep/salesrep') . '.order_id = main_table.entity_id',
                array('rep_id'));
            $commissions->getSelect()->where('main_table.state != ?',
                'canceled');

            $commissions->addFieldToFilter('rep_id', array('in' => $regionSalesreps));

            $periodPV = 0;

            foreach ($commissions AS $c) {
                $periodPV += $c->getGrandTotal();
            }


            $commissions = Mage::getModel('sales/order')->getCollection()
                ->addAttributeToFilter('created_at', array(
                    'from' => date('Y-01-01 00:00:00'),
                    'to' => date('Y-m-d H:i:s')
                ));
            $commissions->getSelect()->joinLeft($this->getTableName('salesrep/salesrep'),
                $this->getTableName('salesrep/salesrep') . '.order_id = main_table.entity_id',
                array('rep_id'));
            $commissions->getSelect()->where('main_table.state != ?',
                'canceled');
            $commissions->addFieldToFilter('rep_id', array('in' => $regionSalesreps));

            $ytdPV = 0;

            foreach ($commissions AS $c) {
                $ytdPV += $c->getGrandTotal();
            }

            $commissions = Mage::getModel('sales/order')->getCollection();
            $commissions->getSelect()->joinLeft($this->getTableName('salesrep/salesrep'),
                $this->getTableName('salesrep/salesrep') . '.order_id = main_table.entity_id',
                array('rep_id'));
            $commissions->getSelect()->where('main_table.state != ?',
                'canceled');
            $commissions->addFieldToFilter('rep_id', array('in' => $regionSalesreps));

            $lifetimePV = 0;

            foreach ($commissions AS $c) {
                $lifetimePV += $c->getGrandTotal();
            }

            $position = Mage::getModel('cminds_positions/position')
                ->load($region);

            $year = date('Y',strtotime($dateFrom->toString('d-m-Y')));
            $goal = $this->getGoal($region, $year, $ytdPV);

            $s = new Varien_Object();
            $s->setTitle($position->getName());
            $s->setPeriod($periodPV);
            $s->setYtd($ytdPV);
            $s->setLifetime($lifetimePV);
            $s->setGoal($goal);
            $collection->addItem($s);
        }

        $this->setCollection($collection);
    }

    protected function _prepareColumns()
    {
      $baseCurrencyCode = Mage::app()->getStore((int)$this->getParam('store'))->getBaseCurrencyCode();
        $this->addColumn('title', array(
            'header'    => Mage::helper('salesrep')->__('SALES SUMMARY'),
            'width'     => '50px',
            'column_css_class' => 'main_header',
            'index'     => 'title',
        ));
        $this->addColumn('ytd', array(
            'header'    => Mage::helper('salesrep')->__('YTD'),
            'index'     => 'ytd',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
            'class'     => 'ytd-sales'
        ));
        $this->addColumn('lifetime', array(
            'header'    => Mage::helper('salesrep')->__('Lifetime'),
            'index'     => 'lifetime',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
            'class'     => 'lifetime-sales'

        ));
        $this->addColumn('period', array(
            'header'    => Mage::helper('salesrep')->__('Period'),
            'index'     => 'period',
            'type'      => 'currency',
            'currency_code'  => $baseCurrencyCode,
            'class'     => 'period-sales'
        ));
        $this->addColumn('goal', array(
            'header'    => Mage::helper('salesrep')->__('Goal'),
            'index'     => 'goal',
            'type'      => 'currency',
//            'currency_code'  => $baseCurrencyCode,
        ));
        return parent::_prepareColumns();
    }

    public function isTopLevel(){
        $currenUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currenUser->getSalesrepPositionId());

        if($positionModel->getId()){
            if($positionModel->getParentId() == 0 || $positionModel->getParentId() == null){
                return true;
            }
        }
        return false;
    }

    public function isRegional(){
        $currenUser = Mage::getSingleton('admin/session')->getUser();
        $positionModel = Mage::getModel('cminds_positions/position')
            ->load($currenUser->getSalesrepPositionId());


        if($positionModel->getId()){
            $childrenCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id', array('eq' => $currenUser->getSalesrepPositionId()));
            if($positionModel->getParentId() != 0 && $positionModel->getParentId() != null && $childrenCollection->getSize()){
                return true;
            }
        }
        return false;
    }

    public function getRegions(){

        $currentUser = Mage::getSingleton('admin/session')->getUser();

        $regionsArray = array();
        if($this->isTopLevel()) {
            $positionCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id',
                    $currentUser->getSalesrepPositionId());
            foreach ($positionCollection as $position) {
                $regionsArray[] = $position->getId();
            }
        } elseif($this->isRegional()){
            $positionCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id',
                    $currentUser->getSalesrepPositionId());
            foreach ($positionCollection as $position) {
                $regionsArray[] = $position->getId();
            }

        } else {
            $regionsArray[] = $currentUser->getSalesrepPositionId();
        }
        return $regionsArray;
    }

    public function getRepIdsByRegion($regionId){
        $positionsUnderRegion = array();
        if($this->isTopLevel()) {
            $positionCollection = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id', $regionId);


            foreach ($positionCollection as $position) {
                $positionsUnderRegion[] = $position->getId();
            }
        } elseif($this->isRegional()){
            $positionsUnderRegion[] = $regionId;
        } else {
            $positionsUnderRegion[] = $regionId;
        }

        $adminUserCollection = Mage::getModel('admin/user')
            ->getCollection()
            ->addFieldToFilter('salesrep_position_id', array('in' => $positionsUnderRegion));

        $salesrepArray = array();

        foreach($adminUserCollection as $admin){
            $salesrepArray[] = $admin->getId();
        }

        return $salesrepArray;
    }

    public function getGoal($regionId, $year, $ytdPV){
        $goalCollection = Mage::getModel('cminds_positions/customergoal')
            ->getCollection()
            ->addFieldToFilter('type_id', array('eq' => 3))
            ->addFieldToFilter('period_type', array('eq' => 2))
            ->addFieldToFilter('period_year', array('eq' => $year));
        if($this->isTopLevel()){
            $goalCollection
                ->addFieldToFilter('region_id', array('eq' => $regionId));
        } else {
            $goalCollection
                ->addFieldToFilter('office_id', array('eq' => $regionId));
        }

        if($goalCollection->getFirstItem()->getId()) {
            $goal = ($ytdPV/$goalCollection->getFirstItem()->getPeriodTarget())*100;
        } else {
            $goal = 0;
        }
        return round($goal,2) . "%";
    }
}
