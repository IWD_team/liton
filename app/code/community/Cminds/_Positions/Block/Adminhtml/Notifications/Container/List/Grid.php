<?php
class Cminds_Positions_Block_Adminhtml_Notifications_Container_List_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('office_id');
        $this->setId('salesrep_notifications_list_grid');
        $this->setDefaultDir('desc');
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
        $this->setDefaultLimit(null);
    }

    protected function _prepareCollection()
    {
        $params = $this->getRequest()->getParams();
        $collection = Mage::getModel('customer/customer')->getCollection();

        if(isset($params['customer_id'])){
            if($params['customer_id'] != 0){
                $collection
                    ->addFieldToFilter('entity_id', array('eq' => $params['customer_id']));
            } elseif($params['region_id'] != 0) {
                $regionId = $params['region_id'];
                $officeId = $params['office_id'][$regionId];
                $salesrepArray = $this->getSalesrepsForFilter($regionId, $officeId);

                $collection
                    ->getSelect()
                    ->where('salesrep_rep_id IN ('. implode(', ',$salesrepArray).')');
            }
        }
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('cminds_positions')->__('Customer Name'),
            'index'     => 'entity_id',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Notifications_Container_List_Renderer_Customer'
        ));
        $this->addColumn('office_id', array(
            'header'    => Mage::helper('cminds_positions')->__('Office'),
            'index'     => 'entity_id',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Notifications_Container_List_Renderer_Office'
        ));
        $this->addColumn('is_notified', array(
            'header'    => Mage::helper('cminds_positions')->__('Notify Me'),
            'index'     => 'customer_id',
            'align' => 'center',
            'renderer'  => 'Cminds_Positions_Block_Adminhtml_Notifications_Container_List_Renderer_Notified'
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return '';
    }

    public function isStoreAdmin(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }

    public function isTopLevel(){
        return Mage::helper('cminds_positions')->isTopLevel();
    }

    public function getSalesrepsForFilter($region, $office){

        $officesArray = array();
        if($office == 0){
            $positionModel = Mage::getModel('cminds_positions/position')
                ->getCollection()
                ->addFieldToFilter('parent_id', array('eq' => $region));

            foreach($positionModel as $position){
                $officesArray[] = $position->getId();
            }
        } else {
            $officesArray[] = $office;
        }

        $adminUsersCollection = Mage::getModel('admin/user')
            ->getCollection()
            ->addFieldToFilter('salesrep_position_id', array('in' => $officesArray));

        $salesrepArray = array();

        foreach($adminUsersCollection as $admin){
            $salesrepArray[] = $admin->getId();
        }

        return $salesrepArray;
    }

}
