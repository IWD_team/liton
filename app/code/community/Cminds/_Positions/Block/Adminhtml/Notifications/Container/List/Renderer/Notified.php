<?php

class Cminds_Positions_Block_Adminhtml_Notifications_Container_List_Renderer_Notified
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $notifyOrderPlaced = $row->getSalesrepNotifyWhenOrderPlaced();
        $notifyOrderShipped = $row->getSalesrepNotifyWhenOrderShipped();

        $isNotified = '<input type="checkbox" name="is_notified['.$row->getId().']" value="0" class="checkbox">';
        if($notifyOrderPlaced || $notifyOrderShipped){
            $isNotified = '<input type="checkbox" name="is_notified['.$row->getId().']" value="1" class="checkbox" checked="checked">';
        }

        return $isNotified;
    }
}