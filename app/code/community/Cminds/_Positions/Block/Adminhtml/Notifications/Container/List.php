<?php
class Cminds_Positions_Block_Adminhtml_Notifications_Container_List extends Mage_Adminhtml_Block_Widget_Grid_Container {

  public function __construct() {
    $this->_controller = 'adminhtml_notifications_container_list';
    $this->_blockGroup = 'cminds_positions';
    $this->_headerText = Mage::helper('cminds_positions')->__('Settings - Notifications');
    parent::__construct();
    $this->removeButton('add');
//    $this->_addButton('add', array(
//        'label'     => $this->__('Save'),
//        'onclick'   => 'saveCustomerNotification(\''.$this->saveCustomerNotigicationUrl().'\')',
//        'class'     => '',
//    ));
  }

  public function saveCustomerNotigicationUrl(){
    return $this->getUrl('*/*/saveNotifications');
  }
}
