<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_View_Tab_Invoices
    extends Mage_Adminhtml_Block_Sales_Order_View_Tab_Invoices
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareColumns()
    {

        if(!Mage::helper('cminds_positions')->isStoreAdmin()) {
            $this->addColumn('increment_id', array(
                'header' => Mage::helper('sales')->__('Invoice #'),
                'index' => 'increment_id',
            ));

            $this->addColumn('created_at', array(
                'header' => Mage::helper('sales')->__('Invoice Date'),
                'index' => 'created_at',
                'type' => 'datetime',
            ));

            $this->addColumn('base_grand_total', array(
                'header' => Mage::helper('customer')->__('Amount'),
                'index' => 'base_grand_total',
                'type' => 'currency',
                'currency' => 'base_currency_code',
            ));

            $this->addColumn('action',
                array(
                    'header' => Mage::helper('catalog')->__('Action'),
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('catalog')->__('View'),
                            'url' => array(
                                'base' => '*/sales_order_invoice/print'
                            ),
                            'field' => 'invoice_id'
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                )
            );

            $this->sortColumnsByOrder();
        } else {
            parent::_prepareColumns();
        }
    }
    /**
     * Check if admin user is store admin.
     */
    public function canSeeElement(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }

    public function getTabLabel()
    {
        if($this->canSeeElement()) {
            return parent::getTabLabel();
        } else {
            return Mage::helper('sales')->__('Invoices / Statement View');
        }
    }

    public function getTabTitle()
    {
        if($this->canSeeElement()) {
            return parent::getTabTitle();
        } else {
            return Mage::helper('sales')->__('Invoices / Statement View');
        }
    }
}
