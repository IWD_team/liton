<?php
class Cminds_Positions_Block_Adminhtml_Sales_Order_View_Items extends Mage_Adminhtml_Block_Sales_Order_View_Items
{
    /**
     * Check if admin user is store admin.
     */
    public function canSeeElement(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }
}
