<?php

class Cminds_Positions_Block_Adminhtml_Positiontype_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'cminds_positions';
        $this->_controller = 'adminhtml_positiontype';
        $this->_mode = 'edit';
    }

    public function getHeaderText() {
        if (Mage::registry('positiontype_data')
            && Mage::registry('positiontype_data')->getId()
        ) {
            return Mage::helper('cminds_positions')->__(
                'Edit Position Type: %s', $this->escapeHtml(
                    Mage::registry('positiontype_data')->getName()
                )
            );
        } else {
            return Mage::helper('cminds_positions')->__('New Position Type');
        }
    }

}