<?php
class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('customer_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('customer')->__('Customer Information'));
    }

    protected function _beforeToHtml()
    {
        /*
                if (Mage::registry('current_customer')->getId()) {
                    $this->addTab('view', array(
                        'label'     => Mage::helper('customer')->__('Customer View'),
                        'content'   => $this->getLayout()->createBlock('adminhtml/customer_edit_tab_view')->toHtml(),
                        'active'    => true
                    ));
                }
        */
        $this->addTab('account', array(
            'label'     => Mage::helper('customer')->__('Account Information'),
            'content'   => $this->getLayout()->createBlock('adminhtml/customer_edit_tab_account')->initForm()->toHtml(),
            'active'    => Mage::registry('current_customer')->getId() ? false : true
        ));

        $this->addTab('addresses', array(
            'label'     => Mage::helper('customer')->__('Addresses'),
            'content'   => $this->getLayout()->createBlock('adminhtml/customer_edit_tab_addresses')->initForm()->toHtml(),
        ));


        // load: Orders, Shopping Cart, Wishlist, Product Reviews, Product Tags - with ajax

        if (Mage::registry('current_customer')->getId()) {

            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
                $this->addTab('orders', array(
                    'label'     => Mage::helper('customer')->__('Orders'),
                    'class'     => 'ajax',
                    'url'       => $this->getUrl('*/*/orders', array('_current' => true)),
                ));
            }

            $this->addTab('cart', array(
                'label'     => Mage::helper('customer')->__('Shopping Cart'),
                'class'     => 'ajax',
                'url'       => $this->getUrl('*/*/carts', array('_current' => true)),
            ));

            $this->addTab('wishlist', array(
                'label'     => Mage::helper('customer')->__('Wishlist'),
                'class'     => 'ajax',
                'url'       => $this->getUrl('*/*/wishlist', array('_current' => true)),
            ));

            if (Mage::getSingleton('admin/session')->isAllowed('newsletter/subscriber')) {
                $this->addTab('newsletter', array(
                    'label'     => Mage::helper('customer')->__('Newsletter'),
                    'content'   => $this->getLayout()->createBlock('adminhtml/customer_edit_tab_newsletter')->initForm()->toHtml()
                ));
            }

            if (Mage::getSingleton('admin/session')->isAllowed('catalog/reviews_ratings')) {
                $this->addTab('reviews', array(
                    'label'     => Mage::helper('customer')->__('Product Reviews'),
                    'class'     => 'ajax',
                    'url'       => $this->getUrl('*/*/productReviews', array('_current' => true)),
                ));
            }

            if (Mage::getSingleton('admin/session')->isAllowed('catalog/tag')) {
                $this->addTab('tags', array(
                    'label'     => Mage::helper('customer')->__('Product Tags'),
                    'class'     => 'ajax',
                    'url'       => $this->getUrl('*/*/productTags', array('_current' => true)),
                ));
            }
        }

        $tabs = array(
            'customer_edit_tab_view',
            'customer_edit_tab_agreements',
            'customer_edit_tab_recurring_profile',
            'customer_edit_tab_admin',
            'cart',
            'wishlist',
            'newsletter',
            'reviews',
            'tags'
        );
        if(!$this->canSeeElement()){
            foreach($tabs as $tabId){
                if (isset($this->_tabs[$tabId])) {
                    unset($this->_tabs[$tabId]);
                }
            }
        }

        if (isset($this->_tabs['customer_edit_tab_notifications'])) {
            $notificationsTab = $this->_tabs['customer_edit_tab_notifications'];
            unset($this->_tabs['customer_edit_tab_notifications']);
            $this->_tabs['customer_edit_tab_notifications'] = $notificationsTab;
        }

        if (isset($this->_tabs['customer_edit_tab_goals'])) {
            $goalsTab = $this->_tabs['customer_edit_tab_goals'];
            unset($this->_tabs['customer_edit_tab_goals']);
            $this->_tabs['customer_edit_tab_goals'] = $goalsTab;
        }

        $this->_updateActiveTab();
        $this->setActiveTab('account');
        Varien_Profiler::stop('customer/tabs');
        return parent::_beforeToHtml();
    }

    protected function _updateActiveTab()
    {
        $tabId = $this->getRequest()->getParam('tab');
        if( $tabId ) {
            $tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
            if($tabId) {
                $this->setActiveTab($tabId);
            }
        }
    }
    /**
     * Check if admin user is store admin.
     */
    public function canSeeElement(){
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }
}
