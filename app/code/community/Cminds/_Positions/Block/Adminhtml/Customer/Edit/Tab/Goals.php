<?php

class Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Goals extends Mage_Adminhtml_Block_Widget_Grid
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_customer_edit_tab_goals';
        $this->_blockGroup = 'cminds_positions';
        $this->_headerText = Mage::helper('cminds_positions')->__('Positions');
        parent::__construct();
        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);
    }

    protected function _prepareCollection()
    {
        $customer = Mage::registry('current_customer');
        $collection = Mage::getModel('cminds_positions/customergoal')->getCollection();
        $collection
            ->addFieldToFilter('type_id', array('eq' => 1))
            ->addFieldToFilter('customer_id',
                array('eq' => $customer->getId()));

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('sales_amount', array(
            'header' => Mage::helper('cminds_positions')->__('Sales'),
            'index' => 'sales_amount',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Goals_Renderer_Sales'
        ));
        $this->addColumn('period_month', array(
            'header' => Mage::helper('cminds_positions')->__('Goal Period'),
            'index' => 'period_month',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Goals_Renderer_Period'
        ));
        $this->addColumn('period_type', array(
            'header' => Mage::helper('cminds_positions')->__('Type'),
            'index' => 'period_type',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Goals_Renderer_Type'

        ));
        $this->addColumn('period_target', array(
            'header' => Mage::helper('cminds_positions')->__('Goal Target'),
            'index' => 'period_target',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Cminds_Positions_Block_Adminhtml_Customer_Edit_Tab_Goals_Renderer_Target'
        ));

        if ($this->isStoreAdmin() || $this->isTopLevel()) {
            $this->addColumn('action',
                array(
                    'header' => Mage::helper('cminds_positions')->__('Action'),
                    'width' => '100',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('cminds_positions')->__('Edit'),
                            'url' => array('base' => '*/goals/edit'),
                            'field' => 'id'
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
                )
            );
        }
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return '';
    }

    public function getAdmins()
    {
        return LucidPath_SalesRepDeluxe_Model_Source_UsersList::toOptionArray();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Goals');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Goals');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    public function isStoreAdmin()
    {
        return Mage::helper('cminds_positions')->isStoreAdmin();
    }

    public function isTopLevel()
    {
        return Mage::helper('cminds_positions')->isTopLevel();
    }

}
