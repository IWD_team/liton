<?php

/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * Generates the card number
     * @return string $number 
     */
    public function generateCardNumber() {
        $str = md5(time());
        $str = str_split($str);
        $number = '';
        $x = 1;
        foreach ($str as $char) {
            if ($x % 2) {
                $number .= $char;
            }
            $x++;
        }
        return $number;
    }

    public function numberRand($length) {
        $array = array();
        for ($i = 0; $i < $length; $i++)
            $key .= rand(0, 9);

        return $key;
    }

    public function randomAlphaNum($length, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890') {
        // Length of character list
        $chars_length = (strlen($chars) - 1);

        // Start our string
        $string = $chars{rand(0, $chars_length)};

        // Generate random string
        for ($i = 1; $i < $length; $i = strlen($string)) {
            // Grab a random character from our list
            $r = $chars{rand(0, $chars_length)};

            // Make sure the same two characters don't appear next to each other
            if ($r != $string{$i - 1})
                $string .= $r;
        }

        // Return the string
        return $string;
    }

    public function randomAlpha($length, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') {
        // Length of character list
        $chars_length = (strlen($chars) - 1);

        // Start our string
        $string = $chars{rand(0, $chars_length)};

        // Generate random string
        for ($i = 1; $i < $length; $i = strlen($string)) {
            // Grab a random character from our list
            $r = $chars{rand(0, $chars_length)};

            // Make sure the same two characters don't appear next to each other
            if ($r != $string{$i - 1})
                $string .= $r;
        }

        // Return the string
        return $string;
    }

    /**
     * Checks the gift card balance
     * @param int $cardNum Card number to check
     * @return object 
     */
    public function cardBalance($cardNum) {
        return Mage::getModel('giftcard/giftcard')->getCollection()
                        ->addFieldToFilter('number', $cardNum)
                        ->getFirstItem()
                        ->getBal();
    }

}