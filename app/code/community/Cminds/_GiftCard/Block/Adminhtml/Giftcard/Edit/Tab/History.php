<?php
/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Block_Adminhtml_Giftcard_Edit_Tab_History extends Mage_Adminhtml_Block_Abstract
{
    /**
     * Conscturcts the history grid
     */
	public function _construct() 
	{
		$this->setTemplate('giftcard/history.phtml');
	}
        /**
         * Gets the purchase information
         * @param int $id Id of Giftcard
         * @return object $mdl
         */
	public function getPurchase($id)
	{
		$mdl = Mage::getModel('giftcard/giftcard')->getCollection()
			->addFieldToFilter('giftcard_id', $id)
			->getFirstItem();
		return $mdl;
	}
	/**
         * Get order number of Order
         * @param int $id Order ID
         * @return int $mdl->getIncrementId()
         */
	public function getTrueOrderNumber($id)
	{
		$mdl = Mage::getModel('sales/order')->getCollection()
			->addFieldToFilter('entity_id', $id)
			->getFirstItem();
		return $mdl->getIncrementId();
	}
	/**
         * Get Gift Card history
         * @param int $id Gift Card id
         * @return object
         */
	public function getHistory($id) 
	{
		$mdl = Mage::getModel('giftcard/payment')->getCollection()
			->addFieldToFilter('giftcard_id', $id)
			->setOrder('created_at', 'ASC');
		return $mdl;
	}
        /**
         * Get the transaction type
         * @param string $typeId
         * @return string
         */
	public function getTransType($typeId = null)
	{
		if($typeId) {
			return 'Refill';
		} else {
			return 'Charge';
		}
	}
}