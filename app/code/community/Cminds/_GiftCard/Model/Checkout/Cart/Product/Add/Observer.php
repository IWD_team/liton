<?php

/**
 * CreativeMindsSolutions
 */
class Cminds_GiftCard_Model_Checkout_Cart_Product_Add_Observer {

    /**
     * Saves the gift card item to the database
     * @param object $observer 
     */
    public function checkSubtotal($observer) {
 //echo "qty incr/decr";die;
       
            $totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals();
            $mytotal = 0;
            $mytotal = $totals["subtotal"]->getValue(); //Subtotal value
            if (isset($totals['shipping']))
                $mytotal+=$totals['shipping']->getData('value');
            if (isset($totals['tax']))
                $mytotal+= $totals['tax']->getValue(); //Tax value if present

            $cards = Mage::getModel('giftcard/payment')->getCollection()
                    ->addFieldToFilter('quote_id', $this->_getQuote()->getId());
            $gcAmount = 0;
            if ($cards) {
                foreach ($cards as $card) {
                    $gcAmount = $gcAmount + $card->getAmount();
                }
            }
//            echo $mytotal . "!" . $gcAmount;
//            die;
            if (isset($_SESSION['cno']) && $mytotal - $gcAmount < 0) {
                $cardNum = $_SESSION['cno'];
          
                $card = Mage::getModel('giftcard/giftcard')->getCollection()
                        ->addFieldToFilter('number', $cardNum)
                        ->getFirstItem();

                $cardPay = Mage::getModel('giftcard/payment')->getCollection()
                        ->addFieldToFilter('quote_id', $this->_getQuote()->getId())
                        ->addFieldToFilter('giftcard_id', $card->getId())
                        ->getFirstItem();
                $card->setBal($card->getBal() + $cardPay->getAmount());
                $card->save();
                $cardPay->delete();
                $this->_getQuote()->collectTotals();
                $this->_getQuote()->save();
            }
        
    }

    protected function _getQuote() {
        if (!Mage::getSingleton('checkout/session')->getQuoteId()) {
            $quote = Mage::getModel('sales/quote')
                    ->setId(null)
                    ->setStoreId(1)
                    ->setCustomerId('NULL')
                    ->setCustomerTaxClassId(1);
            $quote->save();
            Mage::getSingleton('checkout/session')->setQuoteId($quote->getId());
        } else {
            $quote = Mage::getSingleton('checkout/session')->getQuote();
        }
        return $quote;
    }

}