<?php
class LucidPath_SalesRepDeluxe_Model_Observer {

  public function sales_order_place_after(Varien_Event_Observer $observer) {
    $order = $observer->getEvent()->getOrder();

    $rep_id = "";

    if (Mage::helper('salesrep')->isAdmin()) {
      // backend
      $post = Mage::app()->getFrontController()->getRequest()->getPost();

      if (isset($post) && isset($post['salesrep_rep_id'])) {
        // rep from dropdown
        $rep_id = $post['salesrep_rep_id'];
      } else {
        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

        if ($customer->getId()) {
          // rep from customer
          if (!$rep_id = $customer->getSalesrepRepId()) {
            // current logged in admin
	    $user = Mage::getSingleton('admin/session')->getUser();
	    if (!$user) {
		return $this;
	    }
            $rep_id = Mage::getSingleton('admin/session')->getUser()->getId();

            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $table = Mage::getSingleton('core/resource')->getTableName('customer/entity');

            $write->query("UPDATE {$table} SET salesrep_rep_id = '". $rep_id ."' WHERE entity_id = ". $customer->getId() .";");
          }
        }
      }
    } else {
      // frontend
      $rep_id = intval(Mage::getSingleton('core/session')->getRepId());

      if (!$rep_id) {
        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

        if ($customer->getId()) {
          // rep from customer
          $rep_id = $customer->getSalesrepRepId();
        }

      }

      Mage::getSingleton('core/session')->setRepId("");
    }

    Mage::helper('salesrep')->setCommissionEarned($order->getId(), $rep_id);

    return $this;
  }

  public function hookToOrderSaveEvent(Varien_Event_Observer $observer) {
    return $this;
  }

  /**
   * Cancel order in admin event
   *
   * @param Varien_Object $observer
   */
  public function hookToAdminOrderCancel($observer) {
    $salesrep = Mage::getModel('salesrep/salesrep')->loadByOrder($observer['order']);
    $salesrep->setCommissionStatus('Canceled');
    $salesrep->save();

    return $this;
  }


  /**
   * Save system config event
   *
   * @param Varien_Object $observer
   */
  public function saveSystemConfig($observer) {
    $config = new Mage_Core_Model_Config();
    $config->saveConfig('salesrep/email_reports/cron_schedule', $this->_getSchedule(), 'default', 0);

    return $this;
  }

  /**
   * Transform system settings option to cron schedule string
   *
   * @return string
   */
  protected function _getSchedule() {
    $data = Mage::app()->getRequest()->getPost('groups');

    $hours    = !empty($data['email_reports']['fields']['schedule_hour']['value'])?
                      $data['email_reports']['fields']['schedule_hour']['value']:
                      0;

    $minutes  = !empty($data['email_reports']['fields']['schedule_minute']['value'])?
                      $data['email_reports']['fields']['schedule_minute']['value']:
                      0;

    $schedule = "$minutes $hours * * *";

    return $schedule;
  }

  /**
   * Cron action
   */
  public static function dispatch() {
    $email_send = Mage::getStoreConfig('salesrep/email_reports/email_send');
    if ($email_send == 0) return;

    $frequency = Mage::getStoreConfig('salesrep/email_reports/schedule_frequency');

    switch ($frequency) {
      case LucidPath_SalesRepDeluxe_Model_Source_Frequency::EVERY_DAY:
        $start_date = mktime(0, 0, 0, date("m"), date("d")-1, date("Y"));
        $end_date = mktime(23, 59, 59, date("m"), date("d")-1, date("Y"));

        break;
      case LucidPath_SalesRepDeluxe_Model_Source_Frequency::EVERY_WEEKDAY:
        $start_date = mktime(0, 0, 0, date("m"), date("d")-1, date("Y"));
        $end_date = mktime(23, 59, 59, date("m"), date("d")-1, date("Y"));

        // get current day of week
        $weekday = date('w');

        // exit if weekend
        if ($weekday == 0 || $weekday == 6) return;
        break;
      case LucidPath_SalesRepDeluxe_Model_Source_Frequency::EVERY_FRIDAY:
        $start_date = mktime(0, 0, 0, date("m"), date("d")-5, date("Y"));
        $end_date = mktime(23, 59, 59, date("m"), date("d")-1, date("Y"));

        // get current day of week
        $weekday = date('w');

        // exit if not friday
        if ($weekday != 5) return;
        break;
      case LucidPath_SalesRepDeluxe_Model_Source_Frequency::EVERY_TWO_WEEKS:
        $current_day = date('j');
        $last_day_of_month = date("j", strtotime('-1 second',strtotime('+1 month',strtotime(date('m').'/01/'.date('Y').' 00:00:00'))));

        if ($current_day == 15) {
          $start_date = mktime(0, 0, 0, date("m") , date("d")-date("d")+1, date("Y"));
          $end_date   = mktime(23, 59, 59, date("m") , date("d")-date("d")+15, date("Y"));
        } else if ($current_day == $last_day_of_month) {
          $start_date = mktime(0, 0, 0, date("m") , date("d")-date("d")+15, date("Y"));
          $end_date   = mktime(23, 59, 59, date("m") , date("d")-date("d")+$last_day_of_month, date("Y"));
        } else {
          return;
        }

        break;
      case LucidPath_SalesRepDeluxe_Model_Source_Frequency::EVERY_MONTH:
          $last_day_of_month = date("j", strtotime('-1 second',strtotime('+1 month',strtotime(date('m').'/01/'.date('Y').' 00:00:00'))));

          $start_date = mktime(0, 0, 0, date("m") , date("d")-date("d")+1, date("Y"));
          $end_date   = mktime(23, 59, 59, date("m") , date("d")-date("d")+$last_day_of_month, date("Y"));
        break;
      default:
        $start_date = mktime(0, 0, 0, date("m"), date("d")-1, date("Y"));
        $end_date = mktime(23, 59, 59, date("m"), date("d")-1, date("Y"));
        break;
    }

    // $start_date = mktime(0, 0, 0, date("m")-2, date("d")-1, date("Y"));

    $selected_admins = explode(',', Mage::getStoreConfig('salesrep/step_setup/users'));
    $all_admins      = Mage::getResourceModel('admin/user_collection')->load();

    foreach ($all_admins as $admin) {
      if (Mage::getStoreConfig('salesrep/email_reports/send_reports_to') == LucidPath_SalesRepDeluxe_Model_Source_SendReportsTo::EMPLOYEE_ONLY) {
        if (Mage::helper('salesrep')->isAllowed($admin, 'system/config')) continue;
      }

      if (in_array($admin->getId(), $selected_admins)) {
          // get order collection
        $collection = Mage::getModel('sales/order')->getCollection();
        // join salesrep table
        $collection->getSelect()->joinLeft(array('salesrep' => $collection->getTable('salesrep/salesrep')), 'salesrep.order_id=main_table.entity_id');

        // report date range
        # convert from local time to db-time
        $start_date  = Mage::getModel('core/date')->gmtDate(null, $start_date);
        $end_date    = Mage::getModel('core/date')->gmtDate(null, $end_date);

        $collection->addAttributeToFilter('created_at', array('from' => $start_date, 'to' => $end_date));

        // permissions
        $view_rep_name_all = Mage::helper('salesrep')->isAllowed($admin, 'salesrep/emailed_commission_reports/include_order_list_and_rep_name/all_orders');
        $view_rep_name_sub = Mage::helper('salesrep')->isAllowed($admin, 'salesrep/emailed_commission_reports/include_order_list_and_rep_name/orders_of_subordinate');
        $view_rep_name_own = Mage::helper('salesrep')->isAllowed($admin, 'salesrep/emailed_commission_reports/include_order_list_and_rep_name/own_orders_only');

        $view_comm_all     = Mage::helper('salesrep')->isAllowed($admin, 'salesrep/emailed_commission_reports/include_commission_amount/all_orders');
        $view_comm_sub     = Mage::helper('salesrep')->isAllowed($admin, 'salesrep/emailed_commission_reports/include_commission_amount/orders_of_subordinate');
        $view_comm_own     = Mage::helper('salesrep')->isAllowed($admin, 'salesrep/emailed_commission_reports/include_commission_amount/own_orders_only');

        $view_status_all   = Mage::helper('salesrep')->isAllowed($admin, 'salesrep/emailed_commission_reports/include_commission_payment_status/all_orders');
        $view_status_sub   = Mage::helper('salesrep')->isAllowed($admin, 'salesrep/emailed_commission_reports/include_commission_payment_status/orders_of_subordinate');
        $view_status_own   = Mage::helper('salesrep')->isAllowed($admin, 'salesrep/emailed_commission_reports/include_commission_payment_status/own_orders_only');

        //
        $subordinate_ids = array();

        $admin_user_collection = Mage::getResourceModel('admin/user_collection');
        $admin_user_collection->addFieldToFilter('salesrep_manager_id', $admin->getId());

        foreach ($admin_user_collection as $admin_user) {
          $subordinate_ids[] = $admin_user->getId();
        }

        $subordinate_ids[] = $admin->getId();


        if (!Mage::helper('salesrep')->isAllowed($admin, 'system/config')) {
          if ($view_rep_name_sub) {
            $collection->getSelect()->where('salesrep.rep_id IN('. implode(', ', $subordinate_ids) .') OR salesrep.manager_id IN('. implode(', ', $subordinate_ids) .')');
          } else if ($view_rep_name_own) {
            $collection->getSelect()->where('salesrep.rep_id = '. $admin->getId() .' OR salesrep.manager_id = '. $admin->getId());
          }
        }

        $report = array();

        foreach ($collection as $row) {
          // collect Rep commission data
          $show_name = false;

          // Rep name permissions
          if (   $view_rep_name_all
              || ($view_rep_name_sub && in_array($row->getRepId(), $subordinate_ids))
              || ($view_rep_name_own && $admin->getId() == $row->getRepId())
          ){
            $show_name = true;
          }

          // Rep commission amount permissions
          $show_comm = false;

          if (   $view_comm_all
              || ($view_comm_sub && in_array($row->getRepId(), $subordinate_ids))
              || ($view_comm_own && $admin->getId() == $row->getRepId())
          ){
            $show_comm = true;
          }

          // Rep commission payment status permissions
          $show_status = false;

          if (   $view_status_all
              || ($view_status_sub && in_array($row->getRepId(), $subordinate_ids))
              || ($view_status_own && $admin->getId() == $row->getRepId())
          ){
            $show_status = true;

            // die('yes');
          } else {
            // die('no');
          }

          if ($show_name) {
            $rep_name = ($row->getRepName() == "") ? "No Sales Rep." : $row->getRepName();

            if (!array_key_exists($rep_name, $report)) {
              $report[$rep_name] = array();
            }

            // Total earned for user
            if (!isset($report[$rep_name]['paid_total'])) {
              $report[$rep_name]['paid_total'] = 0;
            }

            if (!isset($report[$rep_name]['unpaid_total'])) {
              $report[$rep_name]['unpaid_total'] = 0;
            }

            if (strtolower($row->getRepCommissionStatus()) == "paid") {
              $report[$rep_name]['paid_total'] += round($row->getRepCommissionEarned(), 2);
            } else if (strtolower($row->getRepCommissionStatus()) == "unpaid") {
              $report[$rep_name]['unpaid_total'] += round($row->getRepCommissionEarned(), 2);
            }

            if (!isset($report[$rep_name]['orders'])) {
              $report[$rep_name]['orders'] = array();
            }

            $report[$rep_name]['orders'][] = array(
                                'value'              => $show_comm ? $row->getRepCommissionEarned() : '',
                                'status'             => strtolower($row->getRepCommissionStatus()),
                                'show_status'        => $show_status,
                                'created_at'         => Mage::getModel('core/date')->date(null, strtotime($row->getData('created_at'))),
                                'order_id'           => $row->getId(),
                                'order_increment_id' => $row->getIncrementId(),
                                'order_status'       => $row->getStatus(),
                                'is_manager'           => false,
                                );

            $report[$rep_name]['rep_id'] = $row->getRepId();
            $report[$rep_name]['show_comm'] = $show_comm;
          }

          // Manager
          $show_name = false;

          // Rep name permissions
          if (   $view_rep_name_all
              || ($view_rep_name_sub && in_array($row->getRepId(), $subordinate_ids))
              || ($view_rep_name_own && $admin->getId() == $row->getManagerId())
          ){
            $show_name = true;
          }

          // Rep commission amount permissions
          $show_comm = false;

          if (   $view_comm_all
              || ($view_comm_sub && in_array($row->getManagerId(), $subordinate_ids))
              || ($view_comm_own && $admin->getId() == $row->getManagerId())
          ){
            $show_comm = true;
          }

          $show_status = false;

          if (   $view_status_all
              || ($view_status_sub && in_array($row->getRepId(), $subordinate_ids))
              || ($view_status_own && $admin->getId() == $row->getManagerId())
          ){
            $show_status = true;
          }

          if ($show_name && ($rep_name = $row->getManagerName()) != '') {
            if (!array_key_exists($rep_name, $report)) {
              $report[$rep_name] = array();
            }

            // Total earned for user
            if (!isset($report[$rep_name]['paid_total'])) {
              $report[$rep_name]['paid_total'] = 0;
            }

            if (!isset($report[$rep_name]['unpaid_total'])) {
              $report[$rep_name]['unpaid_total'] = 0;
            }

            if (strtolower($row->getManagerCommissionStatus()) == "paid") {
              $report[$rep_name]['paid_total'] += round($row->getManagerCommissionEarned(), 2);
            } else if (strtolower($row->getManagerCommissionStatus()) == "unpaid") {
              $report[$rep_name]['unpaid_total'] += round($row->getManagerCommissionEarned(), 2);
            }

            if (!isset($report[$rep_name]['orders'])) {
              $report[$rep_name]['orders'] = array();
            }

            $report[$rep_name]['orders'][] = array(
                                'value'              => $show_comm ? $row->getManagerCommissionEarned() : '',
                                'status'             => strtolower($row->getManagerCommissionStatus()),
                                'show_status'        => $show_status,
                                'created_at'         => Mage::getModel('core/date')->date(null, strtotime($row->getData('created_at'))),
                                'order_id'           => $row->getId(),
                                'order_increment_id' => $row->getIncrementId(),
                                'order_status'       => $row->getStatus(),
                                'is_manager'         => true,
                                );

            $report[$rep_name]['rep_id'] = $row->getManagerId();
            $report[$rep_name]['show_comm'] = $show_comm;
          }
        }

        krsort($report);

        // Define the sender, here we query Magento default email (in the configuration)
        // For customer support email, use : 'trans_email/ident_support/...'
        $sender = array('name' => Mage::getStoreConfig('trans_email/ident_general/name'),
                'email' => Mage::getStoreConfig('trans_email/ident_general/email'));

        // Set you store
        // This information may be taken from the current logged in user
        $store = Mage::app()->getStore();

        // In this array, you set the variables you use in your template
        $vars = array('report'     => $report,
                      'start_date' => $start_date,
                      'end_date'   => $end_date);

        $template_id = Mage::getStoreConfig("salesrep/email_reports/email_template");

        // You don't care about this...
        $translate  = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $emailTemplate  = Mage::getModel('core/email_template');

        $email = $admin->getEmail();

        // $email = 'devservice@gmail.com';

        // Send your email
        $emailTemplate->sendTransactional(
          $template_id,
          $sender,
          $email,
          $admin->getFirstname() ." ". $admin->getLastname(),
          $vars,
          $store->getId()
        );

        $translate->setTranslateInline(true);
      }
    }
  }
}
?>
