<?php
class LucidPath_SalesRepDeluxe_Model_Mysql4_Customergroup_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

  public function _construct() {
    $this->_init('salesrep/customergroup');
  }
}
