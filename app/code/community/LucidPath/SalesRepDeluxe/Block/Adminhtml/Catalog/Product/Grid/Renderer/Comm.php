<?php
class LucidPath_SalesRepDeluxe_Block_Adminhtml_Catalog_Product_Grid_Renderer_Comm extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

  public function render(Varien_Object $row) {
    if (!Mage::helper('salesrep')->isModuleEnabled()) {
      return '';
    }

    $view_commission_rate = Mage::getSingleton('admin/session')->isAllowed('salesrep/product_grid/view_commission_rate');
    $edit_commission_rate = Mage::getSingleton('admin/session')->isAllowed('salesrep/product_management/edit_commission');

    $value = '';

    if ($edit_commission_rate || $view_commission_rate) {
      if ($row->getSalesrepRepCommissionRate()) {
        $value = $row->getSalesrepRepCommissionRate();
      }

      if (!$value) {
        $value = Mage::getSingleton('admin/session')->getUser()->getSalesrepRepCommissionRate();
      }

      if (!$value) {
        $value = floatval(Mage::getStoreConfig('salesrep/setup/default_rep_commission'))/100;
      }

      $value = Mage::helper('salesrep')->getPercent($value);
    }


    return $value;
  }
}
?>
