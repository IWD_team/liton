<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$table = $installer->getTable('salesrep');

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('admin/user'),
                                       'salesrep_rep_commission_rate',
                                       array(
                                             'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
                                             'scale'     => 4,
                                             'precision' => 5,
                                             'nullable'  => true,
                                             'default'   => null,
                                             'comment'   => 'Rep Commission Rate'
                                             )
                                       );


$installer->getConnection()->addColumn($installer->getTable('admin/user'), 'salesrep_manager_id', 'INT(10)');

$installer->getConnection()->addColumn($installer->getTable('admin/user'),
                                       'salesrep_manager_commission_rate',
                                       array(
                                             'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
                                             'scale'     => 4,
                                             'precision' => 5,
                                             'nullable'  => true,
                                             'default'   => null,
                                             'comment'   => 'Manager Commission Rate'
                                             )
                                       );

$installer->getConnection()->addColumn($installer->getTable('catalog/product'),
                                       'salesrep_rep_commission_rate',
                                       array(
                                             'type'      => Varien_Db_Ddl_Table::TYPE_DECIMAL,
                                             'scale'     => 4,
                                             'precision' => 5,
                                             'nullable'  => true,
                                             'default'   => null,
                                             'comment'   => 'Rep Commission Rate'
                                             )
                                       );

$installer->getConnection()->addColumn($installer->getTable('customer/entity'), 'salesrep_rep_id', 'INT(10)');

if (!$installer->tableExists($installer->getTable('salesrep'))) {
$installer->run("
CREATE TABLE IF NOT EXISTS `{$installer->getTable('salesrep')}` (
  salesrep_id int(10) unsigned NOT NULL auto_increment,
  order_id int(10) unsigned not null,
  rep_id int(10),
  rep_name varchar(250),
  rep_commission_earned DECIMAL(12,2),
  rep_commission_status varchar(50) default 'unpaid',
  manager_id int(10),
  manager_name varchar(250),
  manager_commission_earned DECIMAL(12,2),
  manager_commission_status varchar(50) default 'unpaid',
  PRIMARY KEY(salesrep_id),
  CONSTRAINT `FK_SALESREP_ORDER` FOREIGN KEY (`order_id`) REFERENCES `{$installer->getTable('sales/order')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
} else {
  $conn = $installer->getConnection();

  try {
    if ($conn->tableColumnExists($table, 'admin_id')) {
      $conn->changeColumn($table, 'admin_id', 'rep_id', "INT(10) NOT NULL DEFAULT '0'");
    } else {
      if (!$conn->tableColumnExists($table, 'rep_id'))
        $conn->addColumn($table, 'rep_id', "INT(10) NOT NULL DEFAULT '0'");
    }
  } catch (Exception $e) {
  }

  try {
    if ($conn->tableColumnExists($table, 'admin_name')) {
      $conn->changeColumn($table, 'admin_name', 'rep_name', "varchar(250)");
    } else {
      if (!$conn->tableColumnExists($table, 'rep_name'))
        $conn->addColumn($table, 'rep_name', "varchar(250)");
    }
  } catch (Exception $e) {
  }

  try {
    if ($conn->tableColumnExists($table, 'commission_earned')) {
       $conn->changeColumn($table, 'commission_earned', 'rep_commission_earned', "DECIMAL(12,2)");
    } else {
      if (!$conn->tableColumnExists($table, 'rep_commission_earned'))
        $conn->addColumn($table, 'rep_commission_earned', "DECIMAL(12,2)");
    }
  } catch (Exception $e) {
  }

  try {
    if ($conn->tableColumnExists($table, 'commission_status')) {
       $conn->changeColumn($table, 'commission_status', 'rep_commission_status', "varchar(50) default 'unpaid'");
    } else {
      if ($conn->tableColumnExists($table, 'rep_commission_status'))
        $conn->addColumn($table, 'rep_commission_status', "DECIMAL(12,2)");
    }
  } catch (Exception $e) {
  }

  $installer->getConnection()->addColumn($installer->getTable('salesrep'), 'manager_id', 'INT(10)');
  $installer->getConnection()->addColumn($installer->getTable('salesrep'), 'manager_name', 'varchar(250)');
  $installer->getConnection()->addColumn($installer->getTable('salesrep'), 'manager_commission_earned', 'DECIMAL(12,2)');
  $installer->getConnection()->addColumn($installer->getTable('salesrep'), 'manager_commission_status', "varchar(50) default 'unpaid'");
}

$installer->setConfigData('salesrep/email_reports/email_template', 'salesrep_email_reports_email_template');

$installer->endSetup();
?>
