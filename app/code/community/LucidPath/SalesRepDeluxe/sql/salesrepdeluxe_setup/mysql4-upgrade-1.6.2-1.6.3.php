<?php
$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('salesrep/customergroup'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')
    ->addColumn('admin_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ), 'admin_id')
    ->addColumn('customer_group_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ), 'customer_group_id')
    ->addColumn('commission', Varien_Db_Ddl_Table::TYPE_DECIMAL, null, array(
        'nullable'  => true,
        'scale'     => 2,
        'precision' => 12
    ), 'commission');

$installer->getConnection()->createTable($table);

$installer->endSetup();
