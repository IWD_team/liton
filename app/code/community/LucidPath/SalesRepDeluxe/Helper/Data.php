<?php
class LucidPath_SalesRepDeluxe_Helper_Data extends Mage_Checkout_Helper_Data {

  protected $_module_name = 'LucidPath_SalesRepDeluxe';
  protected $_subordinateIds = null;

  /**
   * Return shipping status list for order grid filter
   *
   * @return array
   */
  public function getCommissionStatusList() {
    $result = array();

    $read = Mage::getSingleton('core/resource')->getConnection('core_read');
    $table = Mage::getSingleton('core/resource')->getTableName('salesrep/salesrep');

    $res = $read->fetchAll("SELECT DISTINCT(rep_commission_status) FROM {$table} WHERE rep_commission_status IS NOT NULL ORDER BY rep_commission_status;");

    foreach ($res as $item) {
      $result[$item['rep_commission_status']] = $item['rep_commission_status'];
    }

    return $result;
  }

  /**
   * Return admins list for salesrep dropdown
   *
   * @return array
   */
  public function getAdminsList() {
    $selected_admins = explode(',', Mage::getStoreConfig('salesrep/step_setup/users'));
    $all_admins    = Mage::getResourceModel('admin/user_collection')->setOrder('firstname', 'asc')->load();

    $result   = array();
    $result[] = array('value' => "0", 'label' => "No Sales Representative");

    foreach ($all_admins as $admin) {
      if (in_array($admin->getId(), $selected_admins)) {
        $result[] = array('value' => $admin->getId(), 'label' => $admin->getFirstname() .' '. $admin->getLastname());
      }
    }
    return $result;
  }

  /**
   * Return status list for orders
   *
   * @return array
   */
  public function getStatusList() {
    $result = array();
    $result[] = array('value' => 'Unpaid', 'label' => 'Unpaid');
    $result[] = array('value' => 'Paid', 'label' => 'Paid');
    $result[] = array('value' => 'Ineligible', 'label' => 'Ineligible');
    $result[] = array('value' => 'Canceled', 'label' => 'Canceled');

    return $result;
  }

  /**
   * Return status list for order order grid filter
   *
   * @return array
   */
  public function getStatusListFilter() {
    $result = array('Unpaid' => 'Unpaid', 'Paid' => 'Paid', 'Ineligible' => 'Ineligible', 'Canceled' => 'Canceled');

    return $result;
  }

  public function setCommissionEarned($order_id, $rep_id=null, $manager_id=null) {
    $order    = Mage::getModel('sales/order')->load($order_id);
    $salesrep = Mage::getModel('salesrep/salesrep')->loadByOrder($order);


    $customerGroupId = $order->getCustomerGroupId();

    $isBasedOnProfit = Mage::getStoreConfig('salesrep/setup/commissions_based_on_product_profit');
    $isBasedOnCustomerGroup = Mage::getStoreConfig('salesrep/setup/salesrep_commissions_based_on_customer_group');
    if ($rep_id) {
      // calc commission for Rep + Manager
      $rep_model = Mage::getModel('admin/user')->load($rep_id);

      if (!$rep_model->getId()) {
        return;
      }

      $customerGroupCommission = Mage::getModel('salesrep/customergroup')
          ->getCollection()
          ->addFieldToFilter('admin_id', array('eq' => $rep_id))
          ->addFieldToFilter('customer_group_id', array('eq' => $customerGroupId))
          ->getFirstItem();

      $salesrep->setRepId($rep_model->getId());
      $salesrep->setRepName($rep_model->getFirstname() ." ". $rep_model->getLastname());

      $rep_commission_earned = 0;

      $items = $order->getAllItems();

      foreach ($items as $item) {
        $product = Mage::getModel('catalog/product')->load($item->getProductId());

        if ($product->getSalesrepIsExcluded() == '1') {
          continue;
        }

        $qty   = $item->getQtyOrdered();

        $price = $item->getBasePrice();

        if($isBasedOnProfit && $product->getCost()){
          if((int)$price != 0) {
            $price = $item->getBasePrice() - $product->getCost();
          }
        }


        if($isBasedOnCustomerGroup && $customerGroupCommission->getCommission() !== null){
          $commission = floatval($customerGroupCommission->getCommission()) / 100;
        } else {
          $commission = $product->getSalesrepRepCommissionRate();
        }

        if (!$commission) {
          if ($rep_model->getSalesrepRepCommissionRate()) {
            $commission = $rep_model->getSalesrepRepCommissionRate();
          } else {
            $commission = floatval(Mage::getStoreConfig('salesrep/setup/default_rep_commission')) / 100;
          }
        }

        $rep_commission_earned += ($price * $qty - $item->getBaseDiscountAmount()) * $commission;
      }

      $salesrep->setRepCommissionEarned(round($rep_commission_earned, 2));

      $manager_model = Mage::getModel('admin/user')->load($rep_model->getSalesrepManagerId());

      if ($manager_model->getId()) {
        $customerGroupCommission = Mage::getModel('salesrep/customergroup')
            ->getCollection()
            ->addFieldToFilter('admin_id', array('eq' => $manager_model->getId()))
            ->addFieldToFilter('customer_group_id', array('eq' => $customerGroupId))
            ->getFirstItem();

        $salesrep->setManagerId($manager_model->getId());
        $salesrep->setManagerName($manager_model->getFirstname() .' '. $manager_model->getLastname());

        if($isBasedOnCustomerGroup && intval(Mage::getStoreConfig('salesrep/setup/manager_commission_based')) == 1){
          if($customerGroupCommission->getCommission() !== null){
            $manager_commission_rate = floatval($customerGroupCommission->getCommission()) / 100;
          } elseif($manager_model->getSalesrepManagerCommissionRate()) {
            $manager_commission_rate = $manager_model->getSalesrepManagerCommissionRate();
          } else {
            $manager_commission_rate = floatval(Mage::getStoreConfig('salesrep/setup/default_manager_commission')) / 100;
          }
          $manager_commission = round(floatval($order->getBaseSubtotal()) * $manager_commission_rate,
              2);
          $salesrep->setManagerCommissionEarned($manager_commission);
          $salesrep->setManagerCommissionStatus(Mage::getStoreConfig('salesrep/setup/default_status'));


        } else {

          if (!$manager_commission_rate = $manager_model->getSalesrepManagerCommissionRate()) {
            $manager_commission_rate = floatval(Mage::getStoreConfig('salesrep/setup/default_manager_commission')) / 100;
          }

          if ($manager_commission_rate > 0) {
            if (intval(Mage::getStoreConfig('salesrep/setup/manager_commission_based')) == 1) {
              // manager commission based on "Order Subtotal"
              $manager_commission = round(floatval($order->getBaseSubtotal()) * $manager_commission_rate,
                  2);
            } else {
              // manager commission based on "Employee Commission"
              $manager_commission = round($salesrep->getRepCommissionEarned() * $manager_commission_rate,
                  2);
            }

            $salesrep->setManagerCommissionEarned($manager_commission);
            $salesrep->setManagerCommissionStatus(Mage::getStoreConfig('salesrep/setup/default_status'));
          }
        }
      } else {
        $salesrep->setManagerId(null);
        $salesrep->setManagerName(null);
        $salesrep->setManagerCommissionEarned(null);
        $salesrep->setManagerCommissionStatus(null);
      }
    } else if ($manager_id != null) {
      // calc commission for manager only
      $manager_model = Mage::getModel('admin/user')->load($manager_id);

      if ($manager_model->getId()) {
        if (!$manager_commission_rate = $manager_model->getSalesrepManagerCommissionRate()) {
          $manager_commission_rate = floatval(Mage::getStoreConfig('salesrep/setup/default_manager_commission')) / 100;
        }

        if ($manager_commission_rate > 0) {
          if (intval(Mage::getStoreConfig('salesrep/setup/manager_commission_based')) == 1) {
            // manager commission based on "Order Subtotal"
            $manager_commission = round(floatval($order->getBaseSubtotal()) * $manager_commission_rate, 2);
          } else {
            // manager commission based on "Employee Commission"
            $manager_commission = round($salesrep->getRepCommissionEarned() * $manager_commission_rate, 2);
          }

          $salesrep->setManagerId($manager_model->getId());
          $salesrep->setManagerName($manager_model->getFirstname() .' '. $manager_model->getLastname());
          $salesrep->setManagerCommissionEarned($manager_commission);
          $salesrep->setManagerCommissionStatus(Mage::getStoreConfig('salesrep/setup/default_status'));
        } else {
          $salesrep->setManagerId(null);
          $salesrep->setManagerName(null);
          $salesrep->setManagerCommissionEarned(null);
          $salesrep->setManagerCommissionStatus(null);
        }
      } else {
        $salesrep->setManagerId(null);
        $salesrep->setManagerName(null);
        $salesrep->setManagerCommissionEarned(null);
        $salesrep->setManagerCommissionStatus(null);
      }
    } else {
      $salesrep->setRepId(null);
      $salesrep->setRepName(null);
      $salesrep->setRepCommissionEarned(null);
      $salesrep->setRepCommissionStatus(null);
      $salesrep->setManagerId(null);
      $salesrep->setManagerName(null);
      $salesrep->setManagerCommissionEarned(null);
      $salesrep->setManagerCommissionStatus(null);
    }

    $salesrep->setRepCommissionStatus(Mage::getStoreConfig('salesrep/setup/default_status'));
    $salesrep->setManagerCommissionStatus(Mage::getStoreConfig('salesrep/setup/default_status'));
    $salesrep->save();

    return $salesrep;
  }


  public function getSubordinateIds($id=null){
    if (!$this->_subordinateIds) {
      $this->_subordinateIds = array();

      $admin_user_collection = Mage::getResourceModel('admin/user_collection');
      $admin_user_collection->addFieldToFilter('salesrep_manager_id', $id);

      foreach ($admin_user_collection as $admin_user) {
        $this->_subordinateIds[] = $admin_user->getId();
      }

      $this->_subordinateIds[] = $id;
    }

    return $this->_subordinateIds;
  }

  public function getPercent($val, $for_save=false) {
    if ($for_save) {
      if ($val > 100) {
        $val = 100;
      }
      return $val/100;
    }

    return $val ? number_format($val*100, 2) : '';
  }

  public function isAdmin() {
    if (Mage::app()->getStore()->isAdmin()) {
      return true;
    }

    if (Mage::getDesign()->getArea() == 'adminhtml') {
      return true;
    }

    return false;
  }

  /**
   * Check user permission on resource
   *
   * @param   string $user
   * @param   string $resource
   * @return  boolean
   */
  public function isAllowed($user, $resource) {
    $acl = Mage::getResourceModel('admin/acl')->loadAcl();

    if (!preg_match('/^admin/', $resource)) {
      $resource = 'admin/'.$resource;
    }

    try {
      return $acl->isAllowed($user->getAclRole(), $resource);
    } catch (Exception $e) {
      try {
        if (!$acl->has($resource)) {
          return $acl->isAllowed($user->getAclRole(), null);
        }
      } catch (Exception $e) { }
    }
    return false;
  }

  public function isModuleInstalled() {
    $s =  Mage::getConfig()->getModuleConfig($this->_module_name);
    return ($s->active);
  }

  public function isModuleActive() {
    return Mage::getConfig()->getModuleConfig($this->_module_name)->is('active', 'true');
  }

  public function isModuleEnabled($module_name = NULL) {
    return $this->isModuleInstalled() && $this->isModuleActive() && Mage::getStoreConfig('salesrep/module_status/enabled');
  }

  public function isFrontendStepEnabled() {
    return Mage::getStoreConfig('salesrep/step_setup/step_enabled');
  }

  public function showFrontendStep() {
    if ($this->isModuleEnabled() && $this->isFrontendStepEnabled()) {

      $_show_rep_step = true;

      if (Mage::getSingleton('customer/session')->isLoggedIn()) {
        $_customer = Mage::getSingleton('customer/session')->getCustomer();

        if ($_customer->getSalesrepRepId()) {
          $_show_rep_step = false;
        }
      }

      if ($_show_rep_step) {
        return true;
      }
    }

    return false;
  }
}
?>
