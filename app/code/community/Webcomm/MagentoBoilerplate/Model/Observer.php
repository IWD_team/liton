<?php

class Webcomm_MagentoBoilerplate_Model_Observer
{
    public function addLayoutXml($event)
    {
        $xml = $event->getUpdates()
                ->addChild('magentoboilerplate');
        $xml->addAttribute('module', 'Webcomm_MagentoBoilerplate');
        if (Mage::app()->getRequest()->getParam('price-sheet-view') != 'show') {
            $xml->addChild('file', 'magentoboilerplate.xml');
        }
        else{
            $xml->addChild('file', 'magentoboilerplate-pricesheet.xml');
        }
    }
}
