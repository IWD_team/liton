<?php
    class IWD_Category_Model_Observer
    {
        public function catalogControllerCategoryInitAfter($observer)
        {
            $category = $observer->getData('category');
            $productCollection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addCategoryFilter($category)
                ->addAttributeToFilter('status', array('eq' => 1));
            $productsCount = $productCollection->count();
            if($productsCount == 1){
                $firstProduct = $productCollection->addUrlRewrite()->getFirstItem();
                $url = $firstProduct->getProductUrl();
                Mage::app()->getResponse()->setRedirect($url)->sendResponse();
            }
        }

        public function controllerActionLayoutGenerateBlocksAfter($observer)
        {
            $controller = $observer->getAction();
            $layout = $controller->getLayout();
            $rootBlock = $layout->getBlock('root');
            if ($controller->getRequest()->getParam('price-sheet-view') == 'show') {
                $rootBlock->setTemplate('page/1column.phtml');
            }
        }
    }
?>