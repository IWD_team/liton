<?php
class IWD_Breadcrumbs_Model_Observer
{

	public function addCategoryBreadcrumb(Varien_Event_Observer $observer)
	{
		if (!Mage::registry('current_category')) {
            /* @var $product Mage_Catalog_Model_Product */
			$product = $observer->getProduct();
			$categoryIds = $product->getCategoryIds();

			if(count($categoryIds)) {
                /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
                $collection = Mage::getModel('catalog/category')->getCollection();
				$collection->addAttributeToSelect('name');
				$collection->addAttributeToFilter('is_active', array('eq'=>'1'));
				$collection->addAttributeToFilter('entity_id', array('in' => $categoryIds));
				$collection->addAttributeToSort('entity_id', 'desc');
				$collection->setPageSize(1);

                /* @var $category Mage_Catalog_Model_Category */
                $category = $collection->getFirstItem();
				if ($category->getId()) {
					$product->setCategory($category);
					Mage::register('current_category', $category);
				}
			}
		}

		return $this;
	}

}