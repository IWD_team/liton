<?php

class Unleaded_Common_Model_Customer_Customer extends Mage_Customer_Model_Customer {
	public function cleanPasswordsValidationData()
    {
        $this->setData('password_confirmation', null);
        return $this;
    }
}