<?php

class Unleaded_WishlistPrinter_PdfController extends Mage_Core_Controller_Front_Action {

    const XML_PATH_EMAIL_RECIPIENT = 'quotepdfprinter/mail_setting/send_to';
    const XML_PATH_EMAIL_SENDER = 'quotepdfprinter/mail_setting/email_sender';
    const XML_PATH_EMAIL_TEMPLATE = 'quotepdfprinter/mail_setting/email_template';

    public function indexAction() {
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _getCart() {
        return Mage::getSingleton('checkout/cart');
    }

    public function printAction() {

        $this->loadLayout();
        $layout = Mage::getSingleton('core/layout');
        $html = $layout
                ->createBlock('core/template')
                ->setTemplate('ulwishlistprinter/wishlistpdf.phtml')
                ->toHtml();
        $pdfHtml = $html;

        //echo $pdfHtml;
        //die;
        
        $wishlistPdf = Mage::getModel('wishlistprinter/wishlist')->getWishlistPdf($pdfHtml);

        $filename = 'MyProjectFolder.pdf';

        $wishlistPdf->Output(Mage::getBaseDir('var') . '/spec-package/' . $filename, 'F');
        
        $this->_prepareDownloadResponse($filename, file_get_contents(Mage::getBaseDir('var') . '/spec-package/' . $filename), 'application/x-pdf');
    }

    protected function _prepareDownloadResponse($fileName, $content, $contentType = 'application/octet-stream', $contentLength = null) {
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->setHeader('Pragma', 'public', true)
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
                ->setHeader('Content-type', $contentType, true)
                ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength)
                ->setHeader('Content-Disposition', 'attachment; filename=' . $fileName)
                ->setHeader('Last-Modified', date('r'));
        if (!is_null($content)) {
            $this->getResponse()->setBody($content);
        }
        return $this;
    }

}
