<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Block_Product_List extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';

    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

    public function getTemplate()
    {
        if ($this->getNameInLayout() == 'product_list') {
            if (Mage::app()->getRequest()->getParam('price-sheet-view') == 'show') {
                return 'catalog/category/price_sheet.phtml';
            }
            $handles = Mage::app()->getLayout()->getUpdate()->getHandles();
            if (in_array('THEME_frontend_liton_combo-category', $handles)) {
                return 'catalog/category/landing.phtml';
            }
        }
        return parent::getTemplate();
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                    $this->addModelTags($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();

            $handles = Mage::app()->getLayout()->getUpdate()->getHandles();
            if (in_array('THEME_frontend_liton_combo-category', $handles)) {
                $this->_productCollection->addAttributeToFilter('comboview_visibility', array('eq' => 1));
            }

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }

        return $this->_productCollection;
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('catalog/layer');
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            $toolbar->setDefaultDirection($dir);
        }
        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        $this->_getProductCollection()->load();

        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock()
    {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

    /**
     * Retrieve additional blocks html
     *
     * @return string
     */
    public function getAdditionalHtml()
    {
        return $this->getChildHtml('additional');
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    public function setCollection($collection)
    {
        $this->_productCollection = $collection;
        return $this;
    }

    public function addAttribute($code)
    {
        $this->_getProductCollection()->addAttributeToSelect($code);
        return $this;
    }

    public function getPriceBlockTemplate()
    {
        return $this->_getData('price_block_template');
    }

    /**
     * Retrieve Catalog Config object
     *
     * @return Mage_Catalog_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('catalog/config');
    }

    /**
     * Prepare Sort By fields from Category Data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Block_Product_List
     */
    public function prepareSortableFieldsByCategory($category) {
        if (!$this->getAvailableOrders()) {
            $this->setAvailableOrders($category->getAvailableSortByOptions());
        }
        $availableOrders = $this->getAvailableOrders();
        if (!$this->getSortBy()) {
            if ($categorySortBy = $category->getDefaultSortBy()) {
                if (!$availableOrders) {
                    $availableOrders = $this->_getConfig()->getAttributeUsedForSortByArray();
                }
                if (isset($availableOrders[$categorySortBy])) {
                    $this->setSortBy($categorySortBy);
                }
            }
        }

        return $this;
    }

    /**
     * Retrieve block cache tags based on product collection
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            $this->getItemsTags($this->_getProductCollection())
        );
    }

    // BY IWD
    public function getCategoryTree()
    {

        $collection = Mage::getModel('catalog/category')->getResourceCollection()
            //->addStoreFilter()
            ->addIsActiveFilter()
            ->addNameToResult()
            ->addUrlRewriteToResult()
        ;

        $categoryNames = array();
        foreach ($collection as $item) {
            $categoryNames[$item->getId()] = array('name' => $item->getName(), 'url' => $item->getUrl($item));
        }

        $childrenCollection = Mage::getResourceModel('catalog/category_collection')
            ->setOrder('level', 'desc')
            ->setOrder('position', 'asc')
            ->addAttributeToSelect(array('image', 'description'))
            ->addAttributeToFilter('is_active', true);

        foreach ($childrenCollection->getItems() as $item) {
            $categories[$item->getId()] = array(
                'name' => $categoryNames[$item->getId()]['name'],
                'url' => $categoryNames[$item->getId()]['url'],
                'level' => $item->getData('level'),
                'image' => $item->getImageUrl(),
                'description' => $item->getDescription(),
                'parent_id' => $item->getData('parent_id'),
                'product_count' => 0,
                'path' => $item->getData('path'),
            );
        }

        $currentCategoryId = $this->getParentBlock()->getCurrentCategory()->getId();
        $currentCategoryPath = $categories[$currentCategoryId]['path'];
        $currentCategoryPath = explode('/', $currentCategoryPath);

        $productCollection = $this->getLoadedProductCollection();

        foreach ($categories as $id => $category) {
            if (!empty ($category['parent_id'])) {
                $parent = &$categories[$category['parent_id']];
                if (!isset($parent['children']))
                    $parent['children'] = array();
                $parent['children'][$id] = &$categories[$id];
            }
        }

        /**
         * Get the resource model
         */
        $resource = Mage::getSingleton('core/resource');

        /**
         * Retrieve the read connection
         */
        $readConnection = $resource->getConnection('core_read');

        $productPositions = $readConnection->fetchAll('select * from catalog_category_product');
        $productPositionsByCategory = [];
        foreach ($productPositions as $productPosition) {
            if (!isset($productPositionsByCategory[$productPosition['category_id']])) {
                $productPositionsByCategory[$productPosition['category_id']] = [];
            }
            $productPositionsByCategory[$productPosition['category_id']][$productPosition['product_id']] = $productPosition['position'];
        }

        foreach ($productCollection->getItems() as $product) {
            $categoryIds = $product->getCategoryIds();

            foreach ($categories as $id => $category) {
                if (in_array($id, $categoryIds)) {
                    $categories[$id]['product_count']++;
                    if (empty($categories[$id]['products']))
                        $categories[$id]['products'] = array();
                    $categories[$id]['products'][$product->getId()] = array(
                        'id' => $product->getId(),
                        'model' => $product,
                        'name' => $product->getName(),
                        'url' => substr($categories[$id]['url'], 0, strpos($categories[$id]['url'], '.html')).'/'.$product->getUrlKey().'.html',
                        'image' => Mage::helper('catalog/image')->init($product, 'small_image')->resize(220)->__toString(),
                        'lumens_slider' => $product->getResource()->getAttribute('lumens_slider')->getFrontend()->getValue($product),
                        'short_group_description' => $product->getData('short_group_description'),
                        'comboview_single_item_title' => $product->getData('comboview_single_item_title'),
                        'product_description' => $product->getResource()->getAttribute('short_description')->getFrontend()->getValue($product),
                        'product_benefits' => $product->getResource()->getAttribute('product_benefits')->getFrontend()->getValue($product),
                        );
                }
            }
        }

        foreach ($categories as $id => $category) {
            $sortMap = $productPositionsByCategory[$id];
            usort($categories[$id]['products'], function ($a, $b) use ($sortMap) {
                return $sortMap[$a['id']] > $sortMap[$b['id']];
            });
        }

//        echo '<pre>';
//        var_dump($categories[$currentCategoryId]);
//        die();

        if (Mage::app()->getRequest()->getParam('price-sheet-view') == 'show') {
            return $categories[$currentCategoryId];
        }

        foreach ($categories as $id => $category) {
            if (!empty ($category['parent_id']) && $category['product_count'] > 0) {
                $parent = &$categories[$category['parent_id']];
                $parent['product_count'] += $category['product_count'];
            }
        }

        foreach ($categories as $id => $category) {
            if (!empty ($category['parent_id'])) {
                unset($categories[$id]);
            }
        }

        foreach ($currentCategoryPath as $_id) {
              $categories = $categories[$_id]['children'];
        }


//        $currentCategory = $categories[$currentCategoryId];
//        $categories = $currentCategory['children'];

        return $categories;
    }

    public function renderCategoryTree()
    {
        $html = '<div class="combo-categ-wrapper">';
        $counter = 0;
        $categoryTree = $this->getCategoryTree();
        foreach ($categoryTree as $categoryId => $category) {
            $counter++;
            $html .= $this->_render($categoryId, $category);
            if($counter%4 == 0){
                $html .= '</div><div class="combo-categ-wrapper">';
            }
        }
        return $html.'</div>';
    }

    public function renderProductSheet()
    {
        $html = '';

        $categoryTree = $this->getCategoryTree();

        $html .= $this->getLayout()->createBlock('core/template', 'category_with_products_' . uniqid())
            ->setTemplate('catalog/category/price-sheet-products.phtml')
            ->setData(array('products' => $categoryTree['products']))
            ->toHtml();

        foreach ($categoryTree['children'] as $categoryId => $category) {
            $html .= $this->_render($categoryId, $category);
        }
        return $html;
    }

    protected function _render($categoryId, $category)
    {

        $level = $category['level'];
        $name = $category['name'];
        $url = $category['url'];
        $categImage = $category['image'];
        $categDescription = $category['description'];
        $isLast = empty($category['children']);
        $class = 'category-level-' . $level;

        if (Mage::app()->getRequest()->getParam('price-sheet-view') == 'show') {

            if ($level == 5 && $category['product_count'] == 0)
                return '';

            $titleLevel = $level - 2;
            $html = "<div class='price-sheet-wrapper'>";
            $html .= "<h$titleLevel class='product-sheet-categ-name'>$name</h$level>";

            $html .= $this->getLayout()->createBlock('core/template', 'category_with_products_' . $categoryId)
                ->setTemplate('catalog/category/price-sheet-products.phtml')
                ->setData(array('products' => $category['products']))
                ->toHtml();

            foreach ($category['children'] as $childId => $child) {
                $html .= $this->_render($childId, $child);
            }
            $html .= "</div>";

            return $html;
        }

        if ($category['product_count'] == 0)
            return '';

        if ($isLast)
            $class = $class . ' last';

        $priceSheetLink = '';
        if (Mage::getSingleton('customer/session')->isLoggedIn() && Mage::getSingleton('customer/session')->getCustomerGroupId() == 2){
            if($level == 3){
                $priceSheetLink = "<a class='price-sheet-view-link' href='$url?price-sheet-view=show' title='$name'>(View Category Price Sheet)</a>";
            }
            elseif($level == 4){
                $priceSheetLink = "<a class='price-sheet-view-link' href='$url?price-sheet-view=show' title='$name'>(View Item Price Sheet)</a>";
            }
        }

        $html = '<div class="categ-item '.$class.' grid-item">';
        $html .= "<div class='category-info-lvl$level'>";
        $html .= "<h$level><a class='combo-categ-title-link' href='$url' title='$name'>$name</a>$priceSheetLink</h$level>";

//        if ($level == 3) {
//            $html .= $this->getLayout()->createBlock('core/template', 'category_with_items_' . $categoryId)
//                ->setTemplate('catalog/category/combo-view-items.phtml')
//                ->setData(array('products' => $category['products']))
//                ->toHtml();
//        }

        if (!$isLast) {
            if(!empty($categDescription))
                $html .= "<p>$categDescription</p>";
            if(!empty($categImage))
                $html .= "<img class='category-img' src='$categImage' alt='$name'>";
            $html .= "</div>";

            $counter = 0;
            $html .= '<div class="combo-categ-wrapper">';
            foreach ($category['children'] as $childId => $child) {
                $counter++;
                $html .= $this->_render($childId, $child);
                if ($counter % 4 == 0) {
                    $html .= '</div><div class="combo-categ-wrapper">';
                }
            }
            if ($level == 4) {
                $html .= $this->getLayout()->createBlock('core/template', 'category_with_items_' . $categoryId)
                    ->setTemplate('catalog/category/combo-view-items.phtml')
                    ->setData(array('products' => $category['products'], 'counter' => $counter))
                    ->toHtml();
            }
            $html .= '</div>';
        }
        elseif($level == 4) {
            if(!empty($categDescription))
                $html .= "<p>$categDescription</p>";
            if(!empty($categImage))
                $html .= "<img class='category-img' src='$categImage' alt='$name'>";
            $html .= "</div>";

            $counter = 0;
            $html .= '<div class="combo-categ-wrapper">';
            $html .= $this->getLayout()->createBlock('core/template', 'category_with_items_' . $categoryId)
                ->setTemplate('catalog/category/combo-view-items.phtml')
                ->setData(array('products' => $category['products'], 'counter' => $counter))
                ->toHtml();
            $html .= '</div>';
        }
        else{
            $html .= "</div>";
//        elseif($level != 3) {
            $html .= $this->getLayout()->createBlock('core/template', 'last_' . $categoryId)
                ->setTemplate('catalog/category/last-category.phtml')
                ->setData(array('products' => $category['products'], 'categ_url' => $url, 'categ_image' => $categImage))
                ->toHtml();
        }
        $html .= '</div>';

        return $html;
    }
}
