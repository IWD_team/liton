<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Sorting
 */
class Amasty_Sorting_Model_Observer
{
    public function onCoreBlockAbstractToHtmlBefore($observer)
    {
        $block = $observer->getBlock();
        $searchBlocks = array();
        $searchBlocks[] = Mage::getConfig()->getBlockClassName('catalogsearch/result');
        $searchBlocks[] = Mage::getConfig()->getBlockClassName('catalogsearch/advanced_result');
        if (in_array(get_class($block), $searchBlocks)) {
            $block->getChild('search_result_list')
                ->setDefaultDirection('desc')
                ->setSortBy(Mage::getStoreConfig('amsorting/general/default_search'));
        }

        return $this;
    }
}
