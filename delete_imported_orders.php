<?php

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);

$mageFilename = 'app/Mage.php';

if (!file_exists($mageFilename)) {
    echo $mageFilename . ' was not found';
    exit;
}

require_once $mageFilename;

Mage::app();

$resource = Mage::getSingleton('core/resource');
$connection = $resource->getConnection('core_write');

$q = 'SELECT entity_id FROM sales_flat_order WHERE import_id IS NOT NULL and increment_id LIKE \'0%\';';
$orderIds = $connection->fetchCol($q);

$q = 'SELECT quote_id FROM sales_flat_order WHERE import_id IS NOT NULL and increment_id LIKE \'0%\';';
$quoteIds = $connection->fetchCol($q);

$orderIdsStr = implode(',', $orderIds);
$q = 'DELETE FROM sales_flat_order WHERE entity_id IN( ' . $orderIdsStr . ')';
$connection->query($q);

$quoteIdsStr = implode(',', $quoteIds);
$q = 'DELETE FROM sales_flat_quote WHERE entity_id IN( ' . $quoteIdsStr . ')';
$connection->query($q);

exit('All done...' . "\n");
