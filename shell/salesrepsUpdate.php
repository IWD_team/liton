<?php

require_once 'abstract.php';

class Cminds_Shell_Process_Salesreps extends Mage_Shell_Abstract
{
    /**
     * Run script.
     *
     * @return void
     */
    public function run()
    {
        Mage::getModel('import/salesreps')->createSalesreps();
    }
}

$shell = new Cminds_Shell_Process_Salesreps();
$shell->run();