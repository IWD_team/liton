<?php

require_once 'abstract.php';

class Cminds_Shell_Process_Customers extends Mage_Shell_Abstract
{
    /**
     * Run script.
     *
     * @return void
     */
    public function run()
    {
        Mage::getModel('import/customers')->createCustomers();
    }
}

$shell = new Cminds_Shell_Process_Customers();
$shell->run();
