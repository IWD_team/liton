<?php

require_once 'abstract.php';

class Cminds_Shell_Process_Files extends Mage_Shell_Abstract
{
    /**
     * Run script.
     *
     * @return void
     */
    public function run()
    {
        Mage::getModel('import/files')->copyOrdersFile();
        Mage::getModel('import/files')->copyCustomersFile();
        Mage::getModel('import/files')->copySalesrepsFile();
    }
}

$shell = new Cminds_Shell_Process_Files();
$shell->run();
