<?php

require_once 'abstract.php';

class Cminds_Shell_Process_Invoices extends Mage_Shell_Abstract
{
    /**
     * Run script.
     *
     * @return void
     */
    public function run()
    {
        Mage::getModel('import/ordersInvoices')->createOrders();
    }
}

$shell = new Cminds_Shell_Process_Invoices();
$shell->run();
