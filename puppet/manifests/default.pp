# Clear tmp files

exec { "clear_sessions":
    command => "sudo rm -fr /vagrant/var/session/*",
    path    => "/usr/bin"
}

exec { "clear_cache":
    command => "sudo rm -fr /vagrant/var/cache/*",
    path    => "/usr/bin"
}

exec { "clear_report":
    command => "sudo rm -fr /vagrant/var/report/*",
    path    => "/usr/bin"
}

exec { "clear_tmp":
    command => "sudo rm -fr /vagrant/var/tmp/*",
    path    => "/usr/bin"
}

exec { "clear_cache_media":
    command => "sudo rm -fr /vagrant/media/catalog/product/cache/*",
    path    => "/usr/bin"
}

exec { "clear_tmp_media":
    command => "sudo rm -fr /vagrant/media/tmp/*",
    path    => "/usr/bin"
}

exec { "clear_locks":
    command => "sudo rm -fr /vagrant/var/locks/*",
    path    => "/usr/bin"
}

# mailcatcher

exec { "mailcatcher_start":
    command => "sudo mailcatcher --ip 0.0.0.0 --http-ip 0.0.0.0",
    path    => "/usr/bin"
}
